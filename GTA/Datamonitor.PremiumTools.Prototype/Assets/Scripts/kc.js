
// Functions taken from OLD vortal.js file

// handles the Enter button being pressed on the Login Screen
function doKeyPressLogin(event, form)
{
	if (event.keyCode==13)
	{
		if (validateLogin(form) == true)
		{
			form.submit();
		}
	}
	
}

// handles the Enter button being pressed on the Login Screen
function doKeyPressChangePassword(event, form)
{
	if (event.keyCode==13)
	{
		if (validateChangePassword(form) == true)
		{
			form.submit();
		}
	}
	
}

// validates the Login Form
function validateLogin(form)
{
	var username;
	var password;
	var errorText;
	username = form.Username.value;
	password = form.Password.value;
	errorText = "";

	if (username.length < 1)
	{
		errorText = errorText + "Your username must not be blank\n";
		form.Username.focus();
	}
	else
	{
		if (username.length > 15)
		{
			errorText = errorText + "Your username must be less than 15 characters\n";
			form.Username.focus();
		}
	}
	if (password.length < 5)
	{
		errorText = errorText + "Your password must be AT LEAST 5 characters\n";
		form.Password.focus();
	}
	else
	{
		if (password.length > 15)
		{
			errorText = errorText + "Your password must be less than 15 characters\n";
			form.Password.focus();
		}
	}
	if (errorText.length != 0)
	{
		alert(errorText);
		validateLogin == false;
	}
	else
	{
		validateLogin == true;
		form.submit();
	}
	
}

// validates the ChangePassword Form (when user first logs in)
function validateChangePassword(form)
{
	var newPassword;
	var confirmNewPassword;
	var errorText;
	newPassword = form.newPassword.value;
	confirmNewPassword = form.confirmNewPassword.value;
	errorText = "";

	if (newPassword.length < 1)
	{
		errorText = errorText + "Your new password must NOT be blank!\n";
	}
	else 
	{
		if (newPassword.length < 5)
		{
			errorText = errorText + "Your new password must be AT LEAST 5 characters!\n";
		}
	}
	if (newPassword.length > 15)
	{
		errorText = errorText + "Your new password must be less than 15 characters!\n";
	}
	if (newPassword != confirmNewPassword)
	{
		errorText = errorText + "Your new passwords must match!\n";
	}
	if (errorText.length != 0)
	{
		alert(errorText);
		validateChangePassword == false;
	}
	else
	{
		validateChangePassword == true;
		form.submit();
	}
}

// validates the forgotten password form
function validateForgottenPassword(form)
{
	var emailAddress;
	var username;
	var returnBoolean;
	var errorText;
	
	errorText = "";
	emailAddress = form.existingEmail.value;
	username = form.existingUsername.value;
	
	if (username.length == 0)
	{
		errorText = errorText + "Your username must NOT be blank!\n";
		form.existingUsername.focus();
	}
	else
	{
		if (username.length > 15)
		{
			errorText = errorText + "Your username must be less than 15 characters!\n";
			form.existingUsername.focus();
		}
	}
	if (emailAddress.length > 0)
	{
		if (checkEmail(emailAddress) == false)
		{
			errorText = errorText + "The email address you have entered appears to be invalid.\n";
			form.existingEmail.focus();
		}
	}
	else 
	{
		if (emailAddress.length == 0)
		{
			errorText = errorText + "You must enter a valid email address\n";
			form.existingEmail.focus();
		}
	}
	if (errorText.length != 0)
	{
		alert(errorText);
	}
	else
	{
		form.submit();
	}
}


function validateContactUs(form){
	var title;
	var firstName;
	var surname;
	var jobFunction;
	var company;
	var country;
	var telephone;
	var email;
	var contactMethod;
	var message;
	var errorMessage;
	
	errorMessage = "";

	firstName = form.txtFirstName.value;
	if(firstName.length <= 0){
		errorMessage = errorMessage + "Please enter your first name. \n";
	}

	surname = form.txtSurname.value;
	if(surname.length <= 0){
		errorMessage = errorMessage + "Please enter your surname. \n";
	}

	if (form.cmbJobFunction.selectedIndex == 0) { //note checking for first item in drop-down list which will always be blank
		errorMessage = errorMessage + "Please select a job function from the dropdown list. \n";
	}
	else {
		jobFunction = form.cmbJobFunction.options[form.cmbJobFunction.selectedIndex].value; // jobFunction = form.cmbJobFunction.value;
	}

	company = form.txtCompany.value;
	if(company.length <= 0){
		errorMessage = errorMessage + "Please enter your company name. \n";
	}

	if (form.cmbCountry.selectedIndex == -1) {
		errorMessage = errorMessage + "Please select a country from the dropdown list. \n";
	}
	else {
		country = form.cmbCountry.options[form.cmbCountry.selectedIndex].value; // country = form.cmbCountry.value;
	}

	telephone = form.txtTelephone.value;

	email = form.txtEmail.value;

	contactMethod = "";
    for (i=0; i<form.radContactMethod.length; i++){
		if (form.radContactMethod[i].checked){
           contactMethod = form.radContactMethod[i].value;
		}
	}

	//contact method
	if(contactMethod.length <= 0){
		errorMessage = errorMessage + "Please select a contact method. \n";
	}
	else{
		if(contactMethod == "Email"){
			if(email.length <= 0){
				errorMessage = errorMessage + "You have chosen email as your prefered contact method, therefore, please enter your email. \n";
			}
		}
		if(contactMethod == "Phone"){
			if(telephone.length <= 0){
				errorMessage = errorMessage + "You have chosen phone as your prefered contact method, therefore, please enter your phone number. \n";
			}
		}				
	}
	//Check for validity of email (only if an email address is submitted)
	if(email.length > 0){
		if (checkEmail(email) == false){
			errorMessage = errorMessage + "The email address you have entered appears to be invalid. \n";
		}
	}
	//Decide whether to submit or not
	if (errorMessage.length > 0){
		alert(errorMessage);
	}
	else{
		form.submit();
	}	
}


function validateEmailClippings(form){
	var senderEmail;
	var receiverEmail;
	var errorMessage;
	var numOfAts;
	var numOfSemiColons;
	var arrEmails;
	
	errorMessage = "";
	numOfAts = 0;
	numOfSemiColons = 0;
	senderEmail = form.txtSenderEmail.value;
	receiverEmail = form.txtRecepientEmail.value;
	
	//Sender's email
	if(senderEmail.length <= 0){
		errorMessage = errorMessage + "Please enter your email address. \n";
	}
	else{
		if (checkEmail(senderEmail) == false){
			errorMessage = errorMessage + "The sender email address you have entered appears to be invalid. \n";
		}
	}
	//Recepient's email
	if(receiverEmail.length <= 0){
		errorMessage = errorMessage + "Please enter the recepients' email address(s). \n";
	}
	else{
		//First check whether the user has entered more than one email
		for(var i=0; i<receiverEmail.length; i++){
			if(receiverEmail.charAt(i) == "@"){
				numOfAts += 1;
			}
		}
		//If it is only one email address
		if(numOfAts == 1){
			//The user has entered only one email address so check its validity
			if (checkEmail(receiverEmail) == false){
				errorMessage = errorMessage + "The recepient email address you have entered appears to be invalid. \n";
			}		
		}
		//If the user has entered more than one email check whether they have seperated them by semicolons or not
		if(numOfAts > 1 && numOfAts < 11){
			for(var j=0; j<receiverEmail.length; j++){
				if(receiverEmail.charAt(j) == ";"){
					numOfSemiColons += 1;
				}
			}
			//The number of semicolons should be one less than the number of @. If not error
			if(numOfSemiColons = (numOfAts - 1)){
				//Enter the email addresses into an array
				arrEmails = receiverEmail.split(";");
				//Now loop through the array checking each email address for validity
				for(var n=0; n<(arrEmails.length - 1); n++){
					if(checkEmail(arrEmails[n]) == false){
						errorMessage = errorMessage + "The recepient email address, " + arrEmails[n] + " appears to be invalid. \n";	
					}
				}
			}
			else{
				errorMessage = errorMessage + "You should seperate the recepient email addresses using semicolons (;), not after the last address though. \n";	
			}
		}
		//The user has entered more than 10 email address so error
		if (numOfAts > 10){
			errorMessage = errorMessage + "It appears that you are attempting to email this clipping to more than 10 people. The maximum number of email addresses allowed are 10. \n";
		}
	}
	//Decide whether to submit or not
	if (errorMessage.length > 0){
		alert(errorMessage);
	}
	else{
		form.submit();
	}
}


// validates the Change Password Form - for Edit My Profile
function validateProfileChangePass(form)
{
	var username;
	var oldPassword;
	var newPassword;
	var verifyNewPassword;
	var errorText;
	errorText = "";
	
	username = form.username.value;
	oldPassword = form.oldPassword.value;
	newPassword = form.newPassword.value;
	verifyNewPassword = form.verifyNewPassword.value;
	
	if (username.length <= 0)
	{
		errorText = errorText + "You must enter your username\n";
		form.username.focus();
	}
	if (oldPassword.length < 5)
	{
		errorText = errorText + "Your old password must be at least 5 characters\n";
		form.oldPassword.focus();
	}
	if (newPassword.length < 5)
	{
		errorText = errorText + "Your new password must be at least 5 characters\n";
		form.newPassword.focus();
	}
	else
	{
		if (newPassword.length > 15)
		{
			errorText = errorText + "Your new password must be 15 characters or less\n";
			form.newPassword.focus();
		}
	}
	if (verifyNewPassword.length < 5)
	{
		errorText = errorText + "Your verify password must be at least 5 characters\n";
		form.verifyNewPassword.focus();
	}
	else
	{
		if (verifyNewPassword.length > 15)
		{
			errorText = errorText + "Your verify password must be 15 characters or less\n";
			form.verifyNewPassword.focus();
		}
	}
	if (newPassword != verifyNewPassword)
	{
		errorText = errorText + "Your new password must be the same as your verify password\n";
		form.newPassword.focus();
	}
	if (newPassword == oldPassword)
	{
		errorText = errorText + "Your new password cannot be the same as your old password\n";
		form.newPassword.focus();
	}
	
	if (errorText.length != 0)
	{
		alert(errorText);
	}
	else
	{
		form.submit();
	}
	
}

function getNumberOfCharactersTyped() 
{
	if(document.docForm.txtQuestion.value.length>400)
		document.docForm.txtQuestion.value = document.docForm.txtQuestion.value.substr(0,400)	
		
	var oDiv = document.getElementById('numOfCharacters');
	oDiv.innerHTML = "So far you have typed: " + document.forms["docForm"].txtQuestion.value.length + " characters.";
}

function getNumberOfCharactersTypedNew(textArea, textDiv, maxLength) 
{
	if(textArea.value.length>maxLength)
		textArea.value = textArea.value.substr(0,maxLength)	
		
	textDiv.innerHTML = "So far you have typed: " + textArea.value.length + " characters.";
}

//This function validates the "Ask an Analyst" form
function validateAskAnalyst(form){
	var name;
	var email;
	var phone;
	var question;
	
	errorMessage = "";
	name = form.txtName.value;
	email = form.txtEmail.value;
	phone = form.txtTelephone.value;
	question = form.txtQuestion.value;
	
	//name
	if(name.length <= 0){
		errorMessage = errorMessage + "Please enter your full name. \n";
	}
	//email
	if(email.length <= 0){
		errorMessage = errorMessage + "Please enter your email address. \n";
	}
	else{
		if (checkEmail(email) == false){
			errorMessage = errorMessage + "The email address you have entered appears to be invalid. \n";
		}
	}
	//phone
	if(phone.length <= 0){
		errorMessage = errorMessage + "Please enter your telephone number. \n";
	}
	//question
	if(question.length <= 0){
		errorMessage = errorMessage + "Please enter your question or comment. \n";
	}
	else{
		if (question.length > 4000){
			errorMessage = errorMessage + "The question you have entered is more than the allowed 4000 characters. \n";
		}
	}	
	//Decide whether to submit or not
	if (errorMessage.length > 0){
		alert(errorMessage);
	}
	else{
		form.submit();
	}
}

//This function validates the "Ask an Analyst" form
function validateClipForm(formId){
	var title;
	var errorMessage;
	var form = document.getElementById(formId);
	
	errorMessage = "";
	title = form.txtClipTitle.value;
	
	//clip title
	if(title.length <= 0)
	{
		errorMessage = errorMessage + "Please enter a Title for your Clipping. \n";
	}

	//Decide whether to submit or not
	if (errorMessage.length > 0)
	{
		alert(errorMessage);
	}
	else
	{
		form.submit();
	}
}

// End old functions

function di(id,name)
{
	document.images[id].src=eval(name+".src");
}

//butt = new Image(20, 19); butt.src = '/kc/images/butt_off.gif';
//butto = new Image(20, 19); butto.src = '/kc/images/butt_over.gif';
//pres = new Image(183, 19); pres.src = '/kc/images/build_pres_off.gif';
//preso = new Image(183, 19); preso.src = '/kc/images/build_pres_over.gif';
//report = new Image(148, 19); report.src = '/kc/images/build_report_off.gif';
//reporto = new Image(148, 19); reporto.src = '/kc/images/build_report_over.gif';


// This function creates a popup window with set properties
function CreatePopUp(target,name,w,h,scroll,loc,stat,menu,tool,pos,res)
{
    var LeftPos = 50;
    var TopPos = 50;
    var objWin;

    if(pos=="center")
    {
        LeftPos=(screen.width)?(screen.width-w)/2:100;
        TopPos=(screen.height)?(screen.height-h)/2:100;
    }
    var settings='width='+w+',height='+h+',top='+TopPos+',left='+LeftPos+',scrollbars='+scroll+',location='+loc+',directories=no,status='+stat+',menubar='+menu+',toolbar='+tool+',resizable='+res;

    objWin = window.open(target,name,settings);
    if (objWin) objWin.focus();
    return objWin;
}

// this function is being loaded via inline javascript calls in multiple instances..
// therefore calling setTextSize() is pointless from here as the <div> the latter function
// attempts to style is not yet in the DOM..
function addTextControl(){
	if(!document.getElementById) return;	
	// text-sizing controls not added if javascript disabled (or IE 4/NS 4)
	document.write('<span class="label">Text size:</span> <a title="small" href="#" id="text_small" onclick="setTextSize(\'' + 'repbody' + '\', 12, this.id); return false;" style="font-size:12px"><b>A</b></a> <span style="color:#666">|</span> <a title="medium" href="#" id="text_med" onclick="setTextSize(\'' + 'repbody' + '\', 14, this.id); return false;" style="font-size:14px"><b>A</b></a> <span style="color:#666">|</span> <a title="large" href="#" id="text_large" onclick="setTextSize(\'' + 'repbody' + '\', 16, this.id); return false;" style="font-size:16px"><b>A</b></a> ');
	
	// set default size (12px)
	//setTextSize('repbody', 12, 'text_small');
	
	// read in a cookie size value instead..
	var sessionFontSize = document.cookie.match ('(^|;) ?fixed_font_size=([^;]*)(;|$)');
    
    if(sessionFontSize)
    {
        setTextSize('repbody', unescape(sessionFontSize[2]), '', 'cookie');
    }
}			

// replication of previous function?
function addTextControlForNews(){
	if(!document.getElementById) return;	
	// text-sizing controls not added if javascript disabled (or IE 4/NS 4)
	document.write('Text size: <a class="current_state" title="small" href="#" id="text_small" onclick="setTextSize(\'' + 'article_body' + '\', 12, this.id); return false;" style="font-size:12px"><b>A</b></a> <span style="color:#666">|</span> <a title="medium" href="#" id="text_med" onclick="setTextSize(\'' + 'article_body' + '\', 14, this.id); return false;" style="font-size:14px"><b>A</b></a> <span style="color:#666">|</span> <a title="large" href="#" id="text_large" onclick="setTextSize(\'' + 'article_body' + '\', 16, this.id); return false;" style="font-size:16px"><b>A</b></a> ');
	
	// set default size (12px)
	//setTextSize('article_body', 12, 'text_small');
	
	// read in a cookie size value instead..
	var sessionFontSize = document.cookie.match ('(^|;) ?fixed_font_size=([^;]*)(;|$)');
    
    if(sessionFontSize)
    {
        setTextSize('article_body', unescape(sessionFontSize[2]), '', 'cookie');
    }
    
}

function resetTextControls(){
	var c = new Array('text_small', 'text_med', 'text_large');
	for(var i=0; i<c.length; i++){
		var el = document.getElementById(c[i]);
		if(el) el.className='';
	}
}

function setTextSize(el_className, size, control_id, source){
	// resize text
	if(!document.getElementById) return;
	
	// function works fine up to here when called via the onclick event in the <a> but not on 
	// page load as the <div> doesn't yet exist when the function is called from an inline script..
	
	// quick fix.. but will only apply after the DOM is complete = visible jump in text size..
	if (source == 'cookie')
	{
	    window.onload = function()
	    {
	        // this is more efficient than cycling through every <div> in the document. The <div> to
	        // which the style is applied exists only once and is best addressed by 'id' than a class..
	        var el = document.getElementById(el_className);
	        // would be better to change the CSS class on the <div> than this..
	        el.style.fontSize = size + 'px';
	    }
	}
	else
	{
	    var el = document.getElementById(el_className);
	    el.style.fontSize = size + 'px';
	}
	
	//var divs = document.getElementsByTagName("div");
	//for(var i=0; i<divs.length; i++){
	//	var el = divs[i];
		
	//	if(el.className == el_className) 
	//	{
	//	    alert(el.className);
	//	    el.style.fontSize = size + 'px';
	//	}
	//}
	
	resetTextControls();
					
	if(control_id){
		var el = document.getElementById(control_id);
		if(el) el.className = "current_state";
	}
	
	//set the cookie to recall the size
	document.cookie = 'fixed_font_size=' + size;
}


function resizeFig(fig_id, small_file, large_file, control_id, image_location){
	
	// AD - Have taken out min/max height args as there is no need if we use component to resize during deployment
	
	// enlarge/reduce figures
	// args:
	// control_id = id of anchor for "Enlarge image"

	if(!document.getElementById){
		// redirect to large image file (IE4/NS4)
		window.location = image_location + large_file;
		return;
	}
	
	// toggle link text
	var l = document.getElementById(control_id);
	var ln = (l.innerHTML == 'Enlarge image')? 'Reduce image' : 'Enlarge image';
	l.innerHTML = ln;
	
	var img = document.images[fig_id];
	
	// toggle image
	var s = (img.src.substr(img.src.lastIndexOf('/')+1));
	var f = (s == small_file)? large_file : small_file ;

	if (l.innerHTML == 'Enlarge image')
	{
		img.src = image_location + "small_" +  f;
	}
	else
	{			
		img.src = image_location +  f;
	}
}

function setToolbarState(e, over){
	// sets rollover/rollout state of table/figure toolbar cells
	if(!document.getElementsByTagName) return;								
	
	var targ;						
	if (!e) var e = window.event;
	if (e.target) targ = e.target;
	else if (e.srcElement) targ = e.srcElement;
	
	// find parent td
	p = targ;
	while(p.tagName != "TD"){
		p = p.parentNode;
	}
	
	// set td state
	var bgcol;
	var bgpic;					
	if(over){																
		bgcol = "#CBCBFF";
		bgpic = "url(/kc/images/toolbar_bg_over.gif)";																										
	} else {
		bgcol = "#DFE4ED";
		bgpic = "url(/kc/images/toolbar_bg.gif)";										
	}								
	p.style.backgroundColor = bgcol;
	p.style.backgroundImage = bgpic;			
}

// function to handle the Browse Report Drop-Down for Interactive Reports
function interactiveBrowseDropDown(tocURL, reportURL, selectBox)
{	
	var browserType = navigator.appName;
	var section;
	var optionLength;
	
	// the value of what was selected in the drop-down
	section = selectBox.options[selectBox.selectedIndex].value;

	optionLength = section.length;
	if (optionLength > 0)
	{	
		if (section == 'TOC')
		{
			//HACK: firstly remove other table references
			tocURL = tocURL.replace('table=lot', '');
			tocURL = tocURL.replace('table=lof', '');
			//redirect to TOC page
			document.location.href = tocURL;
		}
		else if (section == 'LOF')
		{	
			//HACK: firstly remove other table references
			tocURL = tocURL.replace('table=toc', '');
			tocURL = tocURL.replace('table=lot', '');
			//redirect to LOF page			
			document.location.href = tocURL + '&table=lof';
		}
		else if (section == 'LOT')
		{
			//HACK: firstly remove other table references
			tocURL = tocURL.replace('table=toc', '');
			tocURL = tocURL.replace('table=lof', '');
			//redirect to LOT page
			document.location.href = tocURL + '&table=lot';
		}
		else
		{
			if (browserType.indexOf("Netscape") == 0)
			{
				// we know they're using FireFox/Netscape etc
				document.location.href = reportURL + '&s=' + section;
			}
			else
			{
				// we know they're using a MS-based browser - for some reason a &s character gets interpreted as a "section sign" causing
				// the pid to become invalid
				document.location.href = reportURL + '&amp;s=' + section;
			}
		}
	}	
}

// This function launches a pop-up to display a progress bar while some longer-running-than-usual process is performed
function openProgressBarPopUp(target) 
{
    winPopUp = CreatePopUp(target,'new',412,225,'no','no','no','no','no','center','no');
}

// This function launches a pop-up to display the Ask an analyst from a specific Section of an Interactive Report
function openAnalystPopUp(page, pid, kc, sectionNumberTitle, type, news,email)
{
    winPopUp = CreatePopUp(page + '?pid=' + pid + '&KC=' + kc + '&sNumberTitle=' + sectionNumberTitle + '&type=' + type + '&news=' + news + '&email=' + email,'new',412,450,'no','no','no','no','no','center','no');
}

function openAnalystSlider(link, id)
{
	return hs.htmlExpand(link, { contentId: id } );
}

// This function launches a pop-up to display the Clipping page for an Interactive Report
function openClippingPopUp(page, pid, clipGUID, type, clipType, clipChapterTitle, clipSectionTitle, clipPosition, clipBespokeTitle)
{
    winPopUp = CreatePopUp(page + '?pid=' + pid + '&clipGUID=' + clipGUID + '&type=' + type + '&clipType=' + clipType + '&chapterTitle=' + encodeURIComponent(clipChapterTitle) + '&sTitle=' + encodeURIComponent(clipSectionTitle) + '&clipPosition=' + clipPosition + '&bespokeTitle=' + encodeURIComponent(clipBespokeTitle),'new',412,500,'no','no','no','no','no','center','no');
}
// This function launches a pop-up to edit an existing clipping
function openEditClippingPopUp(page, clipGUID)
{
    winPopUp = CreatePopUp(page + '&clipGUID=' + clipGUID,'new',412,500,'no','no','no','no','no','center','no');
}

// This function launches a pop-up to display the Email a Colleague a product page
function openEmailColleaguePopUp(page, R, productType)
{
    winPopUp = CreatePopUp(page + '?R=' + R + '&productType=' + productType,'new',412,500,'no','no','no','no','no','center','no');
}

function deleteClipping(page, clipGUID, productType, productID, xmlBuilderID, builderType, step, pageNumber, xmlDelete)
{
	if(confirm("Are you sure you want to permanently remove this clipping?"))
	{
		//winPopUp = CreatePopUp(page + '&txtClipGUID=' + clipGUID + '&txtProductType=' + productType + '&txtPID=' + productID + '&xmlBuilderID=' + xmlBuilderID + '&builderType=' + builderType + '&step=' + step + '&pageNumber=' + pageNumber + '&xmlDelete=' + xmlDelete,'new',412,500,'no','no','no','no','no','center','no');
		window.location.href = page + '&txtClipGUID=' + clipGUID + '&txtProductType=' + productType + '&txtPID=' + productID + '&xmlBuilderID=' + xmlBuilderID + '&builderType=' + builderType + '&step=' + step + '&pageNumber=' + pageNumber + '&xmlDelete=' + xmlDelete;
	}
}

// This function launches a pop-up to display the page for emailing clippings
function launchEmailClipping(pid, clipGUID, type, clipType, clipTitle, clipComment)
{
    winPopUp = CreatePopUp('/kc/cm/clippings/email.asp?pid=' + pid + '&clipGUID=' + clipGUID + '&type=' + type + '&clipType=' + clipType + '&title=' + clipTitle + '&clipComment=' + clipComment,'new',412,500,'no','no','no','no','no','center','no');
}

//This function is used to delete a SavedSearch 
function confirmSavedSearchDeletion(){
	if(confirm("You are about to delete a saved search query.\nThis will also delete any saved RSS feed link.\nAre you sure you want to continue?")){
		return true;
	}
}


//------------------------------------------------------------------------------------
// BYOR / BYOP functions - Start
//------------------------------------------------------------------------------------
function validateBuilderForm(form,op,type){
	var senderEmail;
	var receiverEmail;
	var errorMessage;
	var numOfAts;
	var numOfSemiColons;
	var arrEmails;
	var compositeTitle;
	var builderType;
	

	errorMessage = "";
	numOfAts = 0;
	numOfSemiColons = 0;
	senderEmail = form.senderEmail.value;
	receiverEmail = form.targetEmailAddresses.value;

	
	if (type == 1)
	{
		builderType = "report";
	}
	else
	{
		builderType = "presentation";
	}
	
	
	// check to see if they have added a Title for their Presentation/Report - type 1 is ReportBuilder
	if (type == 1)
	{
		compositeTitle = form.compositeReportTitle.value;
		if (compositeTitle.length <= 0)
		{
			errorMessage = errorMessage + "Please enter a title for your report. \n";
		}	
	}
	else
	{
		compositeTitle = form.compositePresentationTitle.value;
		if (compositeTitle.length <= 0)
		{
			errorMessage = errorMessage + "Please enter a title for your presentation. \n";
		}	
	}
	
	//If we are submitting the form for saving the presentation...
	if(op == "save"){
	
		//Decide whether to submit or not
		if (errorMessage.length > 0)
		{
			alert(errorMessage);
		}
		else
		{
			// add a step value to the page
			form.step.value = "step3.asp";
			//Clear the email and message textboxes
			form.senderEmail.value = "";
			form.targetEmailAddresses.value = "";
			form.emailMessageBody.value = "";
			//Set the value of the hidden field to "save"
			form.op.value = "save";
			//Select all the items in the listbox before submitting the form
			for(var n=0; n<document.docForm.clippingID.options.length; n++){
				document.docForm.clippingID.options[n].selected = true;
			}
			//Submit the form
			form.submit();
		}
	}
	//If we are submitting the form for saving and emailing the presentation then validate the form entries...
	else{
		
		//Sender's email
		if(senderEmail.length <= 0){
			errorMessage = errorMessage + "Please enter your email address. \n";
		}
		else{
			if (checkEmail(senderEmail) == false){
				errorMessage = errorMessage + "The sender email address you have entered appears to be invalid. \n";
			}
		}
		//Recepient's email
		if(receiverEmail.length <= 0){
			errorMessage = errorMessage + "Please enter the destination email address(es) for your " + builderType + ". \n";
		}
		else{
			//First check whether the user has entered more than one email
			for(var i=0; i<receiverEmail.length; i++){
				if(receiverEmail.charAt(i) == "@"){
					numOfAts += 1;
				}
			}
			
			//If it is only one email address
			if(numOfAts == 1){
				//The user has entered only one email address so check its validity
				if(checkEmail(receiverEmail) == false){
					errorMessage = errorMessage + "The destination email address you have entered appears to be invalid. \n";
				}		
			}
			
			//If there is no @ symbol then the email address is already invalid
			if(numOfAts == 0){
				errorMessage = errorMessage + "The destination email address you have entered appears to be invalid. \n";
			}
			
			//If the user has entered more than one email check whether they have seperated them by semicolons or not
			if(numOfAts > 1 && numOfAts < 11){
				for(var j=0; j<receiverEmail.length; j++){
					if(receiverEmail.charAt(j) == ";"){
						numOfSemiColons += 1;
					}
				}
				//The number of semicolons should be one less than the number of @. If not error
				if(numOfSemiColons = (numOfAts - 1)){
					//Enter the email addresses into an array
					arrEmails = receiverEmail.split(";");
					//Now loop through the array checking each email address for validity
					for(var k=0; k<(arrEmails.length - 1); k++){
						if(checkEmail(arrEmails[k]) == false){
							errorMessage = errorMessage + "The destination email address, " + arrEmails[k] + " appears to be invalid. \n";
						}
					}
				}
				else{
					errorMessage = errorMessage + "You should seperate the destination email addresses using semicolons (;). \n";	
				}
			}
			//The user has entered more than 10 email address so error
			if(numOfAts > 10){
				errorMessage = errorMessage + "It appears that you are attempting to email this " + builderType + " to more than 10 people. The maximum number of email addresses allowed are 10. \n";
			}
		}

		//Decide whether to submit or not
		if (errorMessage.length > 0){
			alert(errorMessage);
		}
		else{
			//Select all the items in the listbox before submitting the form
			for(var m=0; m<document.docForm.clippingID.options.length; m++){
				document.docForm.clippingID.options[m].selected = true;
			}
			
			// add a step value to the page
			form.step.value = "step3.asp";
			//Set the value of the hidden field to "email"
			form.op.value = "email";
			//Submit the form
			form.submit();
		}
	}
}

function checkMessageTextbox(textbox){
	if(textbox.value == ""){
		textbox.value = "Enter your message here...";
	}
	else{
		if(textbox.value == "Enter your message here..."){
			textbox.value = "";
		}
	}
}

function checkEmail(emailAddress)
{
	var filter = new RegExp("^[\\w-_\.+]*[\\w-_\.]\@([\\w]+\\.)+[\\w]+[\\w]$");
	
	if (filter.test(emailAddress))
	{
		return true;
	}
	else{
		return false;
	}
}


function isMultipleEmailCorrect(emailList, maxEmailAddresses)
{   
	var arrAddresses = emailList.split(';');
	
	//alert('arrAddresses: ' + arrAddresses + '\narrAddresses.length: ' + arrAddresses.length + '\nmaxEmailAddresses: ' + maxEmailAddresses);

	// check if the maximum no. of e-mail addresses has been exceeded..
	if (arrAddresses.length > maxEmailAddresses)
	{
	    return false;
	}
	
	// pass each e-mail to
	for (var i = 0; i < arrAddresses.length; i++)
	{
		if(!checkEmail(arrAddresses[i].strip()))
		{
	        return false;
		}
	}

	return true;
}

function upOne() {
    if(document.getElementById) {
        var box = document.getElementById('clippingID');
        if(box) {
            var index = box.selectedIndex;
           	if (index > 0) {
          		var elOptNew = document.createElement('option');
          		var elOptOld = box.options[index];
          		elOptNew.text = elOptOld.text;
          		elOptNew.value = elOptOld.value;
          		elOptNew.title = elOptOld.text;
         		  elOptOld = box.options[index-1];
              try {
          			box.add(elOptNew, elOptOld); // standards compliant; doesn't work in IE
          		}
          		catch(ex) {
          			box.add(elOptNew, box.selectedIndex-1); // IE only
          		}
         		  box.remove(index + 1);
           		box.selectedIndex = index-1;
          	}
        }
    }
}
	
function downOne() {
    if(document.getElementById) {
        var box = document.getElementById('clippingID');
        if(box) {
            var index = box.selectedIndex;
           	if ((index >= 0)&&(index < box.options.length-1)) {
          		var elOptNew = document.createElement('option');
          		var elOptOld = box.options[index];
          		elOptNew.text = elOptOld.text;
          		elOptNew.value = elOptOld.value;
          		elOptNew.title = elOptOld.text;
         		  elOptOld = box.options[index+2];
              try {
          			box.add(elOptNew, elOptOld); // standards compliant; doesn't work in IE
          		}
          		catch(ex) {
          			box.add(elOptNew, box.selectedIndex+2); // IE only
          		}
         		  box.remove(index);
           		box.selectedIndex = index+1;
          	}
        }
    }
}
	
function toTop() {
    if(document.getElementById) {
        var box = document.getElementById('clippingID');
        if(box) {
            var index = box.selectedIndex;
           	if (index > 0) {
          		var elOptNew = document.createElement('option');
          		var elOptOld = box.options[index];
          		elOptNew.text = elOptOld.text;
          		elOptNew.value = elOptOld.value;
          		elOptNew.title = elOptOld.text;
         		  box.remove(index);
         		  elOptOld = box.options[0];
              try {
          			box.add(elOptNew, elOptOld); // standards compliant; doesn't work in IE
          		}
          		catch(ex) {
          			box.add(elOptNew, 0); // IE only
          		}
           		box.selectedIndex = 0;
          	}
        }
    }
}
	
function toBottom() {
    if(document.getElementById) {
        var box = document.getElementById('clippingID');
        if(box) {
            var index = box.selectedIndex;
           	if (index < box.options.length-1) {
          		var elOptNew = document.createElement('option');
          		var elOptOld = box.options[index];
          		elOptNew.text = elOptOld.text;
          		elOptNew.value = elOptOld.value;
          		elOptNew.title = elOptOld.text;
         		  box.remove(index);
         		  elOptOld = box.options[box.options.length];
              try {
          			box.add(elOptNew, elOptOld); // standards compliant; doesn't work in IE
          		}
          		catch(ex) {
					box.add(elOptNew, box.options.length); // IE only 
          		}
           		box.selectedIndex = box.options.length-1;
          	}
        }
    }
}

function clearPresentationCheckboxes(form) {
	// assign a value to the clearAll
	form.clearAll.value = 1;
	form.step.value = "step2.asp";
	form.submit();
}

function addPresentationCheckBoxes(form,type,page) {
	// the user has clicked on the Step 1 link 
	if (type == 0) {	
		form.step.value = "";
		form.submit();
	}
	// we're just adding the selected figures to the dictionary - paging through the search results
	if (type == 1) {
		form.step.value = "step2.asp";
		form.page.value = page;
		form.submit();
	}
	// we have performed a search and we have results, but the user has clicked to go straight to Step 3
	// make sure they have checked at least one checkbox
	if (type == 3) {
		if (form) {
			form.step.value = "step3.asp";
			form.submit();
		}
		else {
			alert("You must select at least one clipping");
		}
	}
	// special case for when we're on the last Step (Step 3), and click on any of the other steps
	if (type == 4) {
		form.step.value = "step2.asp";
		//Select all the items in the listbox before submitting the form
		for(var n=0; n<document.docForm.clippingID.options.length; n++){
			document.docForm.clippingID.options[n].selected = true;
		}
		form.submit();
	}	
}
// this function checks/unchecks any checkbox within the clipping results (bottom listing) 
// if the user checks/unchecks the same checkbox within the "already added" listing (top listing)
// this will not check/uncheck the boxes if the "already added" result has come from a different page of results
function handleCheck(form, checkbox) {
	// get the value of the checked/unchecked checkbox from the added form
	var sourceCheckValue;
	var targetCheckValue;
	sourceCheckValue = checkbox.value;
	// get the number of checkboxes in the results form
	var checkboxCount = form.clipping.length;
	// loop through the checkboxes in the results form
	for (var idx = 0; idx < checkboxCount; idx++) {
		// find the same checkbox in the results form
		targetCheckValue = form.clipping[idx].value;
		// if the values are the same, we have found the checkbox
		if (sourceCheckValue == targetCheckValue) {
			if (form.clipping[idx].checked == true) {
				form.clipping[idx].checked = false;
			}
			else {
				form.clipping[idx].checked = true;
			}
		}
	}
}
//------------------------------------------------------------------------------------
//PBuilder functions - End
//------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
//MyTags Functions
//This function is used to delete a tag 
function confirmTagDeletion(vPath, vTag){
	if(confirm("You are about to delete a product tag.\nAre you sure you want to continue?")){	
		document.location.href = vPath+ escape(vTag);
		return true;
	}
}
//This function is used to delete a tag item
function confirmTagItemDeletion(vPath,vTag){
	if(confirm("You are about to delete a product tag item.\nAre you sure you want to continue?")){
		document.location.href = vPath+ escape(vTag);
		return true;
		}
}


//This function is used to delete a tag item in product listings
function confirmTagItemDeletionInProdList(vPath,vTag,vProductID){

//var test;
//test = decodeURI(vTag);
//alert(test);

	if(confirm("You are about to delete a product tag item.\nAre you sure you want to continue?")){
		document.location.href = vPath+ escape(vTag)+ '&R=' + vProductID;
		return true;
		}
}

	// function to validate tag name input  
function validTagName( FormName) 
	{ 
	if (trimAll(FormName.txtTag.value).length == 0) 
		{ 
		alert("Please provide a Tag name."); 
		FormName.txtTag.focus();
		return(false) 
		} 
	}
	
//This function trims white space from a supplied string
function trimAll(sString) 
	{
		while (sString.substring(0,1) == ' ')
			{
				sString = sString.substring(1, sString.length);
			}
		while (sString.substring(sString.length-1, sString.length) == ' ')
			{
				sString = sString.substring(0,sString.length-1);
			}
		return sString;
	}


// LOGOUT FUNCTIONALITY
function Logout(kcName){

	if (confirm("This action will log you out of the Knowledge Center. Click OK to continue."))
	{
		expireCookie("userdetails");
		expireCookie("USERGUID");
		expireCookie("CONTRACTGUID");
		expireCookie("kcSessionViewCode");
		expireCookie("kcSetHomePage");
		var refURL = "http://" + document.domain + "/kc/" + kcName + "/login/";
		window.location = refURL;
	}
}
			

function writeCookie(cookieName, cookieValue, expires, domain, path, secureFlag)
{
	if (cookieName)
	{
		var cookieDetails = cookieName + "=" + escape(cookieValue);
		cookieDetails += (expires ? "; expires=" + expires.toGMTString(): '');
		//cookieDetails += (domain ? "; domain=" + domain: '');
		cookieDetails += (path ? "; path=" + path: '');
		//cookieDetails += (secureFlag ? "; secure": '');
		document.cookie = cookieDetails;
	}
}
			
function expireCookie(cookieName)
{
	var expiredDate = new Date();
	expiredDate.setMonth(-1);
	writeCookie(cookieName,"",expiredDate,"","/","");
}	

function setCookieValue(cookieName, cookieValue, path)
{
	if (cookieName)
	{
		var cookieDetails = cookieName + "=" + escape(cookieValue);
		cookieDetails += (path ? "; path=" + path: '');
		document.cookie = cookieDetails;
	}
}

function validateWatchListSearch(form)
{
	var searchPhrase;
	var errorText;
	errorText = "";
	searchPhrase = form.txtSearch.value;
	
	if(searchPhrase.length < 2)
	{
		alert("Your search phrase should be at least 2 characters long.");		
	}
	else
	{
		form.submit();
	}
	
}
function validateWatchListKeyword(form)
{
	var keyword;
	keyword = form.kid.value;
	if(keyword.length < 2){
		alert("Your keyword should be at least 2 characters long.");		
	}
	else{
		form.submit();
	}
	
}

// View Code functionality
function checkForm()
{
	if ((getCheckedValue() =="Yes") && (document.form1.txtViewCode.value ==''))
	{
		alert ("Please enter a view code" + '\n' + "otherwise select the other option if no view code is required");
		document.form1.txtViewCode.focus();
		return false
	}
		else
	{
		return true
	}
}
function getCheckedValue()
{
	for (i=0;i<document.form1.rdoVC.length;i++)
	{
		if (document.form1.rdoVC[i].checked)
		 {
			ValueChecked = document.form1.rdoVC[i].value;
		}
	}
	return ValueChecked;
}
function sortForm()
{
	if (getCheckedValue() == "No")
		{
			document.form1.txtViewCode.disabled = true
		}
		else
		{
			document.form1.txtViewCode.disabled = false
		}
}
function clearForm()
{
		if (getCheckedValue() == "Yes")
		{
			document.form1.txtViewCode.value = '';
		}
}

// checks the Edit My Profile Form for errors
function validateEditMyProfile(form)
{
	var title;
	var firstName;
	var lastName;
	var jobFunction;
	var company;
	var companyType;
	var address;
	var town;
	var postCode;
	var country;
	var email;
	var verifyEmail;
	var marketingInfo;
	var errorText;
	errorText = ""
	
	form.title.options[form.title.selectedIndex].value;
	
	/*
	Ensure that <SELECT> element validation work with NETSCAPE 4.x
	*/
	
	if (form.title.selectedIndex == -1) {
		errorText = errorText + "Please enter a Title\n";
		form.title.focus();
	}
	else {
		title = form.title.options[form.title.selectedIndex].value; //form.title.value;
	}
	
	firstName = form.firstName.value;

	if (firstName.length == 0)
	{
		errorText = errorText + "Please enter your First name\n";
		form.firstName.focus();
	}

	lastName = form.lastName.value;
	if (lastName.length == 0)
	{
		errorText = errorText + "Please enter your Last name\n";
		form.lastName.focus();
	}

	if (form.jobFunction.selectedIndex == -1) {	
		errorText = errorText + "Please enter your Job Function\n";
		form.jobFunction.focus();
	}
	else {
		jobFunction = form.jobFunction.options[form.jobFunction.selectedIndex].value; //form.jobFunction.value;
	}

	company = form.company.value;
	if (company.length == 0)
	{
		errorText = errorText + "Please enter your Company name\n";
		form.company.focus();
	}

	if (form.companyType.selectedIndex == -1)
	{
		errorText = errorText + "Please enter your Company type\n";
		form.companyType.focus();
	}
	else {
		companyType = form.companyType.options[form.companyType.selectedIndex].value  //form.companyType.value;
	}
		
	address = form.address.value;
	if (address.length == 0)
	{
		errorText = errorText + "Please enter your Address\n";
		form.address.focus();
	}
	
	town = form.city.value;
	if (town.length == 0)
	{
		errorText = errorText + "Please enter your Town/City\n";
		form.city.focus();
	}
	
	postCode = form.postcode.value;
	if (postCode.length == 0)
	{
		errorText = errorText + "Please enter your Postal/Zip Code\n";
		form.postcode.focus();
	}
	
	if (form.country.selectedIndex == -1)
	{
		errorText = errorText + "Please enter a Country\n";
		form.country.focus();
	}
	else {
		country = form.country.options[form.country.selectedIndex].value //form.country.value;	
	}
	
	email = form.email.value;
	
	verifyEmail = form.verifyEmail.value;
	
	if (email.length == 0)
	{
		errorText = errorText + "Please enter an Email address\n";
		form.email.focus();
	}
	else
	{
		if (checkEmail(email) == false) 
		{
			errorText = errorText + "Please enter a valid email address\n";
			form.email.focus();
		}
	}
	if (verifyEmail.length == 0)
	{
		errorText = errorText + "Please enter a Verify Email address\n";
		form.verifyEmail.focus();
	}
	else
	{
		if (checkEmail(verifyEmail) == false) 
		{
			errorText = errorText + "Please enter a valid Verify email address\n";
			form.verifyEmail.focus();
		}
	}
	if (email != verifyEmail)
	{
		errorText = errorText + "Your email addresses must match\n";
		form.email.focus();
	}

	marketingInfo = form.marketingInfo.value;
	
	if (errorText.length != 0)
	{
		alert(errorText);
	}
	else
	{
		form.submit();
	}
		
}







// View Code functionality
function checkForm()
{
	if ((getCheckedValue()=="Yes") && (document.form1.txtViewCode.value ==''))
	{
		alert ("Please enter a view code" + '\n' + "or select the other option");
		document.form1.txtViewCode.focus();
		return false;
	}
		else
	{
		return true;	
	}
}

// View Code functionality
function checkVC()
{
if (document.form1.txtViewCode.value=='')
			{
			alert ("Please enter a view code");
			document.form1.txtViewCode.focus();
			return false;
			}
	else
			{
			return true;	
			}
}

function clearVC()
{
	document.form1.txtViewCode.value = '';
}

function submitForm(strStatus)
{
	if (strStatus == 'notRestricted')
	{
		if (checkForm())
		{
			document.form1.submit();
		}
	}
	else
	{
		if (checkVC())
		{
			document.form1.submit();
		}
		// Else the form should not be submitted
	}
}

function getCheckedValue()
{
	for (i=0;i<document.form1.rdoVC.length;i++)
	{
		if (document.form1.rdoVC[i].checked)
		 {
			ValueChecked = document.form1.rdoVC[i].value;
		}
	}
	return ValueChecked;
}

function sortForm()
{
	if (getCheckedValue() == "No")
		{
			document.form1.txtViewCode.disabled = true
		}
		else
		{
			document.form1.txtViewCode.disabled = false
		}
}
function clearForm()
{
		if (getCheckedValue() == "Yes")
		{
			document.form1.txtViewCode.value = '';
		}
}
function disableEnterKey(e)
{
    var key;
    
    if(window.event)
     {
        key = window.event.keycode;
     }
     else
     {
        key = e.which;
     }
     return (key != 13);
}

function toggleLayer( whichLayer )
{
    var elem, vis;
	if( document.getElementById )
	    elem = document.getElementById( whichLayer );
	else
	    if( document.all )
		    elem = document.all[whichLayer];
		else
		    if( document.layers )
			    elem = document.layers[whichLayer];
				vis = elem.style;
				if(vis.display=='' && elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)
					vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';
					vis.display = (vis.display==''||vis.display=='block')?'none':'block';
    if(whichLayer == 'display_options')
    {
        changeDisplay('display_options_tab');
    }
}

function changeDisplay(whichLayer)
{
    var elem, vis;  
    if( document.getElementById ) 
    elem = document.getElementById(whichLayer);  
    else if( document.all ) 
    elem = document.all[whichLayer];  
    else if( document.layers ) 
    elem = document.layers[whichLayer];
    if(elem != null)
    {
        if(elem.className == '')
        {
            elem.className = 'tabdown';
        }
        else
        {
            elem.className = '';
        }
    }
}

function AddRemoveCSSClass(element, cssClassToRemove, cssClassToAdd)
{
    var objElement = $(element);
    objElement.removeClass(cssClassToRemove);
    objElement.addClass(cssClassToAdd);

}

// autocomplete - requires scriptaculous library

if(typeof(SiteNValue) != 'undefined')
{
// Philip Morris - 03 June 2009
document.observe("dom:loaded", function() 
{
  new Ajax.Autocompleter("searchKeywords", "autocomplete_choices", 
    "/SearchTermHandler.aspx", {paramName: "SearchTerm", minChars: 3, parameters: "SiteNValue=" + SiteNValue + "&KcURL=" + KcURL});
});


// Optional sorting of tag-cloud links on importance descending and CSS class switching..
document.observe("dom:loaded", function()
{
    if ($('tag-cloud-zzzzzzzzzz')) //should be tag-cloud to reinstate
    {
        // Build up array of tag objects (for sorting and regenerating markup)
        var tags = $$('#tag-cloud a').collect(
            function(listAnchor, index) 
            {
                return {
                    position: index,
                    //weighting: $$('#tag-cloud span').innerHTML,
                    weighting: parseInt(listAnchor.getAttribute("title").substring(0, 3), 10),
                    //weighting: parseInt(listAnchor.getAttribute("weighting"), 10),
                    a: listAnchor
                };
            }
        );
        
        // Reusable function to empty list and repopulate from given tags
        var updateList = function(list, tags) {
            // Remove all anchors
            tags.each(
                function(tag) { tag.a.parentNode.removeChild(tag.a); }
            );
            
            // Repopulate lis from anchors in order of tags array
            list.select("li").each(
                function(li, index) {
                    li.update(tags[index].a);
                }
            );
        };

        function eventHandler()
        {
            var list = $('tag-cloud');
            
            if (list.hasClassName('alt')) 
            {
                list.removeClassName('alt');
                
                // Sort by position
                tags.sort(
                    function(tagA, tagB) {
                        return tagA.position - tagB.position;
                    }
                );
                updateList(list, tags);
            }
            else
            {
                list.addClassName('alt');
                
                // Sort by weighting
                tags.sort(
                    function(tagA, tagB) {
                        return tagA.weighting - tagB.weighting;
                    }
                );
                tags.reverse();
                updateList(list, tags);
            }
        }
        
        var styleAttributes = {
            href   : "#non-existent-anchor",
            id     : "tag-cloud-switch",
            title  : "Toggle cloud/list view.."
        };
        var styleSwitcher = new Element('a', styleAttributes);
        styleSwitcher.update('Change appearance');
        Element.insert($('tag-cloud'), {after: styleSwitcher});
        Event.observe($(styleAttributes.id), 'click', eventHandler, false);
    }
});
}