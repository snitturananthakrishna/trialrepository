using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Datamonitor.PremiumTools.Prototype.Error
{
    public partial class Default : Page
    {
        //Members
        string code;
        string servername;
        Exception objError;
        Type exType;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
            LoadMessage();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        private void LoadData()
        {
            servername = Request.ServerVariables["HTTP_HOST"].ToString();

            try
            {
                code = Request["code"].ToString();
            }
            catch
            {
                code = "0";
            }

            objError = Server.GetLastError();

            if (objError != null)
            {
                objError = objError.InnerException != null ? objError.InnerException : objError;
            }
            else
            {
                objError = new Exception("Unknown");
            }

            exType = objError.GetType();
        }

        /// <summary>
        /// Loads the message.
        /// </summary>
        private void LoadMessage()
        {
            switch (code)
            {
              
            }
        }
    }
}
