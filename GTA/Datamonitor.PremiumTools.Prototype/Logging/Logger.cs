using System;
using System.Data;
using System.Configuration;

namespace Datamonitor.PremiumTools.Prototype.Logging
{
    /// <summary>
    /// This class has utility methods to log errors that occur in the application
    /// </summary>
    public static class Logger
    {
        public enum LoggingType // enum is accessed to provide the operation to be done
        {
            EventLog,
            SendMail,
            MailAndEventLog
        }


        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public static void LogError(Exception ex)
        {
            //Build the error message
            string message = "Date : " +
                DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture) +
                "  Error : " +
                ex.Message;

            //Save the error to the log file
            ErrorHandler.SaveToEventLog(message, ex);
        }


        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="ex">The exception.</param>
        public static void LogError(Exception ex, LoggingType logtypes)
        {
            //Build the error message
            string message = "Date : " +
                DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture) +
                "  Error : " +
                ex.Message;

            switch (logtypes)
            {
                case LoggingType.EventLog:
                    //Save the error to the log file
                    ErrorHandler.SaveToEventLog(message, ex);
                    break;
                case LoggingType.SendMail:
                    //Mail the error message
                    ErrorHandler.SendErrorMail(message, ex);
                    break;
                case LoggingType.MailAndEventLog:
                    //Save the error to the log file
                    try
                    {
                        ErrorHandler.SaveToEventLog(message, ex);
                    }
                    catch
                    {

                    }
                    //Mail the error message
                    ErrorHandler.SendErrorMail(message, ex);
                    break;
            }

        }
    }
}
