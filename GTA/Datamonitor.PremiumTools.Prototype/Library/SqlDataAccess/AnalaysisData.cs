using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess
{
    public class AnalaysisData: DataServiceBase
    {
        /// <summary>
        /// Gets the axis data.
        /// </summary>
        /// <param name="axisTypeIDs">The axis type Ids.</param>
        /// <param name="includeYears">if set to <c>true</c> [include years].</param>
        /// <returns></returns>
        public static DataSet GetAxisData(string axisTypeIDs, bool includeYears)
        {            
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@typeIDs", axisTypeIDs));
            sqlParams.Add(new SqlParameter("@includeYears", includeYears));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, 
                "GetTaxonomyTypeByIDs", 
                sqlParams.ToArray());

            return ds;
        }
    }
    
}
