using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class TaxonomyDefinition : System.Web.UI.Page
    {
        private string _coverageRange;

            public string CoverageRange
            {
                get { return _coverageRange; }
                set { _coverageRange = value; }
            }

        //PAGE EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            CoverageRange = "";
            DefinitionDiv.Visible = false;
            CoverageRangeDiv.Visible = false;

            //Check 'taxonomyid' is there in query string
            if (Request.QueryString["taxonomyid"] != null && !string.IsNullOrEmpty(Request.QueryString["taxonomyid"]))
            {
                //Get the taxonomy definition for the taxonomyid
                try
                {
                    //Get selected countryids string
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>("1");
                    string CountryIDs = "";
                    
                    if (selectedIDsList != null)
                    {
                        foreach (KeyValuePair<int, string> CurrentValue in selectedIDsList)
                        {
                            CountryIDs += CurrentValue.Key + ",";
                        }
                    }

                    DataSet TaxonomyDefinition = SqlDataService.GetTaxonomyDefinition(Convert.ToInt32(Request.QueryString["taxonomyid"].ToString()), CountryIDs);

                    if (TaxonomyDefinition != null &&
                        TaxonomyDefinition.Tables.Count > 0 &&
                        TaxonomyDefinition.Tables[0].Rows.Count > 0)
                    {
                        //Bind the taxonomy definition details to labels
                        if (!string.IsNullOrEmpty(TaxonomyDefinition.Tables[0].Rows[0]["Definition"].ToString()))
                        {
                            Indicator.Text = TaxonomyDefinition.Tables[0].Rows[0]["Indicator"].ToString();
                            Definition.Text = TaxonomyDefinition.Tables[0].Rows[0]["Definition"].ToString();
                            DefinitionDiv.Visible = true;
                        }

                        //Bind coverage range information
                        if (!string.IsNullOrEmpty(TaxonomyDefinition.Tables[0].Rows[0]["minyear"].ToString()) || !string.IsNullOrEmpty(TaxonomyDefinition.Tables[0].Rows[0]["maxyear"].ToString()))
                        {
                            CoverageRange = TaxonomyDefinition.Tables[0].Rows[0]["minyear"].ToString() + " - " + TaxonomyDefinition.Tables[0].Rows[0]["maxyear"].ToString();
                            CoverageRangeDiv.Visible = true;
                        }
                    }
                }
                catch(Exception ex) 
                {
                }
            }
        }
    }
}
