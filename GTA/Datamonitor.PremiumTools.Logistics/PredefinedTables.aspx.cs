using System;
using System.Text;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class PredefinedTables : AuthenticationBasePage
    {
         // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
            {                               
                //LoadSavedSearches
                ((SavedSearch)SavedSearch1).LoadSavedSearches(GlobalSettings.DefaultResultsPage);

                PopulatePredefinedTablesList();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.CustomPageTitle = "League Tables";
        }

        
        // PRIVATE METHODS (initial population)
        private void PopulatePredefinedTablesList()
        {
            DataSet PredefinedTables = SqlDataService.GetPredefinedTablesList();

            if (PredefinedTables != null &&
                PredefinedTables.Tables.Count > 0 &&
                PredefinedTables.Tables[0].Rows.Count > 0)
            {
                PredefinedTablesList.DataSource = PredefinedTables;
                PredefinedTablesList.DataBind();
            }
        }
       
        //PUBLIC METHODS



        // EVENT HANDLERS
        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            Response.Redirect(GlobalSettings.DefaultResultsPage);
        }

        protected void AnalysisLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("analysis/countrycomparison.aspx");
        }
        protected void BuidSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/" + GlobalSettings.DefaultSearchPage, true);
        }
    }
}
