using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Generic.Logging;
using Datamonitor.PremiumTools.Generic.Library;
using System.Collections.Generic;

namespace Datamonitor.PremiumTools.Generic
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception error = Server.GetLastError();
            if (!(error is HttpException))
            {
                Logger.LogError(error, Logger.LoggingType.EventLog);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Session_Start(object sender, EventArgs e)
        {
            string TaxonomyTypeID;
            string TaxonomyType ;
            int DefaultTaxonomyID;
            string DefaultTaxonomy ;
            //Get default taxonomy types
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyDefaults();
           
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                foreach (DataRow Row in TaxonomyTypes.Tables[0].Rows)
                {
                    TaxonomyTypeID = Row["ID"].ToString();
                    TaxonomyType = Row["Name"].ToString();                    

                    if (Int32.TryParse(Row["DefaultID"].ToString(), out DefaultTaxonomyID))
                    {
                        DefaultTaxonomy = Row["DefaultTaxonomy"].ToString();
                       
                        Dictionary<int, string> CurrentSelections = new Dictionary<int, string>();
                        CurrentSelections.Add(DefaultTaxonomyID, DefaultTaxonomy);
                        CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyTypeID, CurrentSelections);
                    }
                }
            }


            if (!GlobalSettings.IsSSOEnabled)
            {
                Session["SourceKC"] = "consumer";
            }

            Session["sessionkey"] = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Start(object sender, EventArgs e)
        {
            //// Code that runs on application startup
            Aspose.Cells.License cellLicense = new Aspose.Cells.License();
            string filename = Server.MapPath("~/Aspose.Custom.lic");
            cellLicense.SetLicense(filename);
            Aspose.Words.License wordLicense = new Aspose.Words.License();
            wordLicense.SetLicense(filename);
            Aspose.Slides.License slideLicense = new Aspose.Slides.License();
            slideLicense.SetLicense(filename);
            Application["Root"] = System.Configuration.ConfigurationManager.AppSettings["Root"];
        }
    }
}