using System;
using System.IO;
using System.Data;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// Session handler to store session data
    /// </summary>  
    public class SessionHandler : IHttpHandler, IRequiresSessionState
    {
        /// <summary>The query string parameter to use for the taxonomy type id.</summary>
        public const string TAXONOMY_TYPE_ID_PARAM = "taxonomyTypeID";

        /// <summary>The query string parameter to use for the taxonomy id.</summary>
        public const string TAXONOMY_ID_PARAM = "taxonomyID";

        /// <summary>The query string parameter to use for the name.</summary>
        public const string NAME_PARAM = "name";

        /// <summary>The query string parameter to use for the action type.</summary>
        public const string ACTION_TYPE_PARAM = "actionType";

        /// <summary>The query string parameter to use to decide whether the taxonomy type has subcategories or not.</summary>
        public const string HASSUBTAXONOMYTYPES_TYPE_PARAM = "hasSubcategory";

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {

            // Get querystring params               
            string taxonomyTypeID = context.Request.QueryString[TAXONOMY_TYPE_ID_PARAM];
            string taxonomyID = context.Request.QueryString[TAXONOMY_ID_PARAM];
            string name = context.Request.QueryString[NAME_PARAM];
            string actionType = context.Request.QueryString[ACTION_TYPE_PARAM];
            string HasSubCategory = context.Request.QueryString[HASSUBTAXONOMYTYPES_TYPE_PARAM];

            string status = UpdateSessionWithSelections(taxonomyTypeID,
                taxonomyID,
                name,
                actionType,
                HasSubCategory);
            
            //Return status
            context.Response.Clear();
            context.Response.CacheControl = "no-cache";
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.Expires = -1;            
            context.Response.ContentType = "text/plain";
            context.Response.Write(status);
        }

        /// <summary>
        /// Updates the session with selections.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="name">The name.</param>
        /// <param name="actionType">Type of the action.</param>
        /// <param name="HasSubCategory">The has sub category.</param>
        /// <returns></returns>
        public static string UpdateSessionWithSelections(string taxonomyTypeID,
            string taxonomyID,
            string name,
            string actionType,
            string HasSubCategory)
        {
            string status = string.Empty;

            name = Microsoft.JScript.GlobalObject.unescape(name);
            // Validate params
            if (string.IsNullOrEmpty(actionType))
            {
                throw new ArgumentNullException(ACTION_TYPE_PARAM, "The Action Type must be supplied on the querystring");
            }
            if (!actionType.Contains("addGroupSelection") && string.IsNullOrEmpty(taxonomyTypeID))
            {
                throw new ArgumentNullException(TAXONOMY_TYPE_ID_PARAM, "The taxonomy type id must be supplied on the querystring");
            }
            if (!actionType.Contains("addGroupSelection") && string.IsNullOrEmpty(taxonomyID))
            {
                throw new ArgumentNullException(TAXONOMY_ID_PARAM, "The taxonomy id must be supplied on the querystring");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(NAME_PARAM, "The name must be supplied on the querystring");
            }

            if (actionType == "removeAll")
            {
                //Clear all information in the session
                CurrentSession.ClearSelectionsFromSession();
            }
            else if (actionType == "yearsUpdate")
            {
                if (name.Equals("startyear", StringComparison.CurrentCultureIgnoreCase))
                {
                    CurrentSession.SetInSession<int>("StartYear", Convert.ToInt32(taxonomyID));
                }
                else if (name.Equals("endyear", StringComparison.CurrentCultureIgnoreCase))
                {
                    CurrentSession.SetInSession<int>("EndYear", Convert.ToInt32(taxonomyID));
                }
            }
            else if (actionType == "PredefinedView")
            {
                //Clear existing taxonomy selections
                CurrentSession.ClearSelectionsFromSession();
                if (taxonomyID != "0")
                {
                    CurrentSession.SetInSession<string>("PredefinedViewID", taxonomyID);
                }
            }
            else
            {
                if (actionType.Equals("add") || actionType.Equals("remove"))
                {
                    status = AddOrRemoveTaxonomySelection(taxonomyTypeID,
                       Convert.ToInt32(taxonomyID),
                       name,
                       actionType,
                       HasSubCategory);
                }
                else if (actionType.Contains("addGroupSelection"))
                {                    
                    // Remove all taxonomy type's session
                    DataSet TaxonomyTypes;
                    if (actionType.Equals("addGroupSelectionQS"))
                    {
                        TaxonomyTypes = GlobalSettings.GetQuickSearchTaxonomyLinks();
                    }
                    else
                    {
                        TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
                    }
                    actionType = "add";
                    if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
                    {
                        foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
                        {
                            if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())
                            {
                                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
                            }
                            else
                            {
                                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["ID"].ToString(), null);
                                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
                            }
                        }
                    }
                    //Add the current user selections to session
                    string[] GroupSelections = name.Split(new char[] { '|' } , StringSplitOptions.RemoveEmptyEntries);

                    foreach (string Selection in GroupSelections)
                    {
                        string[] SingleSelection = Selection.Split(new char[] { '~' } , StringSplitOptions.RemoveEmptyEntries);

                        AddOrRemoveTaxonomySelection(SingleSelection[0],
                           Convert.ToInt32(SingleSelection[1]),
                           SingleSelection[2],
                           actionType,
                           HasSubCategory);
                    }
                }
            }

            return status;
        }

        /// <summary>
        /// Gets the current selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        private static Dictionary<int, string> GetCurrentSelection(string taxonomyTypeID)
        {
            Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyTypeID);
            if (CurrentSelections == null)
            {
                CurrentSelections = new Dictionary<int, string>();
            }
            return CurrentSelections;           
           
        }        

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the or remove taxonomy selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="name">The name.</param>
        /// 
        /// <param name="actionType">Type of the action.</param>
        /// <returns></returns>
        private static string AddOrRemoveTaxonomySelection(string taxonomyTypeID, 
            int taxonomyID, 
            string name, 
            string actionType,
            string hasSubCategory)
        {            
            string Status = string.Empty;
            
            //Get taxonomy type object based on the input taxonomy type id
            Dictionary<int, string> CurrentSelections = GetCurrentSelection(taxonomyTypeID);

            if (CurrentSelections.ContainsKey(Convert.ToInt32(taxonomyID)))
            {
                if (actionType.Equals("add"))
                {
                    Status = "false";
                }
                else if (actionType.Equals("remove"))
                {
                    DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
                    if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
                    {
                        foreach (DataRow Taxonomy in TaxonomyTypes.Tables[0].Rows)
                        {
                            if (taxonomyTypeID == Taxonomy["ID"].ToString())
                            {                                
                                //Remove selection from the dictionary
                                CurrentSelections.Remove(Convert.ToInt32(taxonomyID));
                                if (CurrentSelections.Count == 0)
                                {
                                    CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, null);
                                }
                                else
                                {
                                    //update session                  
                                    CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);
                                }

                                Dictionary<int, string> CurrentSubSelections = GetCurrentSelection(Taxonomy["FullID"].ToString());
                                //Remove sub selection from the dictionary
                                CurrentSubSelections.Remove(Convert.ToInt32(taxonomyID));
                                if (CurrentSelections.Count == 0)
                                {
                                    CurrentSession.SetInSession<Dictionary<int, string>>(Taxonomy["FullID"].ToString(), null);
                                }
                                else
                                {
                                    //update session                  
                                    CurrentSession.SetInSession<Dictionary<int, string>>(Taxonomy["FullID"].ToString(), CurrentSubSelections);
                                }
                                Status = "true";
                            }                            
                        }
                    }                   
                }
            }
            else
            {
                if ((actionType.Equals("add")) || (actionType.Equals("update")))
                {                    
                    //Add selections to the dictionary
                    CurrentSelections.Add(taxonomyID, name);                                        
                     
                    //update session                  
                    CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);

                    if (!string.IsNullOrEmpty(hasSubCategory) && hasSubCategory == "true")
                    {
                        DataSet SubTaxonomyTypeID = SqlDataService.GetSubTaxonomyTypeID(taxonomyID);
                        if (SubTaxonomyTypeID != null &&
                            SubTaxonomyTypeID.Tables.Count > 0 &&
                            SubTaxonomyTypeID.Tables[0].Rows.Count > 0)
                        {
                            taxonomyTypeID = SubTaxonomyTypeID.Tables[0].Rows[0][0].ToString();
                        }
                    }

                    CurrentSelections = GetCurrentSelection(taxonomyTypeID);
                    if (!CurrentSelections.ContainsKey(Convert.ToInt32(taxonomyID)))
                    {
                        //Add selections to the dictionary
                        CurrentSelections.Add(taxonomyID, name);

                        //update session                  
                        CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);
                    }

                    Status = "true";
                }
                if (actionType.Equals("remove"))
                {
                    Status = "false";
                }
            }
            
            return Status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxonomyID"></param>
        /// <param name="actionType"></param>
        public static void savedSearchCriteria(string taxonomyID,string actionType,string columnID,string name,int i)
        {
            // Remove all taxonomy type's session
            //DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();           
            //if (i == 0)
            //{
            //    if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            //    {
            //        foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
            //        {
            //            if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())
            //            {
            //                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
            //            }
            //            else
            //            {
            //                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["ID"].ToString(), null);
            //                CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
            //            }
            //        }
            //    }                
            //}
            //Add the current user selections to session
            string[] GroupSelections = name.Split(new char[] { '|' } , StringSplitOptions.RemoveEmptyEntries);

            foreach (string Selection in GroupSelections)
            {
                string[] SingleSelection = Selection.Split(new char[] { '~' } , StringSplitOptions.RemoveEmptyEntries);

                string status =  AddOrRemoveTaxonomySelectionForSavedSearch(SingleSelection[0],
                   Convert.ToInt32(SingleSelection[1]),
                   SingleSelection[2],
                   actionType,
                   "true");
            }                    
        }

        /// <summary>
        /// Adds the or remove taxonomy selection for saved searches.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="name">The name.</param>
        /// 
        /// <param name="actionType">Type of the action.</param>
        /// <returns></returns>
        private static string AddOrRemoveTaxonomySelectionForSavedSearch(string taxonomyTypeID,
            int taxonomyID,
            string name,
            string actionType,
            string hasSubCategory)
        {
            string Status = string.Empty;

            //Get taxonomy type object based on the input taxonomy type id
            Dictionary<int, string> CurrentSelections = GetCurrentSelection(taxonomyTypeID);
           
            if (actionType.Equals("update"))
            {
                //Add selections to the dictionary
                CurrentSelections.Add(taxonomyID, name);

                //update session                  
                CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);

                if (!string.IsNullOrEmpty(hasSubCategory) && hasSubCategory == "true")
                {
                    DataSet SubTaxonomyTypeID = SqlDataService.GetSubTaxonomyTypeID(taxonomyID);
                    if (SubTaxonomyTypeID != null &&
                        SubTaxonomyTypeID.Tables.Count > 0 &&
                        SubTaxonomyTypeID.Tables[0].Rows.Count > 0)
                    {
                        taxonomyTypeID = SubTaxonomyTypeID.Tables[0].Rows[0][0].ToString();
                    }
                }

                CurrentSelections = GetCurrentSelection(taxonomyTypeID);
                if (!CurrentSelections.ContainsKey(Convert.ToInt32(taxonomyID)))
                {
                    //Add selections to the dictionary
                    CurrentSelections.Add(taxonomyID, name);

                    //update session                  
                    CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);
                }

                Status = "true";
            }           
            return Status;
        }
    }
}
