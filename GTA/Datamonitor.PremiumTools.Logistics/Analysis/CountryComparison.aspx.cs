using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using dotnetCHARTING;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class CountryComparison : AuthenticationBasePage
    {

        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                !ChartsCallback.IsCallback)
            {                
                hiddenChartType.Value = "3";
                StartYearDropDown.Attributes.Add("onchange", "javascript:Years_onChange('startyear');");
                EndYearDropDown.Attributes.Add("onchange", "javascript:Years_onChange('endyear');");
                ChartTypeDropdown.Attributes.Add("onchange", "javascript:OnChartTypeChange(3);");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";
            
            BindControls();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!ChartsCallback.IsCallback)
            {  
                PlotChart_Click(null, null);                
            }
        }

        // PRIVATE METHODS (initial population)       

        private void BindControls()
        {
            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet IndicatorsAndCountries = SqlDataService.GetIndicatorsAndCountries(SelectionsXML);

            if (IndicatorsAndCountries != null &&
                IndicatorsAndCountries.Tables.Count > 1&&
                IndicatorsAndCountries.Tables[0].Rows.Count>0&&
                IndicatorsAndCountries.Tables[1].Rows.Count > 0)
            {
                ChartResultsStatus.Visible = false;
                ChartResultsDiv.Visible = true;

                //binding indicator data
                foreach (DataRow dbRow in IndicatorsAndCountries.Tables[0].Rows)
                {
                    ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["DisplayName"].ToString(),
                          dbRow["rowID"].ToString(),
                          true,
                          false,
                          false);

                    IndicatorTree.Nodes.Add(newNode);
                }
                
                //binding Country data
                foreach (DataRow dbRow in IndicatorsAndCountries.Tables[1].Rows)
                {
                    ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Country"].ToString(),
                          dbRow["CountryID"].ToString(),
                          true,
                          true,
                          false);

                    CountryTree.Nodes.Add(newNode);
                }
                SelectDefaultsForCheckBoxCombo(CountryCombo, CountryTree, hdnCountry);
                hdnCountryText.Value = CountryCombo.Text;
            }
            else
            {
                ChartResultsStatus.Visible = true;
                ChartResultsDiv.Visible = false;
            }
            
            hdnIndicator.Value = IndicatorTree.Nodes.Count > 0 ? IndicatorTree.Nodes[0].Value : "0";
            IndicatorCombo.Text = IndicatorTree.Nodes.Count > 0 ? IndicatorTree.Nodes[0].Text : "";
            hdnIndicatorText.Value = IndicatorTree.Nodes.Count > 0 ? IndicatorTree.Nodes[0].Text : "";

            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            //Start year combo
            //End year combo
            for (int counter = StartYear; counter <= EndYear; counter++)
            {
                StartYearDropDown.Items.Add(new ListItem(counter.ToString()));
                EndYearDropDown.Items.Add(new ListItem(counter.ToString()));
            }
            StartYearDropDown.SelectedValue = StartYear.ToString();
            EndYearDropDown.SelectedValue = EndYear.ToString();
            hdnStartYear.Value = StartYear.ToString();
            hdnEndYear.Value = EndYear.ToString();

        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="multipleFilterTree"></param>
        /// <param name="hdnMultiple"></param>
        private void SelectDefaultsForCheckBoxCombo(CArt.ComboBox combo,
            CArt.TreeView tree,
            HiddenField hdnField)
        {
            int DefaultSelectionsCount = 5;
            combo.Text = "";
            hdnField.Value = "";
            foreach (CArt.TreeViewNode tnode in tree.Nodes)
            {
                if (DefaultSelectionsCount <= 0) break;
                if (tnode.ShowCheckBox)
                {
                    combo.Text += tnode.Text + ";";
                    hdnField.Value += tnode.Value + ",";
                    tnode.Checked = true;
                    DefaultSelectionsCount--;
                }
            }
            combo.Text = combo.Text.TrimEnd(new char[] { ';' });
        }

        public string getTaxonomyFilterPage()
        {
            return GlobalSettings.SearchPage.ToLower().Contains("search1") ? "TaxonomyFilter1.aspx" : "TaxonomyFilter.aspx";
        }

        // EVENT HANDLERS
        protected void PlotChart_Click(object sender, EventArgs e)
        {   
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetCountryComparisonChartData(Int64.Parse(hdnIndicator.Value),
                hdnCountry.Value.TrimEnd(new char[]{','}),
               Convert.ToInt32(StartYearDropDown.SelectedValue),
               Convert.ToInt32(EndYearDropDown.SelectedValue));

            LogUsage("Charts - country Comparison", hdnIndicator.Value, hdnCountry.Value.TrimEnd(new char[] { ',' }));

            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartSeriesType = ChartTypeDropdown.SelectedValue; //LineRadio.Checked ? "3" : "7";
                
                dotnetCHARTING.Chart targetChart;

                //Plot chart
                targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        ChartDataset.Tables[0].Rows[0][3].ToString(),
                        "Indicator: "+ IndicatorCombo.Text);

                //Check whether the series type is stacked or not
                if (ChartSeriesType.Equals("stacked"))
                {
                    TwoDChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                }

                targetChart.Visible = false;

                //Add copyright information
                TwoDimensionalCharts.AddCopyrightText(targetChart, 370);

                //Make sure the temporary folder exists
                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }

                //Set the temporary folder path
                targetChart.FileManager.TempDirectory = "temp";

                // Save the legend as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.BottomMiddle;
                if (targetChart.SeriesCollection.Count > 3)
                {
                    targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                }
                LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;

                //Set the Chart size
                targetChart.Width = Unit.Point(520);
                targetChart.Height = Unit.Point(300);

                // Remove the legend from the chart and save the chart as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.None;
                ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                ImageMapLabel.Text = targetChart.ImageMapText;

                //Save the chart in viewstate, used in extractions.
                ViewState["2DChartImage"] = ChartImg.ImageUrl;
                hdnImageurl.Value = ChartImg.ImageUrl;
                hdnLegendUrl.Value = LegendImg.ImageUrl;
            }          
        }              

        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            if (e.Parameters[1].ToString() != "")
            {
                //Get chart data 
                DataSet ChartDataset = SqlDataService.GetCountryComparisonChartData(Int64.Parse(e.Parameters[0].ToString()),
                    e.Parameters[1].ToString().TrimEnd(new char[] { ',' }),
                    Int32.Parse(e.Parameters[3].ToString()),
                    Int32.Parse(e.Parameters[4].ToString()));

                #region Plot chart
                
                if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
                {
                    string ChartSeriesType = e.Parameters[2].ToString();

                    dotnetCHARTING.Chart targetChart;

                    //Plot chart
                    targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                            TwoDChart,
                            ChartSeriesType,
                            ChartDataset.Tables[0].Rows[0][3].ToString(),
                            "Indicator: " + e.Parameters[5].ToString());

                    //Check whether the series type is stacked or not
                    if (ChartSeriesType.Equals("stacked"))
                    {
                        TwoDChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                    }

                    //Add copyright information
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);

                    targetChart.Visible = false;

                    //Make sure the temporary folder exists
                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    //Set the temporary folder path
                    targetChart.FileManager.TempDirectory = "temp";

                    // Save the legend as a separate image file
                    targetChart.LegendBox.Position = LegendBoxPosition.BottomMiddle;
                    if (targetChart.SeriesCollection.Count > 3)
                    {
                        targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                    }
                    string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                    //Set the Chart size
                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);

                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = LegendBoxPosition.None;
                    string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

                    ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);

                    //usemap
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;

                    //Render the images (chart image and seperate legend image)
                    ChartImg.RenderControl(e.Output);
                    hdnImageurl.Value = strChartPath;
                    hdnImageurl.RenderControl(e.Output);
                    ImageMapLabel.RenderControl(e.Output);

                    LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    divChart.RenderControl(e.Output);
                    hdnLegendUrl.Value = strLegendPath;
                    hdnLegendUrl.RenderControl(e.Output);
                }
                #endregion
            }
        }
        
        //EXPORT EVENT HANDLERS       
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetCountryComparisonChartData(Int64.Parse(hdnIndicator.Value),
                hdnCountry.Value.TrimEnd(new char[] { ',' }),
                Int32.Parse(StartYearDropDown.SelectedValue),
                Int32.Parse(EndYearDropDown.SelectedValue));

            LogUsage("Charts - Country Comparison - Word download", hdnIndicator.Value, hdnCountry.Value.TrimEnd(new char[] { ',' }));

            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString() + ",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "CountryComparisonChart",
                    WordTemplatePath,
                    ChartTitle.Text);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }
        }
       
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;            
            //string chartTitle = GlobalSettings.SiteTitle;

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetCountryComparisonChartData(Int64.Parse(hdnIndicator.Value),
                hdnCountry.Value.TrimEnd(new char[] { ',' }),
                Int32.Parse(StartYearDropDown.SelectedValue),
                Int32.Parse(EndYearDropDown.SelectedValue));
            LogUsage("Charts - Country Comparison - Excel download", hdnIndicator.Value, hdnCountry.Value.TrimEnd(new char[] { ',' }));

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString()+",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';',','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            
            if (hdnImageurl.Value.Length > 0 )
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            Workbook Excel;
            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "CountryComparisonChart",
                    ExcelTemplatePath, 
                    LogoPath, 
                    ChartTitle.Text);

                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            } 

        }
       
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            //string chartTitle = GlobalSettings.SiteTitle;

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetCountryComparisonChartData(Int64.Parse(hdnIndicator.Value),
                hdnCountry.Value.TrimEnd(new char[] { ',' }),
                Int32.Parse(StartYearDropDown.SelectedValue),
                Int32.Parse(EndYearDropDown.SelectedValue));
            LogUsage("Charts - Country Comparison - PPT download", hdnIndicator.Value, hdnCountry.Value.TrimEnd(new char[] { ',' }));

            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString() + ",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "CountryComparisonChart",  
                    PowerpointTempaltePath, 
                    ChartTitle.Text);

                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }
    }
}
