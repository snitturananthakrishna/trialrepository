using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Datamonitor.PremiumTools.Generic.Library;
using System.IO;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using System.Drawing;
using System.Drawing.Imaging;
using Aspose.Slides;
using Aspose.Cells;
using Aspose.Words;
using Aspose.Words.Reporting;
using Aspose.Words.Viewer;
using System.Net;


namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class FusionMapsSave : AuthenticationBasePage //System.Web.UI.Page
    {
        #region FusionMap variables
        //Decoded data from charts.
        String data;
        //Rows of color values.
        String[] rows;
        //Width and height of chart.
        int mapWidth;
        int mapHeight;
        //Default background color of the chart
        String bgcolor;
        Color bgColor;
        //Bitmap to store the chart.
        Bitmap chartBitmap;
        //Reference to graphics object
        Graphics gr;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["userId"]))
            {
                try
                {
                    //Get the width and height from form
                    mapWidth = Int32.Parse(Request["width"]);
                    mapHeight = Int32.Parse(Request["height"]);                    
                }
                catch (Exception ex)
                {
                    //If the width and height has not been given, we cannot create the image.
                    ////Response.Write("Image width and height not provided.");
                    ////Response.End();
                }
                //Background color, request and set default
                bgcolor = Request["bgcolor"];
                if (bgcolor == "" || bgcolor == null)
                {
                    bgcolor = "#FFFFFF";
                }
                //Conver the bg color in ASP.NET format
                bgColor = ColorTranslator.FromHtml("#" + bgcolor);

                //Decompress the data
                //data = decompress(Request["data"]);		
                data = (Request["data"]);

                //Parse data 
                rows = new String[mapHeight + 1];
                rows = data.Split(';');

                //FusionChartsSave fcs = new FusionChartsSave();
                //chart = fcs.buildBitmap(chart, width, height, rows, bgColor);
                buildBitmap();
                //Now, we need to encode the image using an encoder.
                //Find the encoder.
                ImageCodecInfo[] encoders;
                ImageCodecInfo img_encoder = null;
                encoders = ImageCodecInfo.GetImageEncoders();
                foreach (ImageCodecInfo codec in encoders)
                {
                    if (codec.MimeType == Response.ContentType)
                    {
                        img_encoder = codec;
                        break;
                    }
                }
                //Set the quality as 100
                EncoderParameter jpeg_quality = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                EncoderParameters enc_params = new EncoderParameters(1);
                //Convey the parameter
                enc_params.Param[0] = jpeg_quality;

                string[] extractType = Request.QueryString["userId"].ToString().Split('|');
                string chartTitle = "Heat Map";
                DataTable dt = ((DataTable)Session["HeatMapData"]).Copy();
                if (extractType[1] == "powerpoint")
                {
                    Presentation presentation;
                    string strExtractName = "OvumExtract";
                    LogUsage("Charts - Heat Map - PPT download","heatmap","");
                    presentation = ExtractionsHelper.ExportToPowerpoint(dt,
                                    chartBitmap, 
                                    "HeatMap",
                                    PowerpointTempaltePath, 
                                    chartTitle);

                    Response.ContentType = "application/vnd.ms-powerpoint";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                    presentation.Write(Response.OutputStream);
                    Response.Flush();
                }
                else if (extractType[1] == "excel")
                {                    
                    Workbook excel = new Workbook();
                    LogUsage("Charts - Heat Map - Excel download", "heatmap", "");
                    excel = ExtractionsHelper.ExportToExcelHeatmap(dt, 
                                   chartBitmap,                                   
                                   "HeatMap",
                                   ExcelTemplatePath, 
                                   LogoPath,
                                   chartTitle); //
                    excel.Save("OvumExtract.xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
                }
                else
                {
                    LogUsage("Charts - Heat Map - Word download", "heatmap", "");
                    Document doc = ExtractionsHelper.ExportToWord(dt,
                                chartBitmap,
                                null,
                                "HeatMap",
                                WordTemplatePath, 
                                chartTitle);
                    doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
                }
            }
        }

        /// <summary>
        /// To Convert Fusion Map to buildmap.
        /// </summary>
        /// <returns></returns>
        public Bitmap buildBitmap() 
        {
            chartBitmap = new Bitmap(mapWidth, mapHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            gr = Graphics.FromImage(chartBitmap);
            gr.Clear(bgColor);
            String c;
            int r;
            int ri = 0;
            for (int i = 0; i < rows.Length; i++)
            {
                //Split individual pixels.			
                String[] pixels = rows[i].Split(',');
                //Set horizontal row index to 0
                ri = 0;
                for (int j = 0; j < pixels.Length; j++)
                {
                    //Now, if it's not empty, we process it				
                    //Split the color and repeat factor
                    String[] clrs = pixels[j].Split('_');
                    //Reference to color
                    c = clrs[0];
                    r = Int32.Parse(clrs[1]);
                    //If color is not empty (i.e. not background pixel)
                    if (c != "")
                    {
                        if (c.Length < 6)
                        {
                            //If the hexadecimal code is less than 6 characters, pad with 0
                            c = c.PadLeft(6, '0');
                        }
                        for (int k = 1; k <= r; k++)
                        {
                            chartBitmap.SetPixel(ri, i, ColorTranslator.FromHtml("#" + c));
                            //Increment horizontal row count
                            ri++;
                        }
                    }
                    else
                    {
                        //Just increment horizontal index
                        ri = ri + r;
                    }
                }
            }
            return chartBitmap;
        }

        ////METHODS (For Usage Logging)
        //protected void UserLogging(string extractType, string chartType)
        //{
        //    string additionalDetails = extractType + "-" + chartType;
        //    LogUsage("Logistics - Analysis - Download", "Logistics", additionalDetails);
        //}       
    }    
}
