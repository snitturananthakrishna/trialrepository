<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="XYChart.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.XYChart"        
        Theme="NormalTree"
        EnableViewState="false"%>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" TagName="DynamicChartfilters" TagPrefix="GMPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">

<style type="text/css">
.rightdiv
{
    float:right;
    margin-bottom:4px;
}
.leftdiv
{
    float:left;
    width:60px;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}


</style>
<script type="text/javascript">
function ShowHideFilterOptions()
{     
  var XAxis= "#<%= hdnXAxis.ClientID%>";    
  var Series = "#<%= hdnSeries.ClientID%>";
  var selectedXAxis = $(XAxis).val();
  var selectedSeries = $(Series).val();
  
  $("#filterContainer > div").show();  
  //$("#filterContainer  div  div").show();  
  
  $("div[id*='SingleFilterDiv']").show(); 
  $("div[id*='MultipleFilterDiv']").hide(); 
   
  $("div[id$='SingleFilterDiv"+selectedXAxis+"']").hide(); 
  $("div[id$='MultipleFilterDiv"+selectedXAxis+"']").show();   
  $("div[id$='SingleFilterDiv"+selectedSeries+"']").hide(); 
  $("div[id$='MultipleFilterDiv"+selectedSeries+"']").show();   
}
$(document).ready(function() {
    setTimeout("ShowHideFilterOptions()",500);
});

function ChartTypes_onNodeSelect(sender, eventArgs)
{ 
    ChartTypesDropDown.set_text(eventArgs.get_node().get_text());
    ChartTypesDropDown.collapse();    
    document.getElementById('<%=hiddenChartType.ClientID %>').value = eventArgs.get_node().get_value();   
    $("#PlotChartMsg").show();
}
function XAxis_onChange()
{
    $("#PlotChartMsg").show();
    //alert('x axis change');
    var XAxis= document.getElementById('<%=hdnXAxis.ClientID%>');
    var XAxisDropdown = "#<%=xAxisDropdown.ClientID %>";    
    XAxis.value = $(XAxisDropdown).val();      
    //To get AxisCriteria
    var XaxisText= document.getElementById('<%=hdnXAxisText.ClientID%>');    
    XaxisText.value = $("#<%= xAxisDropdown.ClientID%> option:selected").text(); 
    ShowHideFilterOptions();
}
function Series_onChange()
{
    $("#PlotChartMsg").show();
    var Series= document.getElementById('<%=hdnSeries.ClientID%>');
    var SeriesDropdown = "#<%=SeriesDropDown.ClientID %>";    
    Series.value = $(SeriesDropdown).val();
    //To get AxisCriteria
    var SeriesText= document.getElementById('<%=hdnSeriesText.ClientID%>');    
    SeriesText.value = $("#<%= SeriesDropDown.ClientID%> option:selected").text(); 
    ShowHideFilterOptions();
}

function YAxis_onChange(requireCallback)
{
    $("#PlotChartMsg").show();
    var YAxis= document.getElementById('<%=hdnYAxis.ClientID%>');
    var YAxisDropdown = document.getElementById('<%=YAxisDropDown.ClientID %>');
    YAxis.value = YAxisDropdown.value;  
    //To get AxisCriteria
    var YAsxisText= document.getElementById('<%=hdnYAxisText.ClientID%>');    
    YAsxisText.value = $("#<%= YAxisDropDown.ClientID%> option:selected").text(); 
       
    if(requireCallback)
    {    
        var list=$("#hdnControlsList").val();
    
        var stringSplitArray = list.split(";");
        var filterData = "";
        var filterDataText = "";
        var j = 0;
        for(j = 0; j < stringSplitArray.length-1; j++)
        {   
            var taxonomyhiddenFields = stringSplitArray[j].split("|");
            
            var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");
            
            var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");            
         
            filterData = filterData + 
                taxonomyhiddenFields[0] + 
                "|" + 
                $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                "|" + 
                $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                ";";
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";               
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }          
        }   
        ChartFilterCallback.callback(YAxisDropdown.value,
            filterData,
            filterDataText); 
    }
}

function ChartFilterCallback_onCallbackComplete(sender, eventArgs)
{
   ShowHideFilterOptions();   
}

function PlotChart()
{  
    var xAxisSelected = $("#<%= hdnXAxis.ClientID%>").val(); 
    var yAxisSelected = $("#<%= hdnYAxis.ClientID%>").val();  
    var seriesSelected = $("#<%= hdnSeries.ClientID%>").val();
    //To maintain AxisCriteria
    var xAxisSelectedText = $("#<%= hdnXAxisText.ClientID%>").val();     
    var yAxisSelectedText = $("#<%= hdnYAxisText.ClientID%>").val();   
    var seriesSelectedText= $("#<%= hdnSeriesText.ClientID%>").val();  
    
    var chartType = $("#<%= hiddenChartType.ClientID%>").val();
    var ChartSeriesType = $("#<%= hdnChartSeriesType.ClientID%>").val();
    var list=$("#hdnControlsList").val();
    
   $("#PlotChartMsg").hide();
    var stringSplitArray = list.split(";");
    var filterData = "";
    var filterDataText = "";
    var j = 0;
    for(j = 0; j < stringSplitArray.length-1; j++)
    {   
        var taxonomyhiddenFields = stringSplitArray[j].split("|");
        //alert(taxonomyhiddenFields);
        var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");
        //alert(taxonomyHiddenFieldsSingleText);
        var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");
        //alert(taxonomyHiddenFieldsMultipleText);
     
        filterData = filterData + 
            taxonomyhiddenFields[0] + 
            "|" + 
            $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
            "|" + 
            $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
            ";";
            
        if(taxonomyhiddenFields[0] == "999")
        {
            filterDataText =  filterDataText+
                    taxonomyhiddenFields[3] + 
                    "|" + 
                    $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                    "|" + 
                    $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                    ";";               
        }
        else
        {
              filterDataText = filterDataText +
                    taxonomyhiddenFields[3] + 
                    "|" + 
                    $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                    "|" + 
                    $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                    ";";
        }          
    }   
//    alert(filterData);
//    alert(filterDataText);
    ChartsCallback.callback(xAxisSelected,
            yAxisSelected,
            seriesSelected,
            filterData,
            chartType,
            xAxisSelectedText,
            yAxisSelectedText,
            seriesSelectedText,
            filterDataText,
            ChartSeriesType);
    //alert("callback complete");
    return false; 
}
</script>

<div ><div id="BackToChartWizardDiv" runat="server" style="float:right;margin-top:-13px"><a href="chartwizard.aspx">Back to Chart Wizard</a></div>
<h2><div id="PlotChartMsg" class="PlotChartMessage">*Please click on PLOT CHART to update</div>
    <asp:Label ID="ChartTitle" runat="server" Text="XY Chart"></asp:Label></h2>
</div>
<div class="analyzeresults_col1">
    <div style="padding-bottom:20px;"> 
        <div id="xaxisDropdownDiv" runat="server" style="float:left;">        
        X Axis <asp:DropDownList ID="xAxisDropdown" runat="server" 
        Width="110px" Font-Size="11px"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div id="yaxisDropdownDiv" runat="server" style="float:left;">
            Y Axis <asp:DropDownList ID="YAxisDropDown" runat="server"                     
                    Width="110px" 
                    Font-Size="11px"></asp:DropDownList>            
             &nbsp;&nbsp;&nbsp;&nbsp;
        </div> 
        <div id="seriesDropdownDiv" runat="server" style="float:left;">
            Series type <asp:DropDownList ID="SeriesDropDown" runat="server" Width="110px" Font-Size="11px"></asp:DropDownList> 
        </div>            
        
    </div>
    
  <%--  <div class="iReport_figure">   
       <ul class="iReport_extract">                
            <li class="iReport_extract_nolink">Extract to...</li>
            <li><asp:LinkButton ID="lnkWord1" Text="Word" runat="server" OnClick="lnkWord_Click" ><img width="12" height="10" border="0" src="../assets/images/tool_word.gif" />Word</asp:LinkButton ></li>
            <li><asp:LinkButton ID="lnkExcel1" Text="Excel" runat="server" OnClick="lnkExcel_Click"  ><img width="12" height="10" border="0" src="../assets/images/tool_excel.gif" />Excel</asp:LinkButton></li>
            <li><asp:LinkButton ID="lnkPPT1" Text="PowerPoint" runat="server" OnClick="lnkPPT_Click" ><img width="12" height="10" border="0" src="../assets/images/tool_powerpoint.gif">PowerPoint</asp:LinkButton></li>                                      
        </ul> 
    </div> --%>
    
    <div>
         <dotnetCHARTING:Chart ID="TwoDChart" runat="server" 
                Visible="false">                                           
            </dotnetCHARTING:Chart>
               
            <ComponentArt:CallBack ID="ChartsCallback" runat="server"
                PostState="true"
                OnCallback="Chart_Callback">
                <Content>
                <asp:Label runat="server" ID="ErrorMessagelabel" ForeColor="red"></asp:Label>
                   <ComponentArt:Chart id="ThreeDChart" runat="server" 
                        BackColor="White" 
                        RenderingPrecision="0.1"            
                        SaveImageOnDisk="true" 
                        Visible="false">                                                              
                    </ComponentArt:Chart>             
                    <asp:Image ID="ChartImg" runat="server" Visible="true" />
                    <asp:Label id="ImageMapLabel" runat="server"/>
                    <br />
                <div id="divChart" runat="server" 
                     style="overflow:auto; width:520px; height:150px; scrollbar-face-color: #BFC4D1;
                    scrollbar-shadow-color: #FFFFFF;
                    scrollbar-highlight-color: #FFFFFF;
                    scrollbar-3dlight-color: #FFFFFF;
                    scrollbar-darkshadow-color: #FFFFFF;
                    scrollbar-track-color: #FFFFFF;
                    scrollbar-arrow-color: #FFFFFF;">                    
                    <asp:Image ID="LegendImg" runat="server" Visible="true" />            
                    </div>
                    <asp:HiddenField ID="hdnImageurl" runat="server" Value=""/>
                    <asp:HiddenField ID="hdnImageType" runat="server" Value=""/>
                    <asp:HiddenField ID="hdnChartID" runat="server" />   
                    <asp:HiddenField ID="hdnLegendUrl" runat="server" Value="" />      
            </Content>
              <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:300" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        </ComponentArt:CallBack>
    </div>
    
</div>
<div class="analyzeresults_col2" id="analyzeresults_filter">
    <div id="filterContainer"> 
 
    <h1>FILTER OPTIONS</h1>
      <div style="padding-bottom:15px;">        
        <div style="float:right;margin-bottom:4px;">  
             <ComponentArt:ComboBox ID="ChartTypesDropDown" runat="server"
              KeyboardEnabled="false"
              AutoFilter="false"
              AutoHighlight="false"
              AutoComplete="false"
              CssClass="comboBox"                                            
              TextBoxCssClass="comboTextBox"
              DropDownCssClass="comboDropDown"
              ItemCssClass="comboItem"                      
              SelectedItemCssClass="comboItemHover"
              DropHoverImageUrl="../assets/images/drop_hover.gif"
              DropImageUrl="../assets/images/drop.gif"
              Width="123"
              DropDownHeight="120"
              DropDownWidth="120"
              Visible="true">
            <DropDownContent>
                 <ComponentArt:TreeView id="ChartTypesTreeView" Height="115" Width="120"
                    DragAndDropEnabled="false"
                    NodeEditingEnabled="false"
                    KeyboardEnabled="true"
                    CssClass="TreeView"
                    NodeCssClass="TreeNode"                                                        
                    NodeEditCssClass="NodeEdit"
                    SelectedNodeCssClass="NodeSelected"
                    LineImageWidth="19"
                    LineImageHeight="16"
                    DefaultImageWidth="16"
                    DefaultImageHeight="16"
                    ItemSpacing="0"
                    NodeLabelPadding="3"
                    ImagesBaseUrl="../assets/tvlines/"
                    LineImagesFolderUrl="../assets/images/tvlines/"
                    ShowLines="true"
                    EnableViewState="false"
                    SiteMapXmlFile="~/Config/ChartType.xml"
                    runat="server" >
                  <ClientEvents>
                    <NodeSelect EventHandler="ChartTypes_onNodeSelect" />
                  </ClientEvents>
                  </ComponentArt:TreeView>
            </DropDownContent>
        </ComponentArt:ComboBox>      
        </div>                         
        <div style="float:left;"><asp:label ID="ChartTypeLabel" runat="server" Text="Chart Type"></asp:label></div>
    </div> <br />
    <ComponentArt:CallBack ID="ChartFilterCallback" runat="server"
        OnCallback="UpdateChartfilters_Callback"> 
        <Content>
            <GMPT:DynamicChartfilters ID="ChartFilters1" runat="server" />
           </Content>
        <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:150" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="ChartFilterCallback_onCallbackComplete" />
        </ClientEvents>        
    </ComponentArt:CallBack>  
    </div>
         
   
     
        <BR />
    <div class="button_right_chart" style="width:120px">
        <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClientClick="javascript:PlotChart(); return false;" ToolTip="Update Chart">
        </asp:LinkButton>
    </div>
    
    <br /><br />     
       <h1>MY TOOLS</h1>
       <h2 class="tool_excel"><asp:LinkButton ID="lnkExcel" Text="Extract to Excel" runat="server" OnClick="lnkExcel_Click" ToolTip="extract chart data to excel"/></h2>
       <h2 class="tool_word"><asp:LinkButton ID="LinkButton1" Text="Extract to Word" runat="server" OnClick="lnkWord_Click" ToolTip="extract chart data to word"/></h2>
       <h2 class="tool_powerpoint"><asp:LinkButton ID="lnkPPT" Text="Extract to PowerPoint" runat="server" OnClick="lnkPPT_Click" ToolTip="extract chart data to power point"/></h2>               
<br /><br /><br /><br />

</div>   
  
<asp:HiddenField ID="hiddenChartType" runat="server"  />
<asp:HiddenField ID="hdnChartSeriesType" runat="server"  />
<asp:HiddenField ID="hdnXAxis" runat="server" />
<asp:HiddenField ID="hdnYAxis" runat="server" />
<asp:HiddenField ID="hdnSeries" runat="server" />

<asp:HiddenField ID="hdnXAxisText" runat="server" />
<asp:HiddenField ID="hdnYAxisText" runat="server" />
<asp:HiddenField ID="hdnSeriesText" runat="server" />

<asp:HiddenField ID="hdnChartTitle" runat="server" />

</asp:Content>
