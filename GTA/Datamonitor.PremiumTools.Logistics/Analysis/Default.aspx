<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="Default.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Logistics.Analysis.Default" 
        Title="Datamonitor PremiumTools - Logistics, Analyze results" %>
<%@ Register Assembly="ComponentArt.Web.UI" 
             Namespace="ComponentArt.Web.UI" 
             TagPrefix="ComponentArt" %>
<%@ Register TagPrefix="CArtChart" 
             Namespace="ComponentArt.Web.Visualization.Charting"
             Assembly="ComponentArt.Web.Visualization.Charting" %>
        
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<script type="text/javascript" language="javascript">
function loadDrps(drpRows1,drpSeries1)
{     
   var drpRows = document.getElementById(drpRows1);       
   var drpSeries = document.getElementById(drpSeries1);  
   
   if(drpSeries != null)
      {
        if(drpRows.value == drpSeries.value)
        {
           alert('Series 1 and Series 2 cannot be identical.');
           drpSeries.selectedIndex = 1;
         }
     }  
}
</script>
 <ul id="productscan_tabs">
        <li><asp:HyperLink ID="SearchLink" runat="server" NavigateUrl="~/Search.aspx" Text="Search"></asp:HyperLink></li>
        <li><asp:HyperLink ID="ViewResultsLink" runat="server" NavigateUrl="~/Results/Default.aspx" Text="View Results"></asp:HyperLink></li>
        <li><a class="selected">Analyze Results</a></li>
    </ul>   
 <div id="maincontent" class="other_styles">
        <h2>
            Build Your Own Chart</h2>
        <div class="refinechart">
            <div class="refinechart_col">
                <p>
                    <b>Select X-Axis</b></p>
                <p>
                    <asp:DropDownList ID="drpRows" runat="server" CssClass="formstyle3">
                    </asp:DropDownList>
                </p>                                
            </div>
            <div class="refinechart_col">
                <div id="tblSeries2" runat="server">
                    <p>
                        <b>Select Y-Axis</b></p>
                    <p>
                        <asp:DropDownList ID="drpColumns" runat="server" CssClass="formstyle3">
                        </asp:DropDownList></p>                                        
                </div>
            </div> 
            <div class="refinechart_col2">   
                      
                    <p>
                        <b>Select Series</b></p>
                    <p>
                        <asp:DropDownList ID="drpSeries" runat="server" CssClass="formstyle3">
                        </asp:DropDownList></p>                                                        
            </div>    
    </div> 
       
        <div class="refinechart">
            <div class="refinechart_col">
                <p>
                    <b>Country</b></p>
                <p>
                    <asp:DropDownList ID="drpCtry" runat="server" CssClass="formstyle3">
                    <asp:ListItem Text="asia" Value="asia" />
                    </asp:DropDownList>
                </p>                                
            </div>
            <div class="refinechart_col">
               <p>
                    <b>Mode</b></p>
                    <p>
                        <asp:DropDownList ID="drpMode" runat="server" CssClass="formstyle3">
                        <asp:ListItem Text="Rail" Value="Rail" />
                        </asp:DropDownList></p>                                       
                
            </div> 
            <div class="refinechart_col2">   
                      
                    <p>
                        <b>Market</b></p>
                    <p>
                        <asp:DropDownList ID="drpMarket" runat="server" CssClass="formstyle3">
                        <asp:ListItem Text="Express(Narrow) > Total" Value="Express(Narrow) > Total" />                        
                        </asp:DropDownList></p>                                                        
            </div>    
    </div>
     
        <div class="refinechart">
            <div class="refinechart_col">
                <p>
                    <b>Destination</b></p>
                <p>
                    <asp:DropDownList ID="drpDestn" runat="server" CssClass="formstyle3">
                    </asp:DropDownList>
                </p>                                
            </div>
            <div class="refinechart_col">
                <div id="Div2" runat="server">
                    <p>
                        <b>Provider</b></p>
                    <p>
                        <asp:DropDownList ID="drpProvider" runat="server" CssClass="formstyle3">
                        </asp:DropDownList></p>                                        
                </div>
            </div> 
            <div class="refinechart_col2">   
                      
                    <p>
                        <b>Units</b></p>
                    <p>
                        <asp:DropDownList ID="drpUnits" runat="server" CssClass="formstyle3">
                        </asp:DropDownList></p>                                                        
            </div>    
    </div>
    
        <div class="refinechart">
            <div class="refinechart_col">
                <p>
                    <b>Years</b></p>
                <p>
                    <asp:DropDownList ID="drpYears" runat="server" CssClass="formstyle3">
                    <asp:ListItem Text="2009" Value="2009" />
                    <asp:ListItem Text="2008" Value="2008" />                   
                    </asp:DropDownList>
                </p>                                
            </div>
            <div class="refinechart_col">
                <div id="Div1" runat="server">
                    <p>
                        <b>Currency</b></p>
                    <p>
                        <asp:DropDownList ID="drpCurrency" runat="server" CssClass="formstyle3">
                        </asp:DropDownList></p>                                        
                </div>
            </div>                      
    </div>       
    <div>
    <asp:LinkButton ID="lnkBuildChart" runat="Server" Text="Build Chart" OnClick="lnkBuildChart_Click" />
    </div>   
    <div>
    <CArtChart:Chart ID="Chart1" runat="server" RenderingPrecision="0.1" SaveImageOnDisk="true" >
        <Clientside AutoRenderOnChange="False" ClientsideApiEnabled="False" ClientsideCustomizedImageCachingEnabled="False"
            ControlResizeButtonWidthPx="9" IsScrollControll="False" RangeClientTemplateDateFormat="MMMM dd, yyyy"
            RangeClientTemplateXoffset="0" RangeClientTemplateYoffset="0" RefreshMethod="Callback"
            ScrollControllHeight="21px" ScrollImagesDirectoryPath="scroll_images/" ScrollShadowColor="white"
            ScrollShadowOpacity="0.7" ScrollStepPercentage="0.5" SerializeDatapoints="False"
            ViewAngleChooserEnabled="False" />
        <Series>
            <CArtChart:Series HasIndependentYAxis="False" MarkerSizePts="0" Name="S0">
            </CArtChart:Series>
            <CArtChart:Series HasIndependentYAxis="False" MarkerSizePts="0" Name="S1">
            </CArtChart:Series>
        </Series>
        <View Perspective="57" ViewDirection="(10,7,20)">
        </View>
        <Legend BackColor="Transparent" BorderColor="Transparent" Font="Arial, 10pt" FontColor="Transparent"
            Name="&lt;Default&gt;" NumberOfItems="2147483647" Ordered="False" Visible="False">
        </Legend>
        <CoordinateSystem>
            <XAxis DefaultCoordinateSetComputation="ByNumberOfPoints,50,1,Day,True,True">
            </XAxis>
            <YAxis DefaultCoordinateSetComputation="ByNumberOfPoints,5,20,Day,True,True">
            </YAxis>
            <ZAxis DefaultCoordinateSetComputation="ByNumberOfPoints,50,1,Day,True,True">
            </ZAxis>
        </CoordinateSystem>
        <LabelStyles>
            <CArtChart:LabelStyle LiftZ="0.5" Name="Default" />
        </LabelStyles>
    </CArtChart:Chart>    
    
    </div> 
 </div>
</asp:Content>
