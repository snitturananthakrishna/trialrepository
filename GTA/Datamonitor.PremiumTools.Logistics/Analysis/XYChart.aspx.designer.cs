//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3615
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datamonitor.PremiumTools.Generic.Analysis {
    
    public partial class XYChart {
        protected System.Web.UI.HtmlControls.HtmlGenericControl BackToChartWizardDiv;
        protected System.Web.UI.WebControls.Label ChartTitle;
        protected System.Web.UI.HtmlControls.HtmlGenericControl xaxisDropdownDiv;
        protected System.Web.UI.WebControls.DropDownList xAxisDropdown;
        protected System.Web.UI.HtmlControls.HtmlGenericControl yaxisDropdownDiv;
        protected System.Web.UI.WebControls.DropDownList YAxisDropDown;
        protected System.Web.UI.HtmlControls.HtmlGenericControl seriesDropdownDiv;
        protected System.Web.UI.WebControls.DropDownList SeriesDropDown;
        protected dotnetCHARTING.Chart TwoDChart;
        protected ComponentArt.Web.UI.CallBack ChartsCallback;
        protected System.Web.UI.WebControls.Label ErrorMessagelabel;
        protected ComponentArt.Web.Visualization.Charting.Chart ThreeDChart;
        protected System.Web.UI.WebControls.Image ChartImg;
        protected System.Web.UI.WebControls.Label ImageMapLabel;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divChart;
        protected System.Web.UI.WebControls.Image LegendImg;
        protected System.Web.UI.WebControls.HiddenField hdnImageurl;
        protected System.Web.UI.WebControls.HiddenField hdnImageType;
        protected System.Web.UI.WebControls.HiddenField hdnChartID;
        protected System.Web.UI.WebControls.HiddenField hdnLegendUrl;
        protected ComponentArt.Web.UI.ComboBox ChartTypesDropDown;
        protected ComponentArt.Web.UI.TreeView ChartTypesTreeView;
        protected System.Web.UI.WebControls.Label ChartTypeLabel;
        protected ComponentArt.Web.UI.CallBack ChartFilterCallback;
        protected Datamonitor.PremiumTools.Generic.Controls.DynamicChartfilters ChartFilters1;
        protected System.Web.UI.WebControls.LinkButton PlotChart;
        protected System.Web.UI.WebControls.LinkButton lnkExcel;
        protected System.Web.UI.WebControls.LinkButton LinkButton1;
        protected System.Web.UI.WebControls.LinkButton lnkPPT;
        protected System.Web.UI.WebControls.HiddenField hiddenChartType;
        protected System.Web.UI.WebControls.HiddenField hdnChartSeriesType;
        protected System.Web.UI.WebControls.HiddenField hdnXAxis;
        protected System.Web.UI.WebControls.HiddenField hdnYAxis;
        protected System.Web.UI.WebControls.HiddenField hdnSeries;
        protected System.Web.UI.WebControls.HiddenField hdnXAxisText;
        protected System.Web.UI.WebControls.HiddenField hdnYAxisText;
        protected System.Web.UI.WebControls.HiddenField hdnSeriesText;
        protected System.Web.UI.WebControls.HiddenField hdnChartTitle;
    }
}
