<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/AnalysisMaster.Master"  
        AutoEventWireup="true" 
        CodeBehind="BubbleChart.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.BubbleChart" 
        Theme="NormalTree"%>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" TagName="DynamicChartfilters" TagPrefix="GPMT" %>        
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">

<style type="text/css">
.rightdiv
{
    float:right;
    margin-bottom:4px;
}
.leftdiv
{
    float:left;
    width:60px;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}
</style>
<script type="text/javascript">
function ShowHideFilterOptions()
{  
  var Series= "#<%= seriesDropdown.ClientID%>";    
  var selectedSeries = $(Series).val();  
  
  $("div[id*='SingleFilterDiv']").show(); 
  $("div[id*='MultipleFilterDiv']").hide(); 
   
  $("div[id$='SingleFilterDiv"+selectedSeries+"']").hide(); 
  $("div[id$='MultipleFilterDiv"+selectedSeries+"']").show(); 
  

}
 $(document).ready(function() {
   setTimeout("ShowHideFilterOptions()",500);
});

function Series_onChange()
{
    ShowHideFilterOptions();
    $("#PlotChartMsg").show();
    var Series= document.getElementById('<%=hdnSeries.ClientID%>');
    Series.value = $("#<%= seriesDropdown.ClientID%>").val();
    
    var SeriesText= document.getElementById('<%=hdnSeriesText.ClientID%>');
    SeriesText.value =$("#<%= seriesDropdown.ClientID%> option:selected").text(); 
}

function ChartFilterCallback_onCallbackComplete(sender, eventArgs)
{
   ShowHideFilterOptions();   
}

function XAxis_onChange(requireCallback)
{  $("#PlotChartMsg").show();
    var XAxis= document.getElementById('<%=hdnXAxis.ClientID%>');
    XAxis.value = $("#<%= xAxisDropdown.ClientID%>").val();
    
    var XAxisText= document.getElementById('<%=hdnXAxisText.ClientID%>');
    XAxisText.value =$("#<%= xAxisDropdown.ClientID%> option:selected").text();
    
    ChartFilter_Callback(requireCallback);
}

function YAxis_onChange(requireCallback)
{  $("#PlotChartMsg").show();
    var YAxis= document.getElementById('<%=hdnYAxis.ClientID%>');
    YAxis.value = $("#<%= YAxisDropDown.ClientID%>").val();
    
    var YAxisText= document.getElementById('<%=hdnYAxisText.ClientID%>');
    YAxisText.value =$("#<%= YAxisDropDown.ClientID%> option:selected").text();
    
    ChartFilter_Callback(requireCallback);
}

function Bubble_onChange(requireCallback)
{  
    var Bubble= document.getElementById('<%=hdnBubble.ClientID%>');
    Bubble.value = $("#<%= BubbleDropDown.ClientID%>").val();
    $("#PlotChartMsg").show();
    var BubbleText = document.getElementById('<%=hdnBubbleText.ClientID%>');
    BubbleText.value =$("#<%= BubbleDropDown.ClientID%> option:selected").text();
    
    ChartFilter_Callback(requireCallback);
}

function ChartFilter_Callback(requireCallback)
{
    if(requireCallback)
    {     
        var list=$("#hdnControlsList").val();
    
        var stringSplitArray = list.split(";");
        var filterData = "";
        var filterDataText = "";
        var j = 0;
        for(j = 0; j < stringSplitArray.length-1; j++)
        {   
            var taxonomyhiddenFields = stringSplitArray[j].split("|");
            
            var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");
            
            var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");            
         
            filterData = filterData + 
                taxonomyhiddenFields[0] + 
                "|" + 
                $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                "|" + 
                $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                ";";
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";               
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }          
        }   
        
        ChartFilterCallback.callback($("#<%= hdnXAxis.ClientID%>").val(), 
                                       $("#<%= hdnYAxis.ClientID%>").val(), 
                                       $("#<%= hdnBubble.ClientID%>").val(),
                                        filterData,
                                        filterDataText); 
    }
}

function ChartTypes_onNodeSelect(sender, eventArgs)
{ 
    ChartTypesDropDown.set_text(eventArgs.get_node().get_text());
    ChartTypesDropDown.collapse();    
    document.getElementById('<%=hiddenChartType.ClientID %>').value = eventArgs.get_node().get_value();   
$("#PlotChartMsg").show();
}

function PlotChart()
{  
    $("#PlotChartMsg").hide();
    var seriesSelected = $("#<%= hdnSeries.ClientID%>").val();
    var xAxisSelected = $("#<%= hdnXAxis.ClientID%>").val();    
    var yAxisSelected = $("#<%= hdnYAxis.ClientID%>").val();
    var bubbleSelected = $("#<%= hdnBubble.ClientID%>").val();  
    
    var seriesSelectedText = $("#<%= hdnSeriesText.ClientID%>").val();
    var xAxisSelectedText = $("#<%= hdnXAxisText.ClientID%>").val();    
    var yAxisSelectedText = $("#<%= hdnYAxisText.ClientID%>").val();
    var bubbleSelectedText = $("#<%= hdnBubbleText.ClientID%>").val();     
      
    var chartType = $("#<%= hiddenChartType.ClientID%>").val();  
//    var list=$("#hdnControlsList").val();
//    
//    var stringSplitArray = list.split(";");    
//    var filterData = "";
//    var j = 0;
//    
//    for(j = 0; j < stringSplitArray.length-1; j++)
//    {
//        var taxonomyhiddenFields = stringSplitArray[j].split("|");
//            
//        filterData = filterData + 
//            taxonomyhiddenFields[0] + 
//            "|" + 
//            $("#"+taxonomyhiddenFields[1]).val()+ 
//            "|" + 
//            $("#"+taxonomyhiddenFields[2]).val()+
//            ";";
//    }
 var list=$("#hdnControlsList").val();
   
    var stringSplitArray = list.split(";");

    var filterData = "";
    var filterDataText = "";
    var j = 0;
    for(j = 0; j < stringSplitArray.length-1; j++)
    {  
        var taxonomyhiddenFields = stringSplitArray[j].split("|"); 
        var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");  
         var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");
//        filterData = filterData + 
//            taxonomyhiddenFields[0] + 
//            "|" + 
//            $("#"+taxonomyhiddenFields[1]).val()+ 
//            "|" + 
//            $("#"+taxonomyhiddenFields[2]).val()+
//            ";";
     
         filterData = filterData + 
            taxonomyhiddenFields[0] + 
            "|" + 
            $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
            "|" + 
            $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
            ";";
            
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";                
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }
              
    }
//    alert(filterData);
//    alert(filterDataText);    
    ChartsCallback.callback(seriesSelected,
            xAxisSelected,
            yAxisSelected,
            bubbleSelected,
            filterData,
            chartType,
            seriesSelectedText,
            xAxisSelectedText,
            yAxisSelectedText,
            bubbleSelectedText,
            filterDataText);
    
    return false; 
}
</script>
<div >
<h2><div id="PlotChartMsg" class="PlotChartMessage">*Please click on PLOT CHART to update</div><asp:Label ID="ChartTitle" runat="server" Text="Bubble Chart"></asp:Label></h2>
</div>
<div class="analyzeresults_col1">
           <div style="padding-bottom:20px;"> 
           <div id="seriesDropdownDiv" runat="server" style="float:left;">
                Series <asp:DropDownList ID="seriesDropdown" runat="server" 
                Width="85px" Font-Size="11px"></asp:DropDownList> </div>
                 <div id="xaxisDropdownDiv" runat="server" style="float:left;">  
                XAxis <asp:DropDownList ID="xAxisDropdown" runat="server" 
                Width="95px" Font-Size="11px"></asp:DropDownList></div>
                <div id="yaxisDropdownDiv" runat="server" style="float:left;">
                YAxis <asp:DropDownList ID="YAxisDropDown" runat="server" 
                Width="95px" Font-Size="11px"></asp:DropDownList>   </div>
                <div id="YAxis2DropDown" runat="server" style="float:left;">                         
                Bubble <asp:DropDownList ID="BubbleDropDown" runat="server" Width="95px" Font-Size="11px">
                </asp:DropDownList>  </div>               
                </div>                           
          
           <div>
           
           <dotnetCHARTING:Chart ID="TwoDChart" runat="server" 
            Visible="false">              
        </dotnetCHARTING:Chart>    
        
        <ComponentArt:CallBack ID="ChartsCallback" runat="server"
                PostState="true"
                OnCallback="Chart_Callback">
           <Content> 
           <asp:Label runat="server" ID="ErrorMessagelabel" ForeColor="red"></asp:Label>               
            <ComponentArt:Chart id="ThreeDChart" runat="server" 
                BackColor="White" 
                RenderingPrecision="0.1"            
                SaveImageOnDisk="true" 
                Visible="false">
            </ComponentArt:Chart>
            <asp:Image ID="ChartImg" runat="server" Visible="true" />
            <asp:Label id="ImageMapLabel" runat="server"/>
             <br />
            <div id="divChart" runat="server" 
                 style="overflow:auto; width:520px; height:150px; scrollbar-face-color: #BFC4D1;
                scrollbar-shadow-color: #FFFFFF;
                scrollbar-highlight-color: #FFFFFF;
                scrollbar-3dlight-color: #FFFFFF;
                scrollbar-darkshadow-color: #FFFFFF;
                scrollbar-track-color: #FFFFFF;
                scrollbar-arrow-color: #FFFFFF;">                    
                <asp:Image ID="LegendImg" runat="server" Visible="true" />            
                </div>
            
            <asp:HiddenField ID="hdnImageurl" runat="server" Value="" />
            <asp:HiddenField ID="hdnImageType" runat="server" Value="" />
            <asp:HiddenField ID="hdnChartID" runat="server" Value=""/>
            <asp:HiddenField ID="hdnLegendUrl" runat="server" Value="" />   
          </Content>
           <LoadingPanelClientTemplate>
                <table class="loadingpanel" width="100%" style="height:300" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td colspan="2">
                      <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                      </td>
                    </tr>
                    </table>
                    </td>
                  </tr>
                  </table>
            </LoadingPanelClientTemplate>
        </ComponentArt:CallBack> 
            </div>
        </div>
    <div class="analyzeresults_col2" id="analyzeresults_filter">
    <h1>FILTER OPTIONS</h1>
    
     <div style="padding-bottom:15px">
         <div style="float:right;margin-bottom:4px;">        
                <ComponentArt:ComboBox ID="ChartTypesDropDown" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="220"
                      DropDownWidth="140"
                      Visible="true">
                    <DropDownContent>
                         <ComponentArt:TreeView id="ChartTypesTreeView" Height="220" Width="140"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="16"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="false"   
                            runat="server" 
                            > <%--SiteMapXmlFile="~/Config/ChartType.xml" --%>
                          <ClientEvents>
                            <NodeSelect EventHandler="ChartTypes_onNodeSelect" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                    </DropDownContent>
                </ComponentArt:ComboBox>          
              </div>  
            <div style="float:left;">Chart&nbsp;Type: &nbsp;</div>                                   
      </div>  
      
    <div id="filterContainer">    
         <ComponentArt:CallBack ID="ChartFilterCallback" runat="server"
        OnCallback="UpdateChartfilters_Callback"> 
        <Content>
            <GPMT:DynamicChartFilters ID="ChartFilters1" runat="server" />
        </Content>
        <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:150" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="ChartFilterCallback_onCallbackComplete" />
        </ClientEvents>        
    </ComponentArt:CallBack> 
    </div>
    
    <div class="button_right_chart" style="width:120px">
         <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClientClick="javascript:PlotChart(); return false;">
        </asp:LinkButton>
    </div>
    
    <br /><br />
       <h1>MY TOOLS</h1>
       <h2 class="tool_excel"><asp:LinkButton ID="lnkExcel" Text="Extract to Excel" runat="server" OnClick="lnkExcel_Click" /></h2>
       <h2 class="tool_word"><asp:LinkButton ID="LinkButton1" Text="Extract to Word" runat="server" OnClick="lnkWord_Click" /></h2>
       <h2 class="tool_powerpoint"><asp:LinkButton ID="lnkPPT" Text="Extract to PowerPoint" runat="server" OnClick="lnkPPT_Click" /></h2>               
<br /><br /><br /><br /></div>
   
<asp:HiddenField ID="hiddenChartType" runat="server" />
<asp:HiddenField ID="hdnSeries" runat="server" />
<asp:HiddenField ID="hdnXAxis" runat="server" />
<asp:HiddenField ID="hdnYAxis" runat="server" />
<asp:HiddenField ID="hdnBubble" runat="server" />
<asp:HiddenField ID="hdnTaxonomy11Selection" runat="server" />
<asp:HiddenField ID="hdnSeriesText" runat="server" />
<asp:HiddenField ID="hdnXAxisText" runat="server" />
<asp:HiddenField ID="hdnYAxisText" runat="server" />
<asp:HiddenField ID="hdnBubbleText" runat="server" />
</asp:Content>
