using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class DualYAxisChart : AuthenticationBasePage 
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;  

        // EVENT OVERRIDES
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                 !ChartFilterCallback.IsCallback &&
                 !ChartsCallback.IsCallback)
            {
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();                
                BindMacoEconomicIndicatorsDropdownData();
                BindChartTypeDropdownData();
                SelectDefaultchartType();

                xAxisDropdown.Attributes.Add("onchange", "javascript:Xaxis_onChange();");
                YAxis2DropDown.Attributes.Add("onchange", "javascript:YAxis2_onChange(true);");
                YAxis1DropDown.Attributes.Add("onchange", "javascript:YAxis1_onChange(true);");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";

            if (!ChartFilterCallback.IsCallback && 
                !ChartsCallback.IsCallback && 
                !IsPostBack&&
                !string.IsNullOrEmpty(hdnYAxis1.Value)&&
                !string.IsNullOrEmpty(hdnYAxis2.Value))
            {
                ChartFilters1.YAxis = hdnYAxis2.Value != "11" ?
                       Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnYAxis1.Value))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnYAxis2.Value))) :
                       Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnYAxis1.Value)));
                
                    ChartFilters1.LoadData();
            }           
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack && !ChartsCallback.IsCallback)
            {
                PlotChart_Click(null, null);
            }
        }

        // PRIVATE METHODS (initial population) 
        private void BindAxisDropdownsData()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath(GlobalSettings.ChartFilterOptionsConfigPath));
            string xPathExpression = "//Taxonomy[@type='XAxis']";
            XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            //Get and bind data to X Axis drop down
            string taxonomtypes = xaxisItem.Attributes["taxonomtypes"].Value;
            string from = xaxisItem.Attributes["from"].Value;
            string includeYears = xaxisItem.Attributes["includeYears"].Value;
            DataSet XAxisData = new DataSet();
            if (from.Equals("taxonomtypes"))
            {
                XAxisData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
            }
            else if (from.Equals("dataset"))
            {
                XAxisData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
            }

            xAxisDropdown.DataSource = XAxisData;
            xAxisDropdown.DataTextField = XAxisData.Tables[0].Columns["Name"].ToString();
            xAxisDropdown.DataValueField = XAxisData.Tables[0].Columns["ID"].ToString();
            xAxisDropdown.DataBind();

            //Get and bind data to series drop down
            XmlElement SeriesItem = (XmlElement)doc.SelectSingleNode(xPathExpression);

            try
            {
                xAxisDropdown.SelectedValue = GlobalSettings.DefaultXAxisType;
                hdnXAxis.Value = GlobalSettings.DefaultXAxisType;
                hdnXAxisText.Value = xAxisDropdown.SelectedItem.Text;
            }
            catch (Exception ex) { }


            //Get Y-Axis1 data.
            string SelectionsY1XML = GetFilterCriteriaFromSession();

            DataSet YAxis1DataSet = SqlDataService.GetYAxisTaxonomyByCriteria(SelectionsY1XML, true);
            if (YAxis1DataSet != null && YAxis1DataSet.Tables.Count > 0)
            {
                if (GlobalSettings.ShowChartWizard &&
                    Request.QueryString["source"] != null &&
                    Request.QueryString["source"].ToString().Equals("wizard") &&
                    Session["SelectedMeasures"] != null &&
                    Session["SelectedMeasures"].ToString() != "")
                {
                    //Filter measures if chart wizard selections are found
                    DataView SelectedMeasures = YAxis1DataSet.Tables[0].DefaultView;
                    SelectedMeasures.RowFilter = "id in (" + Session["SelectedMeasures"].ToString() + ")";
                    DataTable SelectedMeasuresTable = SelectedMeasures.ToTable();

                    //Binding Y-Axis
                    YAxis1DropDown.DataSource = SelectedMeasuresTable;
                    YAxis1DropDown.DataTextField = SelectedMeasuresTable.Columns["displayName"].ToString();
                    YAxis1DropDown.DataValueField = SelectedMeasuresTable.Columns["ID"].ToString();
                    YAxis1DropDown.DataBind();

                    YAxis2DropDown.DataSource = SelectedMeasuresTable;
                    YAxis2DropDown.DataTextField = SelectedMeasuresTable.Columns["displayName"].ToString();
                    YAxis2DropDown.DataValueField = SelectedMeasuresTable.Columns["ID"].ToString();
                    YAxis2DropDown.DataBind();
                }
                else
                {
                    //Binding Y-Axis
                    YAxis1DropDown.DataSource = YAxis1DataSet;
                    YAxis1DropDown.DataTextField = YAxis1DataSet.Tables[0].Columns["displayName"].ToString();
                    YAxis1DropDown.DataValueField = YAxis1DataSet.Tables[0].Columns["ID"].ToString();
                    YAxis1DropDown.DataBind();

                    YAxis2DropDown.DataSource = YAxis1DataSet;
                    YAxis2DropDown.DataTextField = YAxis1DataSet.Tables[0].Columns["displayName"].ToString();
                    YAxis2DropDown.DataValueField = YAxis1DataSet.Tables[0].Columns["ID"].ToString();
                    YAxis2DropDown.DataBind();
                }

                try
                {
                    YAxis1DropDown.SelectedValue = GlobalSettings.DefaultYAxisType;
                    YAxis2DropDown.SelectedIndex = YAxis2DropDown.Items.Count > 1 ? 1 : 0;
                    hdnYAxis1.Value = YAxis1DropDown.SelectedValue;
                    hdnYAxis1Text.Value = YAxis1DropDown.SelectedItem.Text;
                    hdnYAxis2.Value = YAxis2DropDown.Items[YAxis2DropDown.SelectedIndex].Value;
                    hdnYAxis2Text.Value = YAxis2DropDown.Items[YAxis2DropDown.SelectedIndex].Text;

                }
                catch (Exception ex) { }
            }
        }

        private void BindChartTypeDropdownData()
        {
            CArt.TreeViewNode newNode = new CArt.TreeViewNode();

            newNode = new CArt.TreeViewNode();
            newNode.Text = "2D";
            newNode.Value = "2D";
            ChartTypesTreeView.Nodes.Add(newNode);

            //newNode = new CArt.TreeViewNode();
            //newNode.Text = "3D";
            //newNode.Value = "3D";
            //ChartTypesTreeView.Nodes.Add(newNode);
        }

        private void BindMacoEconomicIndicatorsDropdownData()
        {
            CArt.TreeViewNode newNode = new CArt.TreeViewNode();

            DataSet ds = SqlDataService.GetMacroeconomicIndicatorsList();

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    newNode = new CArt.TreeViewNode();
                    newNode.Text = row["displayName"].ToString();
                    newNode.Value = row["taxonomyId"].ToString();
                    IndicatorTree.Nodes.Add(newNode);
                }
            }

        }

        private void SelectDefaultchartType()
        {
            CArt.TreeViewNode LeafNode = null;
            foreach (CArt.TreeViewNode tnode in ChartTypesTreeView.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    LeafNode = GetFirstLeafNode(tnode);
                    break;
                }
                else
                {
                    LeafNode = tnode;
                    break;
                }
            }
            if (LeafNode != null)
            {
                ChartTypesDropDown.Text = LeafNode.Text;
                hiddenChartType.Value = LeafNode.Value;
                ChartTypesTreeView.SelectedNode = LeafNode;
            }
        }
        
        private CArt.TreeViewNode GetFirstLeafNode(CArt.TreeViewNode parentNode)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    return GetFirstLeafNode(tnode);
                }
                else
                {
                    return tnode;
                }
            }
            return parentNode;
        }

        // METHODS (Build selection criteria into XML)
        private string GetCallbackDataFilterXML(string xAxis,
            string yAxis1,
            string yAxis2,
            string filterValues,
            string indicatorsSelected,
            string filterValuesText)
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            string[] taxonomyFilters = filterValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string[] taxonomyFiltersText = filterValuesText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < taxonomyFilters.Length; i++)
            {
                string taxonomyID = taxonomyFilters[i].Split('|')[0];
                string taxonomyName = taxonomyFiltersText[i].Split('|')[0];
                string taxonomyRowID = GetColumnRowIDById(taxonomyID);

                string taxonomyValues = taxonomyID.Equals(xAxis) ?
                    taxonomyFilters[i].Split('|')[2] :
                    taxonomyFilters[i].Split('|')[1];

                string taxonomyValuesText = taxonomyID.Equals(xAxis) ?
                taxonomyFiltersText[i].Split('|')[2] :
                taxonomyFiltersText[i].Split('|')[1];

                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                    taxonomyID,
                                    taxonomyValues.TrimEnd(new char[] { ',' }),
                                    taxonomyRowID);
                if (taxonomyID == "999")
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyName,
                                    taxonomyValues.TrimEnd(new char[] { ',' }));
                }
                else
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                         taxonomyName,
                         taxonomyValuesText.TrimEnd(new char[] { ',' }));
                }
            }

            if (yAxis2.Equals("11"))
            {
                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                   yAxis2,
                                   indicatorsSelected,
                                   "None");
            }

            SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
            Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            return SelectionsXML;
        }

       private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    string TaxonomySelectedText = string.Empty;

                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                            {
                                //Get all keys (ids) from dictionary
                                List<int> keys = new List<int>(selectedIDsList.Keys);
                                //convert int array to string array
                                string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                                TaxonomySelectedValue = string.Join(",", SelectedIDs);
                            }
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                                TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                            }
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                                TaxonomySelectedText = TaxonomySelectedText.TrimEnd(new char[] { ',' });
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                            }
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                            }

                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                              taxonomyRow["ID"].ToString(),
                              TaxonomySelectedValue,
                              taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedText))
                        {
                            if (TaxonomySelectedText.Contains("'"))
                            {
                                TaxonomySelectedText = TaxonomySelectedText.Replace("'", "&apos;");
                            }
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedText);
                        }
                    }
                    else if (taxonomyRow["ID"].ToString().Equals("999"))
                    {

                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy999Selection")).Value;
                            TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                        }
                        else
                        {
                            //Get selected year value from user control's hidden field
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy999Selection")).Value;
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                             taxonomyRow["ID"].ToString(),
                             TaxonomySelectedValue,
                             taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }
                    else if(taxonomyRow["ID"].ToString().Equals("11"))
                    {
                        if (hdnYAxis2.Value.Equals("11"))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                             taxonomyRow["ID"].ToString(),
                             hdnTaxonomy11Selection.Value,
                             taxonomyRow["ColumnRowID"].ToString());
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
                Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            }

            return SelectionsXML;
        }

        // EVENT HANDLERS       
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            DataSet ChartDataset = null;
            if (xAxisDropdown.SelectedItem != null && YAxis1DropDown.SelectedItem != null && YAxis2DropDown.SelectedItem != null)
            {
                AxisCriteria = "XAxis: " + xAxisDropdown.SelectedItem.Text.ToString() +
                               "; Y Axis1: " + YAxis1DropDown.SelectedItem.Text.ToString() +
                               "; Y Axis2: " + YAxis2DropDown.SelectedItem.Text;
                //Get selection criteria   
                string SelectionsXML = GetDataFilterXML();

                //Get chart data 
                ChartDataset = SqlDataService.GetDualYAxisData(Int32.Parse(xAxisDropdown.SelectedValue),
                    Int32.Parse(YAxis1DropDown.SelectedValue),
                    Int32.Parse(hdnYAxis2.Value),
                    SelectionsXML);
                LogUsage("Chart - Dual YAxis", "Dual YAxis1", SelectionsXML);
                ViewState["ChartDataset"] = ChartDataset;
            }

            //Plot chart
            if (ChartDataset!=null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = hiddenChartType.Value;
                string ChartType = "2D";
                string ChartSeriesType = "Bar";

                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                    //ChartSeriesType = ChartOptions.Split('|')[1];
                }
                string YAxis1Label = ChartDataset.Tables[0].Rows[0]["Y1Unit"].ToString();
                string YAxis2Label = ChartDataset.Tables[0].Rows[0]["Y2Unit"].ToString();

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotDualYAxisChart(ChartDataset,
                        ThreeDChart,
                        ChartSeriesType,
                        YAxis1Label,
                        YAxis2Label);
                    TwoDChart.Visible = false;
                    targetChart.Visible = true;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["3DChartImage"] = targetChart.ImageOutputDirectory.ToString();
                    //Save the Chart GUID in viewstate, used in extractions.
                    ViewState["ChartID"] = targetChart.CustomImageFileName.ToString();
                    ViewState["ChartType"] = "3D";
                }
                else
                {
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotDualYAxisChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                       YAxis1Label,
                       YAxis2Label,
                       AxisCriteria);
                    TwoDimensionalCharts.AddCopyrightText(targetChart, Convert.ToInt32(targetChart.Width.Value) - 180);
                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;

                    if (!Directory.Exists(Server.MapPath("temp")))
                        Directory.CreateDirectory(Server.MapPath("temp"));

                    targetChart.FileManager.TempDirectory = "temp";

                    #region Add extra legend
                    //Create the new legend entry box.
                    dotnetCHARTING.LegendBox newLegend = new dotnetCHARTING.LegendBox();

                    // Set the columns for the box to use. The first 3 columns refer to legend entry properties
                    // LegendEntry.Name, LegendEntry.Marker, and LegendEntry.Value.
                    newLegend.Template = "%Icon%Name";

                    // instantiate the legend entries with a name, value, and background color (LegendEntry.Marker will use this background).
                    dotnetCHARTING.LegendEntry entry1 = new dotnetCHARTING.LegendEntry(YAxis1DropDown.SelectedItem.Text + " - " + YAxis1Label, "", new dotnetCHARTING.Background(ColorTranslator.FromHtml("#002252")));
                    dotnetCHARTING.LegendEntry entry2 = new dotnetCHARTING.LegendEntry(YAxis2DropDown.SelectedItem.Text + " - " + YAxis2Label, "", new dotnetCHARTING.Background(ColorTranslator.FromHtml("#6c80bf")));

                    // Add the entries to our custom legend.
                    newLegend.ExtraEntries.Add(entry1);
                    newLegend.ExtraEntries.Add(entry2);

                    // The the new legend to the chart.
                    targetChart.ExtraLegendBoxes.Add(newLegend);

                    #region Set styles
                    newLegend.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
                    newLegend.CornerBottomLeft = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerBottomRight = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerTopLeft = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerTopRight = dotnetCHARTING.BoxCorner.Square;
                    newLegend.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Arial", 8), Color.Black);
                    newLegend.Line = new dotnetCHARTING.Line(Color.White);                   
                    newLegend.Line.Color = Color.White;
                    //newLegend.Position = LegendBoxPosition.Top;
                    // Set an orientation for the custom legend.
                    newLegend.Orientation = dotnetCHARTING.Orientation.Bottom;
                    newLegend.Shadow.Color = Color.White;
                    newLegend.InteriorLine.Color = Color.White;
                    #endregion

                    #endregion

                    targetChart.LegendBox.Visible = false;
                    if (targetChart.SeriesCollection.Count > 3)
                    {
                        targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                    }
                    //LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                    LegendImg.Visible = false;
                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    //targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["2DChartImage"] = ChartImg.ImageUrl;
                    ViewState["ChartType"] = "2D";

                    hdnImageurl.Value = ChartImg.ImageUrl;
                    hdnImageType.Value = "2D";
                    hdnLegendUrl.Value = LegendImg.ImageUrl;
                }
            }
        }              


        // CALLBACK EVENT HANDLERS
        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string FilterXML = GetCallbackDataFilterXML(e.Parameters[0].ToString(),
             e.Parameters[1].ToString(),
             e.Parameters[2].ToString(),
             e.Parameters[3].ToString(),
             e.Parameters[5].ToString(),
             e.Parameters[9].ToString());

            AxisCriteria = "XAxis: " + e.Parameters[6].ToString() 
                         + "; Y Axis1: " + e.Parameters[7].ToString() 
                         + "; Y Axis2: " + e.Parameters[8].ToString();

             DataSet ChartDataset = null;
             try
             {
                 //Get chart data 
                 ChartDataset = SqlDataService.GetDualYAxisData(Int32.Parse(e.Parameters[0].ToString()),
                       Int32.Parse(e.Parameters[1].ToString()),
                       Int32.Parse(e.Parameters[2].ToString()),
                       FilterXML);
             }
             catch (Exception DataFetchException)
             {
                 ChartDataset = null;
             }
         
            //Plot chart
             if (ChartDataset != null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = e.Parameters[4].ToString();
                string ChartType = "2D";
                string ChartSeriesType = "Line";

                string YAxis1Label = ChartDataset.Tables[0].Rows[0]["Y1Unit"].ToString();
                string YAxis2Label = ChartDataset.Tables[0].Rows[0]["Y2Unit"].ToString();
                
                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];                    
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotDualYAxisChart(ChartDataset,
                       ThreeDChart,
                       ChartSeriesType,
                       YAxis1Label,
                       YAxis2Label);

                    TwoDChart.Visible = false;
                    targetChart.Visible = true;
                    targetChart.RenderControl(e.Output);
                }
                else
                {
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotDualYAxisChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        YAxis1Label,
                        YAxis2Label,
                        AxisCriteria);
                    TwoDimensionalCharts.AddCopyrightText(targetChart, Convert.ToInt32(targetChart.Width.Value) - 180);
                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;   

                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    #region Add extra legend
                    //Create the new legend entry box.
                    dotnetCHARTING.LegendBox newLegend = new dotnetCHARTING.LegendBox();

                    // Set the columns for the box to use. The first 3 columns refer to legend entry properties
                    // LegendEntry.Name, LegendEntry.Marker, and LegendEntry.Value.
                    newLegend.Template = "%Icon%Name";

                    // instantiate the legend entries with a name, value, and background color (LegendEntry.Marker will use this background).
                    dotnetCHARTING.LegendEntry entry1 = new dotnetCHARTING.LegendEntry(e.Parameters[7].ToString() + " - " + YAxis1Label, "", new dotnetCHARTING.Background(ColorTranslator.FromHtml("#002252")));
                    dotnetCHARTING.LegendEntry entry2 = new dotnetCHARTING.LegendEntry(e.Parameters[8].ToString() + " - " + YAxis2Label, "", new dotnetCHARTING.Background(ColorTranslator.FromHtml("#6c80bf")));

                    // Add the entries to our custom legend.
                    newLegend.ExtraEntries.Add(entry1);
                    newLegend.ExtraEntries.Add(entry2);

                    // The the new legend to the chart.
                    targetChart.ExtraLegendBoxes.Add(newLegend);

                    #region Set styles
                    newLegend.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
                    newLegend.CornerBottomLeft = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerBottomRight = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerTopLeft = dotnetCHARTING.BoxCorner.Square;
                    newLegend.CornerTopRight = dotnetCHARTING.BoxCorner.Square;
                    newLegend.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Arial", 8), Color.Black);
                    newLegend.Line = new dotnetCHARTING.Line(Color.White);
                    newLegend.Line.Color = Color.White;
                    //newLegend.Position = LegendBoxPosition.Top;
                    // Set an orientation for the custom legend.
                    newLegend.Orientation = dotnetCHARTING.Orientation.Bottom;
                    newLegend.Shadow.Color = Color.White;
                    newLegend.InteriorLine.Color = Color.White;
                    #endregion

                    #endregion

                    targetChart.LegendBox.Visible = false;

                    targetChart.FileManager.TempDirectory = "temp";
                    string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");                  

                    ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;
                    ChartImg.RenderControl(e.Output);
                    hdnImageurl.Value = strChartPath;
                    hdnImageurl.RenderControl(e.Output);
                    hdnImageType.Value = ChartType;
                    hdnImageType.RenderControl(e.Output);
                    ImageMapLabel.RenderControl(e.Output);

                    LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                    LegendImg.Visible = false;
                    divChart.RenderControl(e.Output);
                    hdnLegendUrl.Value = strLegendPath;
                    hdnLegendUrl.RenderControl(e.Output);
                    hdnTaxonomy11Selection.Value = e.Parameters[5].ToString();
                    hdnTaxonomy11Selection.RenderControl(e.Output);
                }               
            }
            else
            {
                ErrorMessagelabel.Text = "Data not available";
            }
            ErrorMessagelabel.RenderControl(e.Output);
        }

        public void UpdateChartfilters_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string YAxisValue;
            string FilterData = e.Parameters[2];
            string FilterDataText = e.Parameters[3];

            YAxisValue = e.Parameters[1].Equals("11") ?
                Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[0]))) :
                Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[0]))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[1])));

            ChartFilters1.YAxis = YAxisValue;
            ChartFilters1.LoadDataInCallback(FilterData, FilterDataText);
            ChartFilters1.RenderControl(e.Output);
        }

        // EXPORT EVENT HANDLERS       
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string chartTitle = GlobalSettings.SiteTitle;
            string chartType = "DualYAxisChart";

            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetDualYAxisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis1.Value),
                Int32.Parse(hdnYAxis2.Value),
                SelectionsXML);

            LogUsage("Chart - Dual YAxis - Word Download ", "DualYAxis chart1", " ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }     

            if (chartBitmap != null)
            {
                Document document = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                    chartBitmap,
                    chartType,
                    WordTemplatePath,
                    ChartTitle.Text);
                document.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }
        }
       
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;           
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            //string chartTitle = GlobalSettings.SiteTitle;
            string chartType = "DualYAxisChart";
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetDualYAxisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis1.Value),
                Int32.Parse(hdnYAxis2.Value),
                SelectionsXML);

            LogUsage("Chart - Dual YAxis - Excel Download ", "DualYAxis chart1", "");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            Workbook Excel;

            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0],
                            chartBitmap, 
                            legendBitmap,
                            chartType,
                            ExcelTemplatePath,
                            LogoPath,
                            ChartTitle.Text);
                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
        
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";        
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string chartTitle = GlobalSettings.SiteTitle;
            string chartType = "DualYAxisChart";

            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetDualYAxisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis1.Value),
                Int32.Parse(hdnYAxis2.Value),
                SelectionsXML);

            LogUsage("Chart - Dual YAxis - PPT Download ", "DualYAxis Chart1", " ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }     
            if (chartBitmap != null)
            {
                Presentation presn = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0], 
                    chartBitmap,
                    chartType,
                    PowerpointTempaltePath,
                    ChartTitle.Text);

                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presn.Write(Response.OutputStream);
                Response.Flush();
            }
        } 
    }
}
