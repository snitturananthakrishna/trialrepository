<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="PredefinedTables.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.PredefinedTables" 
        Title="Scrip 100" %>

<%@ Register Src="Controls/CheckListControl.ascx" TagName="CheckListControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/TreeviewControl.ascx" TagName="TreeviewControl" TagPrefix="uc2" %>
<%@ Register Src="Controls/SelectionList.ascx" TagName="Selections" TagPrefix="uc3" %>
<%@ Reference Control = "Controls/TreeviewControl.ascx" %>
<%@ Reference Control = "Controls/CheckListControl.ascx" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>        
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
         
    <div id="maincontent_full">
        
         <GMPT:PageTitleBar id="PageTitleBar1" runat="server"></GMPT:PageTitleBar>
         <div style="float:right;position:relative;margin-top:-35px"><GMPT:SavedSearch ID="SavedSearch1" runat="server"/> </div>
    <ul id="database_tabs">        
        <li style="display:none;"><a href="Results/CountryOverview.aspx" title="go to country view">Country Snapshot</a></li>
          <li><asp:HyperLink ID="SearchLink" runat="server" Text="Search" NavigateUrl="~/Search1.aspx" ToolTip="click to modify search"></asp:HyperLink></li>
        <li><a class="selected">League Tables</a></li>
        <li style="display:none;"><asp:LinkButton ID="ResultsLink" runat="server" OnClick="ResultsLink_Click" Text="View Results" ToolTip="click to view results"></asp:LinkButton></li>        
        <li style="display:none;"><asp:LinkButton ID="AnalysisLink" runat="server" OnClick="AnalysisLink_Click" Text="Chart Results" ToolTip="click to view chart results"></asp:LinkButton></li>                
        <GMPT:Glossary ID="Links" runat="server" />
        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       </li>
    </ul>
    
    <br />  

     <div class="marketdata_search">
        <h1>League Tables</h1>
          
        <asp:Repeater ID="PredefinedTablesList" runat="server" >
            <HeaderTemplate>
                <ul style="padding-left:15px;color:#001447;width:750px; " >
            </HeaderTemplate>
            <ItemTemplate>
                <li style="padding:4px; cursor:hand;border-bottom:1px;border-bottom-color:LightGrey;border-bottom-style:solid;">
                    <a onclick="window.open('PredefinedTableView.aspx?tableid=<%# DataBinder.Eval(Container.DataItem, "tableid") %>&heading= <%# DataBinder.Eval(Container.DataItem, "Heading")%>','TableView','toolbar=no,status=no,resizable=yes,scrollbars=yes,width=730,left=50,top=100,height=530 ');">
                            <%# DataBinder.Eval(Container.DataItem, "Heading")%></a>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
        <br />

    </div>             

<div class="button_right" style="width: 150px;" >
                   <asp:HyperLink Width="150px" ID="serachLink" runat="server" 
                   Text="Detailed Search" 
                   ToolTip="click to go detailed search"
                   NavigateUrl="~/Search1.aspx"></asp:HyperLink>
                </div>
             </div>
</asp:Content>

