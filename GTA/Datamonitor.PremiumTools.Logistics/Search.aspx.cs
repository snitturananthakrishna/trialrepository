using System;
using System.Text;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class Search : AuthenticationBasePage
    {
         // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            LoadUserSelectionsToHiddenField();
            if (!IsPostBack)
            {                
                //Load taxonomy selection links
                LoadTaxonomyTypes();
                //Bind years drop downs
                BindYearsDropdown();
                //LoadSavedSearches
                ((SavedSearch)SavedSearch1).LoadSavedSearches(GlobalSettings.DefaultResultsPage);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.CustomPageTitle = "Search";            
        }

        
        // PRIVATE METHODS (initial population)
       
        private void BindYearsDropdown()
        {
            //Get list of valid years from config
            string[] ValidYears = GlobalSettings.ValidYears;
            StartYear.DataSource = ValidYears;
            StartYear.DataBind();
            EndYear.DataSource = ValidYears;
            EndYear.DataBind();
            
            if (Session["StartYear"] != null)
            {
                StartYear.SelectedValue = CurrentSession.GetFromSession<int>("StartYear").ToString();
            }
            else
            {
                try
                {
                    StartYear.SelectedValue = GlobalSettings.StartYearSelectValue; 
                    CurrentSession.SetInSession<int>("StartYear", Convert.ToInt32(StartYear.SelectedValue));
                }
                catch (Exception ex) { }
            }
            //Update session
            if (Session["EndYear"] != null)
            {
                EndYear.SelectedValue = CurrentSession.GetFromSession<int>("EndYear").ToString();
            }
            else
            {
                try
                {
                    EndYear.SelectedValue = GlobalSettings.EndYearSelectValue;
                    CurrentSession.SetInSession<int>("EndYear", Convert.ToInt32(EndYear.SelectedValue));
                }
                catch (Exception ex) { }
            }           
        }

        private void BindPredefinedViews()
        {
            //DataSet Views = GlobalSettings.GetPredefinedViews();
            //PredefinedViews.DataSource = Views;
            //PredefinedViews.DataTextField = "name";
            //PredefinedViews.DataValueField = "id";
            //PredefinedViews.DataBind();
            //PredefinedViews.Items.Insert(0, new ListItem("---Select---", string.Empty));

            //PredefinedViews.Attributes.Add("onchange", "javascript:PredefinedViews_onChange();");            
            //try
            //{
            //    object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");
            //    PredefinedViews.SelectedValue = ViewID != null ? (string)ViewID : "0";
            //}
            //catch (Exception ex) { }

        }               
        
       private void LoadDefaultTaxonomyTypeControl(int taxonomyTypeID,
            string taxonomyType,
            string controlType,
           string fullTaxonomyTypeID,
           string parentIds,
           string flagcheckboxText)
        {
            SelectedTaxonomy.Text = taxonomyType;
            BaseSelectionControl SelectionControl;

            switch (controlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }

            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = fullTaxonomyTypeID;
            SelectionControl.ParentIDs = parentIds;
            SelectionControl.Source = "search";
            SelectionControl.Visible = true;
            SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInSearch;
            SelectionControl.FlagChecked = false;
            SelectionControl.FlagCheckboxText = flagcheckboxText;
            SelectionControl.LoadData();  

            controlHolder.Controls.Add(SelectionControl);         

            StringBuilder Selections = new StringBuilder();
            Selections.Append("<script language='javascript' type='text/javascript'>");
            Selections.Append(string.Format("renderControl('{0}','{1}','{2}','{3}','{4}','{5}',false,'{6}');",
                                taxonomyTypeID.ToString(),
                                taxonomyType,
                                controlType,
                                "false",
                                fullTaxonomyTypeID,
                                parentIds,
                                flagcheckboxText));
            Selections.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadDefaultControl", Selections.ToString(), false);
            hdnFullTaxonomyTypeID.Value = fullTaxonomyTypeID;
        }

        public void LoadTaxonomyTypes()
        {
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
            TaxonomyTypesList.DataSource = TaxonomyTypes;
            TaxonomyTypesList.DataBind();

            //Load a taxonomy type details
            if (TaxonomyTypes != null &&
                TaxonomyTypes.Tables.Count > 0 &&
                TaxonomyTypes.Tables[0].Rows.Count > 0)
            {
                int DefaultRow = GlobalSettings.DefaultTaxonomyTypeRowNumber;
                LoadDefaultTaxonomyTypeControl(Convert.ToInt32(TaxonomyTypes.Tables[0].Rows[DefaultRow]["ID"].ToString()),
                                TaxonomyTypes.Tables[0].Rows[DefaultRow]["Name"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[DefaultRow]["ControlType"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[DefaultRow]["FullID"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[DefaultRow]["ParentIds"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[DefaultRow]["FlagcheckboxText"].ToString());
            }
        }

        private void LoadUserSelectionsToHiddenField()
        {            
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString());
                    if (CurrentSelections != null && CurrentSelections.Count > 0) //Check if the taxonomy data exists in session
                    {
                        foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                        {
                            if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())                                
                            {
                                if (TaxonomyType["type"].ToString().ToLower() != "static")
                                {
                                    hdnUserSelections.Value += TaxonomyType["ID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|";
                                }
                            }
                            else
                            {
                                hdnUserSelections.Value += TaxonomyType["ID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|" +
                                    TaxonomyType["FullID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|";
                            }
                        }
                    }
                }
            }
        }
       
        //PUBLIC METHODS

        public string GetVisiblity(string id)
        {
            object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");

            if (ViewID != null)
            {
                string SelectableTaxonomyTypes = GetPredefinedViewSelectableIDs((string)ViewID);
                SelectableTaxonomyTypes = "," + SelectableTaxonomyTypes + ",";

                if (SelectableTaxonomyTypes.Length > 0 && !SelectableTaxonomyTypes.Contains(","+id+","))
                {
                    return "disabled";
                }
            }

            return "";
        }

        public string GetMandatoryTaxonomy()
        {
            string MandatoryTaxonomy = string.Empty;

            MandatoryTaxonomy = GlobalSettings.GetMandatoryTaxonomyInSearch;

            return MandatoryTaxonomy;
        }
        
        public string GetMandatoryTaxonomyTypeIDs()
        {
            return GlobalSettings.MandatoryTaxonomyTypeIDs;
        }
        
       
        // EVENT HANDLERS

        public void CallBack1_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {            
            int taxonomyTypeID = Convert.ToInt32(e.Parameters[0]);
            string taxonomyType = e.Parameters[1].ToString();
            string ControlType = e.Parameters[2].ToString();
            string FullTaxonomyTypeID = e.Parameters[3].ToString();
            string ParentIds = e.Parameters[4].ToString();
            bool IsFlagCheckboxChecked = Convert.ToBoolean(e.Parameters[6].ToString());
            string strFlagCheckboxText = e.Parameters[7].ToString();

            //update current selections to session
            if (e.Parameters[5].ToString().Trim().Length > 0)
            {
                SessionHandler.UpdateSessionWithSelections("", "", e.Parameters[5].ToString(), "addGroupSelection", "");
            }
           
            SelectedTaxonomy.Text = taxonomyType;
            SelectedTaxonomy.RenderControl(e.Output);

            BaseSelectionControl SelectionControl;

            switch (ControlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");                    
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");                    
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }
          
            controlHolder.Controls.Add(SelectionControl);

            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = FullTaxonomyTypeID;
            SelectionControl.ParentIDs = ParentIds;
            SelectionControl.Source = "search";
            SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInSearch;
            SelectionControl.Visible = true;
            SelectionControl.FlagChecked = IsFlagCheckboxChecked;
            SelectionControl.FlagCheckboxText = strFlagCheckboxText;
            SelectionControl.LoadData();

            //check if any comments available for this taxonomy type
            string Path = Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath);
            XmlDocument TaxonomyTypes = new XmlDocument();
            TaxonomyTypes.Load(Path);
            XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//TaxonomyTypes/TaxonomyType[@ID='" + taxonomyTypeID.ToString() + "']");
            if (TaxonomyType != null && 
                TaxonomyType.Attributes.GetNamedItem("Note")!=null&&
                !string.IsNullOrEmpty(TaxonomyType.Attributes.GetNamedItem("Note").Value))
            {
                NoteLabel.Text = "Note: " + TaxonomyType.Attributes.GetNamedItem("Note").Value;
            }
            else
            {
                NoteLabel.Text = "";
            }

            //Render the controls
            SelectionControl.RenderControl(e.Output);
            NoteLabel.RenderControl(e.Output);

        }
       
        protected void PredefinedViews_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Re bind (enable/disable) taxonomy links in left pane
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            TaxonomyTypesList.DataSource = TaxonomyTypes;
            TaxonomyTypesList.DataBind();


        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            SessionHandler.UpdateSessionWithSelections("", "", hdnUserSelections.Value, "addGroupSelection","");

            Response.Redirect(GlobalSettings.DefaultResultsPage);
        }
        
        protected void AnalysisLink_Click(object sender, EventArgs e)
        {
            SessionHandler.UpdateSessionWithSelections("", "", hdnUserSelections.Value, "addGroupSelection", "");

            Response.Redirect("analysis/countrycomparison.aspx");
        }

        public string GetResultsPage()
        {
            return GlobalSettings.DefaultResultsPage;
        }
    }
}
