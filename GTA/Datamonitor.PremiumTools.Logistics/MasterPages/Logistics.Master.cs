using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.MasterPages
{
    public partial class Logistics : BaseMasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["KCUserId"] == null)
            {
                string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                Response.Redirect(strOvumUrl, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KCUserId"] == null)
            {
                string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                Response.Redirect(strOvumUrl, true);
            }

            if (!Page.IsPostBack)
            {
                if (ConfigurationManager.AppSettings["OvumICTLink"] != null)
                {
                    lnkOvumICT.HRef = ConfigurationManager.AppSettings["OvumICTLink"].ToString();
                }
                if (ConfigurationManager.AppSettings["OvumTelecomsLink"] != null)
                {
                    lnkOvumTelecoms.HRef = ConfigurationManager.AppSettings["OvumTelecomsLink"].ToString();
                }
                if (ConfigurationManager.AppSettings["PrivacyPolicyLink"] != null)
                {
                    lnkPrivacyPolicy.HRef = ConfigurationManager.AppSettings["PrivacyPolicyLink"].ToString();
                }
                if (ConfigurationManager.AppSettings["TermsOfUseLink"] != null)
                {
                    lnkTermsofuse.HRef = ConfigurationManager.AppSettings["TermsOfUseLink"].ToString();
                }
                if (ConfigurationManager.AppSettings["CookiesLink"] != null)
                {
                    lnkCookies.HRef = ConfigurationManager.AppSettings["CookiesLink"].ToString();
                }
            }

            string AbsoluteUri = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            if(AbsoluteUri.IndexOf("?")>-1)
            {
                AbsoluteUri = AbsoluteUri.Remove(AbsoluteUri.IndexOf("?"));
            }
            AbsoluteUri = Request.ApplicationPath.Length > 1 ? 
                AbsoluteUri + Request.ApplicationPath : 
                AbsoluteUri;

            sourceCSS.Attributes.Add("href", AbsoluteUri + "/assets/css/datamonitor.css");

            //TODO: Remove it
            //string SourceKC1 = "telecoms";

            if (!string.IsNullOrEmpty(SourceKC))
            {
                switch (SourceKC.ToLower())
                {
                    case "bgcio":
                        //Load footer info
                        footerbutlergroup.Visible = true;
                        footerdefault.Visible = false;
                        break;
                    case "ovumit":
                    case "telecoms":
                        headerOvumNew.Visible = true;
                        header.Visible = false;
                        footer.Visible = false;
                        footerNew.Visible = true;
                        cssLink_New.Attributes.Add("href", AbsoluteUri + "/assets/css/ovum.css");                        
                        sourceCSS.Attributes.Add("href", AbsoluteUri + "/assets/css/global.css");

                        //if (ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"] != null)
                        //{
                        //    lnkEditProfile.HRef = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                        //}

                        if (Session["KCUserId"] != null)
                        {
                            userName.InnerText = Session["KCUserId"].ToString();
                        }

                        break;
                    case "vendor":
                    case "enterprise":
                        footerOvum.Visible = true;
                        footerdefault.Visible = false;
                        sourceCSS.Attributes.Add("href", AbsoluteUri + "/assets/css/ovum.css");                        
                        break;

                    case "premium_tool_kc_dm":
                        sourceCSS.Attributes.Add("href", AbsoluteUri + "/assets/css/datamonitor.css");
                        cssLink_New.Attributes.Add("href", "/assets/css/removesearch.css");
                        break;

                    case "premium_tool_kc_ovum":
                        footerOvum.Visible = true;
                        footerdefault.Visible = false;
                        sourceCSS.Attributes.Add("href", AbsoluteUri + "/assets/css/ovum.css");
                        cssLink_New.Attributes.Add("href", "/assets/css/removesearch.css");
                        break;
                }
            }
        }

        /// <summary>
        /// Handles the click event of Logout control. This Event is used to redirect to ICTOP Help page. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();

            string strOvumUrl = "http://www.ovumkc.com/user/logout";
            Response.Redirect(strOvumUrl, true);
        }

        /// <summary>
        /// Handles the click event of search control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(SearchText.Value) == false && SearchText.Value.Trim().Length > 0)
            {
                string redirectURL = string.Format("http://www.ovumkc.com/content/search?SearchText={0}&SearchType=Standard", Server.UrlEncode(SearchText.Value));
                Response.Redirect(redirectURL, true);
            }
        }
    }
}
