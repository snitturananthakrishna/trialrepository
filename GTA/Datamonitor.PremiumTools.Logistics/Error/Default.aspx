        <%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="Default.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Error.Default"        
        %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
              <div id="pagelogin" class="company_styles" runat="server" style="width:100%;" >
                    <div id="div404" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Page Cannot Be Found</b></p><br />
                          <p align="center">Unfortunately, the page you are trying to retrieve does not exist on the Knowledge Center.</p>
                          <p align="center">This is the same as a &quot;HTTP 404: File not found&quot; error and technical support team has already been alerted.</p>
                          <br />
                          <p align="center">Go to <b><asp:HyperLink ID="SearchLink1" runat="server" Text="Home Page"></asp:HyperLink></b>
                          </p>
                      </div>
                      <div id="div403" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Access Denied</b></p><br />
                          <p align="center">The resource you are trying cannot be accessed.</p>
                          <br />
                          <p align="center">Go to <b><asp:HyperLink ID="SearchLink2" runat="server" Text="Home Page"></asp:HyperLink></b> 
                          </p>
                      </div>
                      <div id="divMDAAccessDenied" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Access Denied</b></p><br />
                          <p align="center">You do not have access to our full Market Data programme.</p>
                          <br />
                          <p align="center">Go to <b><asp:HyperLink ID="SearchLinkHome" runat="server" Text="Home Page"></asp:HyperLink></b> 
                          </p>
                      </div>
                       <div id="divError" runat="server" visible="false"  >
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Sorry.. an error has occured</b></p><br />
                          <p align="center">Unfortunately, the page you are trying to retrieve cannot be displayed and technical support team has already been alerted.</p>              
                          <br /><br />
                          <p align="center">Go to <b><asp:HyperLink ID="SearchLink3" runat="server" Text="Home Page"></asp:HyperLink></b> 
                          </p>
                      </div>
                      <div id="divSessionExpired" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Sorry.. your session expired</b></p><br />                          
                          <br /><br />
                          <p align="center">Go to <b><asp:HyperLink ID="SearchLink4" runat="server" Text="Home Page"></asp:HyperLink></b> 
                          </p>
                      </div>
                </div>

</asp:Content>