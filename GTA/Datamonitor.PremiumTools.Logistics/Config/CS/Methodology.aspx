<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Methodology.aspx.cs" Inherits="Datamonitor.PremiumTools.Logistics.Config.CS.Methodology" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Country Statics Methodology</title>
    <link href="../../assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/css/layout.css" rel="stylesheet" type="text/css"/>
</head>
<body>
 <div id="wrapper">
 
    <form id="form1" runat="server">
    <div id="pagetitle" style="margin:5px;"><h1>Country Statistics: Methodology</h1></div><br />
	            <div align="right">
	            <a href="javascript:window.close()">Close this window</a>
	            </div>

 <br />
    <div style="float:right;width:500px;margin:5px;">
    <ComponentArt:TreeView  ID="tvForAll" runat="server"  
        ShowLines="true" 
        LineImagesFolderUrl="~/assets/images/tvlines" 
        ImagesBaseUrl="~/assets/images/tvlines"
        ClientSideOnNodeCheckChanged="tvForAll_onNodeCheckChange" 
        Height="600px"
        width="500px" >
                    <ClientTemplates>
                     <ComponentArt:ClientTemplate id="NT">
                              <div>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate id="MT">
                            <div>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;"  target="_blank"
                                href="Methodologies/## DataItem.get_value() ##.pdf">
                                <img alt="View Definition" border="0" src="../../Assets/Images/pdf_icon.gif" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                                 </ClientTemplates>
                </ComponentArt:TreeView>
  
    </div>
       <div style="float:left;width:400px;margin:5px;">
    <p>Document below highlights the overall methodology adopted by the Country Statistics Database in brief. </p>
<asp:HyperLink ID="pdfDownload" runat="server" 
    ImageUrl="/assets/images/pdf_icon_large.jpg" 
    NavigateUrl="Methodology.pdf"
    Target="_blank"></asp:HyperLink>
    <br />
<p>For detailed indicator specific information, select the methodology against the required indicator from the indicator tree on the right.</p>
<br /><p>The document includes detailed information on the Definition, Years and Country Coverage, Sources Used, and the Methodology adopted to prepare the dataset.</p>

<br /><br /><p>For further assistance, drop an email at csqueries@datamonitor.com</p>

    </div>
        
    </form>
    </div>
</body>
</html>
