using System;
using System.Data;
using System.Text;
using System.Web;
using System.Collections;
using System.Net.Mail;
using System.Configuration;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{    
    public class AskAnAnalyst : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string status = "fail";
            string Name = context.Request.QueryString["name"];
            string Email = context.Request.QueryString["email"];
            string Tel = context.Request.QueryString["tel"];
            string Question = context.Request.QueryString["question"];
            string ContactMethod = context.Request.QueryString["contact"];
            string SourceKC = HttpContext.Current.Session["SourceKC"] == null ? null : HttpContext.Current.Session["SourceKC"].ToString(); ;

            string DisableWelcomeMsgUser = context.Request.QueryString["welcomeuser"];


            if (SourceKC!= null && !string.IsNullOrEmpty(Email))
            {
                SmtpClient objSmtpClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage objMessage;

                #region Build mail message
                StringBuilder sbMailIds = new StringBuilder();
                StringBuilder sbBody = new StringBuilder();

                sbBody.Append("<span style='font-family:Arial;font-size:12px;'>");

                if (Question != string.Empty)
                {
                    sbBody.Append(Question + "<br /><br />");
                }
                sbBody.Append("Regards,");
                sbBody.Append("<br />" + Name);
                sbBody.Append("<br />" + Email);
                sbBody.Append("<br />Phone: " + Tel);
                sbBody.Append("<br /><br />Preferable Contact Method: " + ContactMethod);

                sbBody.Append("</span>");

                string strBody = sbBody.ToString();
                #endregion

                string ToMailId = ConfigurationManager.AppSettings[SourceKC + "SupportEmailId"].ToString();

                objMessage = new MailMessage(Email, ToMailId, "User Question on " + GlobalSettings.SiteTitle, strBody);
                objMessage.IsBodyHtml = true;

                objSmtpClient.Send(objMessage);

                status = "success";
            }

            if (!string.IsNullOrEmpty(DisableWelcomeMsgUser))
            {
                SqlDataService.SetWelcomePageVisibility(DisableWelcomeMsgUser, false);

                status = "success";

            }

            //Return status
            context.Response.Clear();
            context.Response.CacheControl = "no-cache";
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.Expires = -1;
            context.Response.ContentType = "text/plain";
            context.Response.Write(status);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
