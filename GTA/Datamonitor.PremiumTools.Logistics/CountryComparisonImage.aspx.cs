using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using dotnetCHARTING;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Results
{
    public partial class CountryComparisonImage : System.Web.UI.Page
    {

        private static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), Color.FromArgb(108, 128, 191), Color.FromArgb(165, 174, 204), Color.FromArgb(210, 217, 229), Color.FromArgb(216, 69, 25), Color.FromArgb(255, 102, 0), Color.FromArgb(255, 172, 112), Color.FromArgb(250, 222, 199), Color.FromArgb(255, 153, 0), Color.FromArgb(255, 204, 0) };


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ChartData = null;

            if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
                Request.QueryString["countries"] != null && !string.IsNullOrEmpty(Request.QueryString["countries"].Trim()) &&
                Request.QueryString["selectedyear"] != null && !string.IsNullOrEmpty(Request.QueryString["selectedyear"].Trim()))
            {
                string RowID = Request.QueryString["rowid"].Trim();
                string SelectedCountries = Request.QueryString["countries"].TrimEnd(',');
                int SelectedYear = Convert.ToInt32(Request.QueryString["selectedyear"].Trim());

                ChartData = SqlDataService.GetCountryComparisonImageData(Convert.ToInt64(RowID), SelectedCountries, SelectedYear);          
            }

            if (ChartData != null && 
                ChartData.Tables.Count > 0 &&
                ChartData.Tables[0].Rows.Count>0)
            {
                ChartTitle.Text = ChartData.Tables[0].Rows[0]["Indicator"].ToString();
                PlotChart(ChartData, SampleChart, "7");
            }            
        }

        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotChart(DataSet chartDataset,
                                Chart chart,
                                string chartSeriesType)
        {
            chart.Use3D = false;
            chart.Visible = true;
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName ;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();

                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                string yAxis1Text = chartDataset.Tables[0].Columns[1].ColumnName;
                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxis1Text;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
               
                //Set chart series types
                for (int i = 0; i < chart.SeriesCollection.Count; i++)
                {
                    chart.SeriesCollection[i].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                }

                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Width = 325;
                chart.Height = 175;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles
                chart.LegendBox.Visible = false;                

                chart.DefaultElement.ToolTip = "%SeriesName";
               
            }

            return chart;
        }

        /// <summary>
        /// Plots the Dual YAxis chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        private void PlotDualYAxisChart(DataSet chartDataset,
                                   Chart chart,
                                   string yAxis1Text,
                                   string yAxis2Text,
                                   string AxisCriteria)
        {
            chart.Use3D = false;
            chart.Visible = true;
            chart.Debug = false;
            //chart.FileManager.TempDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;

            //DataView yAxisDataview = chartDataset.Tables[0].DefaultView;
            //yAxisDataview.RowFilter = "yType= Value";
            //DataTable yAxisDatatable = yAxisDataview.ToTable();

            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                //Set chart styles
                SampleChart.FileManager.ImageFormat = ImageFormat.Png;
                SampleChart.FileQuality = 100;
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.Width = 325;
                chart.Height = 175;
                chart.LegendBox.Visible = false;
                //Set axis label texts and styles

                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;

                DataEngine de = new DataEngine();
                DataEngine de1 = new DataEngine();

                SeriesCollection sc;
                SeriesCollection sc1 = new SeriesCollection();
                chart.SeriesCollection.Clear();
                de.Data = chartDataset;
                de.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName;
                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                sc = de.GetSeries();

                sc[0].XAxis = new dotnetCHARTING.Axis();
                sc[0].YAxis = new dotnetCHARTING.Axis();
                sc[0].XAxis.Label.Text = xAxisText;
                sc[0].YAxis.Label.Text = yAxis1Text;
                sc[0].Name = yAxis1Text;

                de1.Data = chartDataset;
                de1.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName;
               
                sc1 = de1.GetSeries();

                for (int i = 0; i < sc1.Count; i++)
                {
                    sc.Add(sc1[i]);
                }
                for (int i = 1; i < sc.Count - 1; i++)
                {
                    sc[i].XAxis = sc[0].XAxis;
                    sc[i].YAxis = sc[0].YAxis;
                }

                if (sc.Count == 2)
                {
                    sc[1].XAxis = sc[0].XAxis;
                }
                //chart.Type = dotnetCHARTING.ChartType.Combo;
                chart.SeriesCollection.Add(sc);

                sc[1].YAxis = new dotnetCHARTING.Axis();
                sc[1].YAxis.Orientation = dotnetCHARTING.Orientation.Right;
                sc[1].YAxis.LabelRotate = true;

                if (GlobalSettings.TrendChartAbsoluteDataChartType.ToLower() == "auto")
                {
                    sc[0].Type = chartDataset.Tables[0].Rows.Count > 15 ? SeriesType.AreaLine : SeriesType.Bar;
                }
                else
                {
                    sc[0].Type = GetSeriesType(GlobalSettings.TrendChartAbsoluteDataChartType);
                }
                if (GlobalSettings.TrendChartGrowthDataChartType.ToLower() == "auto")
                {
                    sc[1].Type = SeriesType.Line;
                }
                else
                {
                    sc[1].Type = GetSeriesType(GlobalSettings.TrendChartGrowthDataChartType);
                }
                switch (GlobalSettings.TrendChartType.ToLower())
                {
                    case "absoluteonly":
                        sc[0].Visible = true;
                        sc[1].Visible = false;
                        break;
                    case "growthonly":
                        sc[0].Visible = false;
                        sc[1].Visible = true;
                        break;
                    default:
                        sc[0].Visible = true;
                        sc[1].Visible = true;
                        break;
                }
                
                sc[1].Line.Width = 1;
                sc[1].Name = yAxis2Text;
                sc[1].YAxis.Label.Text = yAxis2Text;

                chart.DataBind();

                sc[0].XAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].YAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].XAxis.Line.Color = Color.DarkGray;
                sc[0].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].YAxis.Line.Color = Color.DarkGray;
                sc[0].YAxis.DefaultTick.Line.Color = Color.DarkGray;                
                sc[0].DefaultElement.Marker.Type = ElementMarkerType.None;
                sc[0].YAxis.ShowGrid = false;
                sc[0].XAxis.ShowGrid = false;
                sc[0].YAxis.AlternateGridBackground.Color = Color.White;

                sc[1].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[1].XAxis.Line.Color = Color.DarkGray;
                sc[1].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[1].YAxis.Line.Color = Color.DarkGray;
                sc[1].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[1].DefaultElement.Marker.Type = ElementMarkerType.None;
                sc[1].YAxis.ShowGrid = false;
                sc[1].XAxis.ShowGrid = false;
                sc[1].YAxis.AlternateGridBackground.Color = Color.White;

                chart.DefaultAxis.ShowGrid = false;
                chart.DefaultAxis.AlternateGridBackground.Color = Color.White;

                chart.BackColor = Color.White;



                
            }
            chart.LegendBox.Template = "%Icon";
            chart.LegendBox.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
            chart.LegendBox.CornerBottomLeft = BoxCorner.Square;
            chart.LegendBox.CornerBottomRight = BoxCorner.Square;
            chart.LegendBox.CornerTopLeft = BoxCorner.Square;
            chart.LegendBox.CornerTopRight = BoxCorner.Square;
            chart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Verdana", 7), Color.Black);
            chart.LegendBox.Line = new dotnetCHARTING.Line(Color.White);
            chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            chart.LegendBox.Line.Color = Color.White;
            chart.LegendBox.Shadow.Color = Color.White;
            chart.LegendBox.InteriorLine.Color = Color.White;
            
        }

        private SeriesType GetSeriesType(string typeString)
        {
            switch (typeString.ToLower())
            {
                case "bar": return SeriesType.Bar;
                case "line": return SeriesType.Line;
                case "area": return SeriesType.AreaLine;
                default: return SeriesType.Bar;
            }
        }
    }
}
