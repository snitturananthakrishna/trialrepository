<%@ Page Language="C#" 
    MasterPageFile="~/MasterPages/Logistics.Master"  
    AutoEventWireup="true"
    Codebehind="CountryOverview.aspx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.CountryOverview"
    Theme="NormalTree" 
    EnableEventValidation="false"
    EnableViewStateMac="false"
    ViewStateEncryptionMode="never"
    ValidateRequest="false"%>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="~/Controls/CountrySnapshotOptions.ascx" TagName="CountrySnapshotOptions" TagPrefix="GMPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
    <link href="../assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
    <script src="../assets/Scripts/googleMap.js" type="text/javascript"></script>


    <style type="text/css">
        .rightdiv
        {
            float:right;
            margin-bottom:4px;
        }
        .leftdiv
        {
            float:left;
            width:60px;
        }
        input[type=checkbox] 
        {
	        width:5px;
	        margin-left:-65px;
	        margin-right:-65px;
	        padding:0px;
	        text-align:left;
        }
        .canBold
        {
            font-weight:bold;
            border-bottom-color:gray;
            border-bottom-style:solid;
            border-bottom-width:1px;
        }
        .notBold
        {            
            border-bottom-color:gray;
            border-bottom-style:solid;
            border-bottom-width:1px;            
        }
                
        .lvl0
        {            	        
            margin:0 0 0 0;
	        padding-left:0px;
	 
        }        
                
        .lvl1
        {            	        
            margin:0 0 0 0;
	        padding-left:10px;
	 
        }        
        
        .lvl2
        {            	        
            margin:0 0 0 0;
	        padding-left:20px;
	 
        }                
        .browse_table TH {
	       
	        height:18px;
	        border:0px;
        }
        .browse_table TD
         {	       
	        border:0px;
        }
    </style>

    <script type="text/javascript">
        
        function getValueType(dataitem, fieldName)
        {        
            return "Actual";
        }
        
        function getAnchorVisibility(val)
         {
             if(val.length>0)
             {     
                return "block";
             }
             else
             {
                return "none";
             }
         }
         
        function CountryCombo_onChange(sender, eventArgs)
        {   
            var item = sender.getSelectedItem();
            
            document.getElementById('<%=hdnCountry.ClientID %>').value = item .get_value(); 
            document.getElementById('<%=hdnCountryName.ClientID %>').value = item .get_text();
        }
        function getAnchorVisibility(val)
         {
             if(val.length>0)
             {     
                return "block";
             }
             else
             {
                return "none";
             }
         }
                
        $(document).ready(function() {
            var country = $("#<%= hdnCountryMapName.ClientID%>").val();
            var zoomLevel = $("#<%= hdnZoomLevel.ClientID%>").val();
            
            //alert(zoomLevel);
            
            var script = "initialize('map_canvas');showAddress('"+country.replace("Total ", "")+"',"+zoomLevel+");"            
            setTimeout(script ,1000);
        });
        
    </script>
    <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=AIzaSyDU0TuJuIkfdtsnwnDxV_oeROJFo3WSL-o" type="text/javascript"></script>
    <div id="maincontent_full">
        <GMPT:PageTitleBar ID="PageTitleBar1" runat="server"></GMPT:PageTitleBar>        
         <ul id="database_tabs">
            <%--<li><asp:HyperLink ID="SearchLink" runat="server" Text="Search"></asp:HyperLink></li>--%>
             <li><a class="selected">Country Snapshot</a></li>
             <li><asp:LinkButton ID="SearchLink" runat="server" OnClick="SearchLink_Click" Text="Search" ToolTip="click to modify search"></asp:LinkButton></li>
            <li><asp:LinkButton ID="ResultsLink" runat="server" OnClick="ResultsLink_Click" Text="View Results"  ToolTip="click to view results"></asp:LinkButton></li>        
            <li><asp:LinkButton ID="AnalysisLink" runat="server" OnClick="AnalysisLink_Click" Text="Chart Results" ToolTip="click to view chart results"></asp:LinkButton></li>
            <GMPT:Glossary ID="Links" runat="server" />
            <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;            </li>   
        </ul> 
        <%--<GMPT:CountrySnapshotOptions id="CountrySnapshotOptions" runat="server" Source="browser"></GMPT:CountrySnapshotOptions>--%>
        <%--<br />--%> 
        <div>
            <div style="padding-bottom:8px; float: right;">
                <table><tr><td>
                    
                <div id="CountryDiv" runat="server" style="float: left; z-index:8;">
                    <table>
                        <tr>                        
                            <td>

                 Select Country
                            </td>
                            <td>
                                 <ComponentArt:ComboBox
                                    Id="CountryCombo" CssClass="comboBox" HoverCssClass="comboBoxHover" FocusedCssClass="comboBoxHover"
                                    TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown" ItemCssClass="TreeNode" 
                                    ItemHoverCssClass="TreeNode" SelectedItemCssClass="NodeSelected"
                                    DropHoverImageUrl="../assets/images/drop_hover.gif" DropImageUrl="../assets/images/drop.gif"
                                    Width="180" DropDownHeight="220" DropDownWidth="177"
                                    RunAt="Server" AutoTheming="true" AutoHighlight="false" AutoComplete="true" AutoFilter="true"
                                    DataTextField="Country" DataValueField="CountryID"
                                  >
                                  <ClientEvents>
                                  <Change EventHandler="CountryCombo_onChange" />
                                  </ClientEvents>
                                  </ComponentArt:ComboBox>
                            </td>
                        </tr>
                    </table>                              
                </div>
                </td><td>
                <div class="button_right_chart" style="width: 120px;">
                    <asp:LinkButton ID="GoLink" runat="server" Text="go" OnClick="GoLink_Click" ToolTip="click to update results">
                    </asp:LinkButton>
                </div>
                </td></tr>
                <tr>
                     <td colspan="4" align="right">Extract To : <asp:LinkButton ID="lnkExcel" runat="server" OnClick="lnkExcel_Click"  >
                        <img width="12" height="10" border="0" src="../assets/images/tool_excel.gif" alt="extract country data to excel" />Excel</asp:LinkButton>
                     </td>
                     <td colspan="2" style="display:none;">
                        <asp:LinkButton ID="LinkButton2" Text="Word" runat="server" OnClick="lnkWord_Click">
                        <img width="12" height="10" border="0" src="../assets/images/tool_word.gif"  alt="extract country data to word"/>Word</asp:LinkButton>                     
                     </td>
                </tr>
                </table>
                 
            </div>
            <div style="float: left; color:#1E3267; padding-top:15px;font-size:16px; font-weight:bold;">
            <asp:Label ID="CountryName" runat="server"></asp:Label>
            </div>
     </div>
        <div class="browse_styles" style="width:100%">
        <div class="bucket_full">
            <div class="bucket_left">
             <h5>Map </h5>
                <div id="map_canvas" style="width: 357px; height: 240px; overflow:hidden;z-index: 1"></div>
            </div>            
        
         <div class="bucket_right">
         <h5><asp:Label ID="KeyStatisticsLabel" runat="server"></asp:Label> </h5>
            <asp:Repeater ID="KeyStatisticsRepeater" runat="server">
            <HeaderTemplate>                
                <table width="100%" cellspacing="2">                  
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="height:19px;"><%#Eval("Indicator")%></td>                    
                    <td align="right">
                        <a style="color: Red; text-decoration: none;display:<%# DisplayAnchor(Eval("rowid").ToString()) %>" 
                            href="TrendImage.aspx?rowid=<%# Eval("rowid") %>&type=countryoverview"                                                 
                            onclick="return hs.htmlExpand(this, { contentId: 'PopupChart', objectType: 'iframe', preserveContent: false} )">
                            <asp:Image alt="View Trend Chart" Width="70" ID="IndicatorChart" ImageUrl='<%# ResolveUrl("~/imagegenerator.aspx?rowid="+Eval("itemID")+"&countryid="+Eval("countryID"))%>' runat="server" />
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
            
            <asp:Label ID="QuartileLabel" runat="server"></asp:Label> 
             <asp:Repeater ID="QuartileRepeater" runat="server">            
            <HeaderTemplate>                
                <table width="100%">                
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td height="18px;"><%#Eval("Indicator")%></td>
                    <td class='<%# "td"+Eval("Rank")%>'>
                        <div style="padding: 0px; font-weight: bold; text-align: center;">
                            <a style="color: Red; text-decoration: none; color:black" 
                                title="view all country ranks"
                                href="TrendImage.aspx?rowid=<%# Eval("rowID") %>&year=<%# GetQuartileYear() %>&countryid=<%# Eval("countryID") %>"                                 
                                onclick="return hs.htmlExpand(this, { contentId: 'QuartileStatus', objectType: 'iframe', preserveContent: false} )">
                                <%#Eval("Rank")%>
                            </a>                 
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
         </div>
         <div class="bucket_middle">
         
          <h5>Macroeconomic</h5>
          <table width="100%" border="0">
          <tr>
          <td><asp:Image ID="flag" runat="server" /><asp:Label runat="server" ID="FlagLabel" Text=""></asp:Label> </td>
          <td align="right"></td></tr>                                    
           <asp:Repeater ID="countrydata" runat="server">         

              <ItemTemplate>
                <tr>
                    <td height="20px" width="90"><%#Eval("Indicator")%></td>                    
                    <td align="right"><%#Eval("Value")%></td>   
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
         </asp:Repeater><br />
         
         <div id="ContryProfileSection" runat="server">           
         <h5>In-depth Insight</h5><div style="height:8px;"></div>
           <span style="font-size:12px; padding-top:5px; background-image:url('../assets/images/pdf_icon.gif'); background-repeat:no-repeat; background-position-x:right;">           
                <asp:HyperLink ID="CountryProfilePDFLink" runat="server" Width="100%"> Country Profile (PDF) </asp:HyperLink> 
                <%--<img src="../assets/images/pdf_icon.gif" />--%>
            </span>           
         </div>
         </div>        
         </div>  
          <div class="bucket_full">
            <div id="OverviewDescriptionDiv" runat="server">
                <div style="color:#1E3267; padding-top:10px;padding-bottom:2px;font-size:16px; font-weight:bold;">
                    <div class="divider">Overview</div>
                </div>
                <div style="padding-bottom:5px;padding-top:0px">
                
                    <asp:Label ID="OverviewLabel" runat="server"></asp:Label>            
                    
                </div>
            </div>
          </div>
           <div class="bucket_full">
         <table cellpadding="0" cellspacing="0">
            <tr>
                <td width="150px"> <div class="button_right" style="width: 150px;">
                   <asp:LinkButton Width="150px" ID="serachLink" runat="server" Text="Detailed Search" 
                   ToolTip="click to start new search"
                   OnClick="BuidSearch_Click"></asp:LinkButton>
                </div></td>
                <td style="padding-left:5px">
                <asp:Label ID="Notes" runat="server"></asp:Label></td>
            </tr>
        </table>
        </div>     
         <div class="bucket_full">
                        
            <asp:GridView ID="IndicatorGrid"
                 runat="server"
                 OnRowDataBound="IndicatorGrid_RowDataBound"
                 DataKeyNames="SectionID"
                 AutoGenerateColumns="false"
                 BorderWidth="0"
                 Width="100%">
            <Columns>            
                <asp:TemplateField>
                 <ItemTemplate>                                                    
                        <asp:GridView ID="SectionRepeater" runat="server" 
                            BorderWidth="0" 
                            CssClass="browse_table"                            
                            HeaderStyle-HorizontalAlign="Right"  
                            OnRowDataBound="SectionRepeater_RowDataBound"                          
                            RowStyle-HorizontalAlign="Right"
                            AlternatingRowStyle-CssClass="fill"
                            Visible="true"
                            Width="100%"
                            AutoGenerateColumns="false">    
                            
                            <Columns>
                                <%--<asp:BoundField DataField="Indicators" HeaderText="Indicators" ItemStyle-Width="200"  />--%>
                                <asp:TemplateField ItemStyle-Width="230px">
                                <HeaderTemplate>
                                    <asp:Label ID="SectionHeading" runat="server"></asp:Label>
                                </HeaderTemplate>
                                    <ItemTemplate>
                                    <div class="lvl<%# Eval("IndentLevel") %>"><span style="float:left"><%# Eval("Indicators") %></span>
                                        <a style="color: Red;float:left; text-decoration: none;display: <%# DisplayAnchor(Eval("Definition").ToString()) %>" 
                                            href="../TaxonomyDefinition.aspx?taxonomyid=<%# Eval("indicatorid") %>" 
                                            onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                            &nbsp;<img alt="View Definition" border="0" src="Assets/Images/help2.gif"  />
                                        </a> 
                                    </div>                                                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Units" HeaderText="Units" ItemStyle-Width="110" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:TemplateField ItemStyle-Width="25px">
                                    <ItemTemplate>
                                        <div style="padding: 0px;  text-align: center;">
                                            <a style="color: Red; text-decoration: none;display:<%# DisplayAnchor(Eval("rowid").ToString()) %>" 
                                                href="TrendImage.aspx?rowid=<%# Eval("rowid") %>&type=countryoverview"                                                 
                                                onclick="return hs.htmlExpand(this, { contentId: 'PopupChart', objectType: 'iframe', preserveContent: false} )">
                                        <%--        <img alt="" border="0" src="../assets/images/chart_icon.gif" />--%>
                                                <asp:Image ID="IndicatorChart" runat="server"  alt="View Trend Chart"
                                                    ImageUrl='<%# ResolveUrl("~/imagegenerator.aspx?rowid="+Eval("itemid")+"&countryid="+Eval("countryID")+"&index="+(Container.DataItemIndex+1))%>' 
                                                    width="40px" Height="14px"/>

                                            </a>                 
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Rounding" HeaderText="Rounding" ItemStyle-Width="100" Visible="false" />
                                <asp:TemplateField HeaderText="Rank" ItemStyle-Width="30">
                                    <ItemTemplate>
                                        <a  title="view all country ranks"
                                            href="TrendImage.aspx?rowid=<%# Eval("itemID") %>&year=<%# GetQuartileYear() %>&countryid=<%# Eval("countryID") %>&IsRank=true"                                 
                                            onclick="return hs.htmlExpand(this, { contentId: 'QuartileStatus', objectType: 'iframe', preserveContent: false} )">
                                            <%#Eval("Rank")%>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div style="float:right;padding-bottom:5px">
                             <asp:LinkButton ID="MoreLink" runat="server" Text="More..." 
                                Visible='<%# Eval("ShowMoreLink") %>'
                                CommandArgument='<%# Eval("sectionID") %>'        
                                OnCommand="MoreLink_Click" 
                                ToolTip="click to go search">
                            </asp:LinkButton>
                        </div>
                    <br />
                </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
          </div>  
        </div>
        <asp:HiddenField ID="hdnCountry" runat="server" />
        <asp:HiddenField ID="hdnCountryName" runat="server" />
        <asp:HiddenField ID="hdnCountryMapName" runat="server" />
        <asp:HiddenField ID="hdnZoomLevel" runat="server" Value="2" />
       
    </div>
    <div class="highslide-html-content" id="PopupChart" style="width:360px;height:300px">
        <div class="highslide-header">
            <ul>
                <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                <li class="highslide-close">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                        Text="Close"></asp:LinkButton>
                </li>
            </ul>
            <h1>Trend Chart</h1>
        </div>
        <div class="highslide-body">               
        </div>
      
    </div>
    <div class="highslide-html-content" id="QuartileStatus" style="width:280px;height:400px">
        <div class="highslide-header">
            <ul>
                <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                <li class="highslide-close">
                    <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hs.close(this);return false;"
                        Text="Close"></asp:LinkButton>
                </li>
            </ul>
            <h1>Countries</h1>
        </div>
        <div class="highslide-body">               
        </div>
      &nbsp;
    </div>
    <div class="highslide-html-content" id="TaxonomyDefinition" style="width:280px;height:200px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Taxonomy Definition</h1>
    </div>
    <div class="highslide-body" style="padding:0px;margin:0px">
    </div>
</div>
</asp:Content>
