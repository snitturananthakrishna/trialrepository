using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Logistics.Library;
using Datamonitor.PremiumTools.Logistics.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class GroupResults : AuthenticationBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !ResultsGrid.CausedCallback)
            {
                //Get the data and bind to grid.
                GetDataToBindGrid();
                ResultsGrid.DataBind();

            }
        }


        /// <summary>
        /// Gets the data to bind.
        /// </summary>        
        private void GetDataToBindGrid()
        {
            //Get selection criteria   
            DataSet TaxonomyTypes = LEGlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                string SelectionsXML = string.Empty;
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        List<int> keys = new List<int>(selectedIDsList.Keys);
                        //convert int array to string array
                        string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });

                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                            taxonomyRow["ID"].ToString(),
                            string.Join(",", SelectedIDs));
                    }
                }

                //Get the years filter data from session
                int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                string TaxonomySelectedValue = StartYear.ToString();
                while (EndYear > StartYear)
                {
                    StartYear++;
                    TaxonomySelectedValue += "," + StartYear;
                }

                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                Dictionary<string, string> removedColumns;
                //Get results
                DataSet ResultsSet = SqlDataService.GetGridData(SelectionsXML,
                    TaxonomySelectedValue,
                    false, 
                    out removedColumns);
                //Bind data to grid
                BindDataToGrid(ResultsSet);               
            }
        }

        /// <summary>
        /// Binds the data to grid.
        /// </summary>
        /// <param name="ResultsSet">The results set.</param>
        private void BindDataToGrid(DataSet ResultsSet)
        {
            //Load data into grid
            string columnsToIgnore = ConfigurationManager.AppSettings.Get("ColumnsToIgnore");
            string columnsToHide = ConfigurationManager.AppSettings.Get("ColumnsToHide");
            if (ResultsSet.Tables.Count > 0)
            {
                //ResultsGrid.Items.Clear();
                //ResultsGrid.Levels[0].Columns.Clear();
                //foreach (DataColumn column in ResultsSet.Tables[0].Columns)
                //{
                //    //Add columns
                //    if (!columnsToIgnore.Contains("|" + column.ColumnName.ToLower() + "|"))
                //    {
                //        GridColumn col1 = new GridColumn();

                //        col1.DataField = column.ColumnName;
                //        col1.Width = column.ColumnName.Length * 10 - 10;
                //        col1.HeadingText = column.ColumnName;
                //        //col1.HeadingCellCssClass = "HeadingCell_Freeze";
                //        col1.Visible = !columnsToHide.Contains(("|" + column.ColumnName.ToLower() + "|"));
                //        col1.ContextMenuId = "Menu1";
                //        if (column.ColumnName.StartsWith("Y19") || column.ColumnName.StartsWith("Y20"))
                //        {
                //            col1.HeadingText = column.ColumnName.Replace("Y", "");
                //        }
                //        ResultsGrid.Levels[0].Columns.Add(col1);
                //    }
                //}
                ResultsGrid.DataSource = ResultsSet;
                //ResultsGrid.DataBind();
            }
        }


        public void OnNeedRebind(object sender, EventArgs oArgs)
        {
            System.Threading.Thread.Sleep(200);
            ResultsGrid.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs oArgs)
        {
            GetDataToBindGrid();
        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            ResultsGrid.CurrentPageIndex = oArgs.NewIndex;
        }

        public void OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            ResultsGrid.Filter = oArgs.FilterExpression;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            ResultsGrid.Sort = oArgs.SortExpression;
        }

        public void OnGroup(object sender, ComponentArt.Web.UI.GridGroupCommandEventArgs oArgs)
        {
            ResultsGrid.GroupBy = oArgs.GroupExpression;
        }

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ResultsGrid.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            ResultsGrid.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            ResultsGrid.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            ResultsGrid.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            ResultsGrid.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(OnFilter);
            ResultsGrid.GroupCommand += new ComponentArt.Web.UI.Grid.GroupCommandEventHandler(OnGroup);
        }


    }
}
