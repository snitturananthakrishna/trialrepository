using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using ComponentArt.Web.UI;
using System.Collections.Generic;
using System.Xml;
using Aspose.Cells;
using Aspose.Words;

namespace Datamonitor.PremiumTools.Logistics.Results
{
    public partial class GetReportSectionsData :System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDropdown();                                
            }
        }     

        public void LoadDropdown()
        {
           DataSet TaxonomyList = SqlDataService.GetCountryDataForReports();
           dropdownCountry.DataSource = TaxonomyList;
           dropdownCountry.DataTextField = TaxonomyList.Tables[0].Columns["country"].ToString();
           dropdownCountry.DataValueField = TaxonomyList.Tables[0].Columns["countryid"].ToString();
           dropdownCountry.DataBind();          
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GenerateReport_Click(object sender, EventArgs e)
        {
            DataSet GetPredefinedReportSections = SqlDataService.GetPredefinedReportSections();
            Workbook excel = new Workbook();
            string excelTemplatePath = Server.MapPath("~/Config/GTA/ExtractSectionsDataExcel.xls");            
            excel.Open(excelTemplatePath);
            int countryID = Convert.ToInt32(dropdownCountry.SelectedItem.Value);
            string country = dropdownCountry.SelectedItem.Text;
            //To put country name in Title page.
            excel.Worksheets[0].Cells[3, 2].PutValue("Telecom industry outlook: " + country);
            string Date = DateTime.Now.ToLongDateString();           
            excel.Worksheets[0].Cells[4, 2].PutValue(Date.Substring(8, Date.Length));
           // for(int i = 0;i < GetPredefinedReportSections.Tables[0].Rows.Count; i++)
            for (int i = 0; i < GetPredefinedReportSections.Tables[0].Rows.Count; i++)
            {
                DataSet GetPredefinedReportSectionsData = SqlDataService.getPredefinedReportSectionsData(Convert.ToInt32(GetPredefinedReportSections.Tables[0].Rows[i]["SectionID"].ToString()), countryID);
                excel = ExtractionsHelper.ExportToExcel(
                             excel,
                             GetPredefinedReportSectionsData,
                             Convert.ToInt32(GetPredefinedReportSections.Tables[0].Rows[i]["RowNumber"].ToString()),
                             Convert.ToInt32(GetPredefinedReportSections.Tables[0].Rows[i]["ColumnNumber"].ToString()),
                             country,
                             i);                   
            }              
            excel.Save("GTA_CountryReport_" + country +".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);            
        }       
    }
}
