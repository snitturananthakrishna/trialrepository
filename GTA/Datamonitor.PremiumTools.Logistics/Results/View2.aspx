<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/ResultsMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="View2.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.View2" 
        EnableEventValidation="false"
        EnableViewStateMac="false"
       ViewStateEncryptionMode="never"
       ValidateRequest="false" %>
        
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
        
<asp:Content ID="Content1" ContentPlaceHolderID="ResultsMasterHolder" runat="server">  
<link href="../assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../assets/Scripts/NumberFormat154.js"></script>
<style type="text/css">

.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.ResultsPanel
{
    float:left;
    
}

</style>

<style type="text/css">
.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.contentCss
{
  border-left:0px solid #C8C2A8;
  border-right:0px solid #C8C2A8;
  background-color:#ECECD9;
     width:205px;
     text-align:center;
     border-top:0px;
}

.iFrameCss
{
    overflow:auto;
    width:208px;
    height:302px;
    padding:0px;
    margins:0px;
}

</style>

 <script type="text/javascript">
    function formatN(value)
    {
        var decimalPlaces="<%= NoOfDecimalsToGridValues %>";
    
        //return value;
        if(value==null)
        return "";
        //for growth rows, data will come in the format xx.xx% - so format the number part
        var postfix='';
        if(String(value).indexOf('%')!=-1)
        {
            value=String(value).substring(0,value.length-1);
            postfix='%';
        }
        if(value != "0" && value == "")
    	{
       	 return "";
    	}
        var num = new NumberFormat();
        num.setInputDecimal('.');
        num.setNumber(value); // obj.value is '-1000.247'
        
        num.setCurrency(false);
        num.setNegativeFormat(num.LEFT_DASH);
        num.setNegativeRed(false);
        num.setSeparators(true, ',', ',');       
        
        if(Number(value) >= 100)
        {
            num.setPlaces(0, false);    
        }
        else 
        {
            num.setPlaces(decimalPlaces, false);
        }
        return num.toFormatted()+postfix;

    }
    function AddPercentage(value)
    {
        if(value==null || value == "")
    	{
       	    return "";
    	}
    	return value + '%';
    }
    function GetCagrLabelCss(value)
    {
        if(value>0)
    	{
       	    return "green";
    	}
    	else if(value<0)
    	{
    	    return "red";
    	}   
    	else
    	{
    	    return "black";
    	} 	
    }
    function getValueType(dataitem, fieldName)
    {
        var fName;
        fName = fieldName.replace("Y", "");
        var ffy = dataitem.getMemberAt("FFY").get_value();
        if(ffy == "")
        {
            return "Actual";
        }
        else if (fName == ffy)
        {
            return "FFY";
        }
        else if (fName > ffy)
        {
            return "Forecast";
        }
        return "Actual";
    }
    function getCellCssClass(dataitem, fieldName)
    {        
        var taxonomyLevel = dataitem.getMemberAt("taxonomyLevel").get_value();        
        return "L"+taxonomyLevel;
    }
    function getColumnNumber(columnName)
    {
        var numColumns = ResultsGrid.get_table().get_columns().length;
        //alert(numColumns);
        for (i=0;i<numColumns;i++)
        {
            var col = ResultsGrid.get_table().get_columns()[i]
            if (col.get_dataField() == columnName) 
                return i;           
        }
    }

    
    var sortFlag = true; 

    function ResultsGrid_onSortChange(sender, e)
    {
        //alert("sort Change : " + flag);
        //	    sortDescending = e.get_descending();
        //	    var columnIndex = 0;
        //	    for (var i=0;i<sender.get_table().get_columns().length;i++)
        //	    {
        //		    if (sender.get_table().get_columns()[i].get_dataField() == e.get_column().get_dataField())
        //		    {
        //			    sortColumnIndex = i;
        //			    break;
        //		    }		    
        //	    }
	    if(!sortFlag)
	        e.set_cancel(true); 
	        
	    sortFlag=true;
    }
    
    
    
    function ShowTaxonomyFilter(type)
    {        
        alert(type);
        sortFlag=false;         
         
    }
    
    function CheckChange(chkbox,colName)
    {    
        //ShowHideGridColumn(chkbox.value, chkbox.checked);        
        var hiddenColumns = document.getElementById('<%=HiddenColumns.ClientID%>');
        if(chkbox.checked)
        {            
            hiddenColumns.value = hiddenColumns.value.replace("|"+colName + "|", "|"); 
        }
        else
        {
            hiddenColumns.value = hiddenColumns.value + colName + '|';
        }
        
        ChangeDisplayOptionsChangedStatus();         
    }

    function ShowHideGridColumn(index, show)
    {
        var grd = <%=ResultsGrid.ClientObjectId %>;
        var col = grd.get_table().get_columns()[index];
        col.set_visible(show); 
        grd.render();  
    }
    function ResultsGrid_onLoad(sender, e)
    {       
        var columntoHide = document.getElementById('<%=ColumnToHide.ClientID%>').value;        
        if(columntoHide.length>0)
        {
            var grd = <%=ResultsGrid.ClientObjectId %>;       
            
            var columnstoHide = columntoHide.split(",");        
            var j = 0;        
            ResultsGrid.beginUpdate();
            for(j = 0; j < columnstoHide.length; j++)
            {            
                var col = grd.get_table().get_columns()[columnstoHide[j]];
                col.set_visible(false);         
            }         
            ResultsGrid.endUpdate();
        }
    }
    function ResultsGrid_onColumnResize(sender, eventArgs)
    {
        ResultsGrid.beginUpdate();
        
        
        setTimeout("ResultsGrid_onLoad(null,null)",100);    
        ResultsGrid.endUpdate();
    }
    

    function RefreshPage()
    {
        if (typeof this.__EVENTTARGET != "undefined")
        {
            __EVENTTARGET.value = "";
        }
        if (typeof this.__EVENTARGUMENT != "undefined")
        {
            __EVENTARGUMENT.value = "";
        }

        window.location.reload();
    }
   
    $(document).ready(function() { 
    //alert("inside ready");  
});


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}

 function ChangeDisplayOptionsChangedStatus() 
 {
    var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
    hdnStatus.value="true";     
 }
 function CheckDisplayOptionsChanged()
 {
    var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
    return hdnStatus.value==""?false:true;
 }
 
 
 function getUniqueID()
 {
    var newDate = new Date;
    return newDate.getTime();  
 }
 
 function UnitSelections_onChange(dropDown)
{    
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropDown.value;
    var baseUnitID=selection.split("~")[0];
    var unitsConversions=document.getElementById('<%=hdnUnitsConversions.ClientID %>').value;    
    var unitsArray=unitsConversions.split("|");    
    
    if(unitsArray)
    {
        for(j = 0; j < unitsArray.length; j++)
        {            
            if(unitsArray[j].indexOf(baseUnitID)==0)
            {
                unitsConversions=unitsConversions.replace(unitsArray[j]+"|","");
                break;
            }
        }
    }
    unitsConversions=unitsConversions+selection+"|";
    document.getElementById('<%=hdnUnitsConversions.ClientID %>').value=unitsConversions;
    
}

 function getUnitsConversions()
 {
    var unitsConversions=document.getElementById('<%=hdnUnitsConversions.ClientID %>').value;
    var unitsArray=unitsConversions.split("|");    
    var result="";
    if(unitsArray)
    {
        for(j = 0; j < unitsArray.length; j++)
        {            
            if(unitsArray[j].indexOf("~")!=-1)
            {
                result=result+unitsArray[j].split("~")[1]+",";            
            }
        }
    }
    return result;
 }
 
 function Year_onChange(type,dropdown)
{
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropdown.value;
    
    if(type=="startyear")
    {
        document.getElementById('<%=hdnStartYear.ClientID %>').value =selection;   
    }
    else if(type=="endyear")
    {
        document.getElementById('<%=hdnEndYear.ClientID %>').value =selection;
    }    
}
 function getStartYear()
 {
    return  document.getElementById('<%=hdnStartYear.ClientID %>').value;
 }
 function getEndYear()
 {
    return  document.getElementById('<%=hdnEndYear.ClientID %>').value;
 }
 
 function TryRenderGrid()
 {
//    alert(IsIE8Browser());
    if(IsIE8Browser())
    {
        ResultsGrid.render();
    }
 }
 
 function IsIE8Browser() 
 {
     var rv = -1;
     var ua = navigator.userAgent;    
     var re = new RegExp("Trident\/([0-9]{1,}[\.0-9]{0,})");    
     if (re.exec(ua) != null)
      {        
        rv = parseFloat(RegExp.$1);    
      }    
    return (rv == 4);
}

 function currency_onChange(dropdown)
{   
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropdown.value;
    document.getElementById('<%=hdnConversionID.ClientID %>').value =selection;   
}
function getTargetCurrentID()
{
    return document.getElementById('<%=hdnConversionID.ClientID %>').value ;
}
function AbsoluteOrIndex(type)
{
    ChangeDisplayOptionsChangedStatus(); 
    
    //document.getElementById('<%=FixedConversion.ClientID %>').disabled=true;
    //document.getElementById('<%=YearOnYearConversion.ClientID %>').disabled=true;
    
    if(type=="index")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-1";   
    }
    else if(type=="percapita")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-2";   
    }
    else if(type=="growths")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-3";   
    }
    else if(type=="fixed")
    {
        document.getElementById('<%=hdnCurrencyConversionType.ClientID %>').value ="false";   
        //document.getElementById('<%=FixedConversion.ClientID %>').disabled=false;
        //document.getElementById('<%=YearOnYearConversion.ClientID %>').disabled=false;
    }
    else if(type=="yearonyear")
    {
        document.getElementById('<%=hdnCurrencyConversionType.ClientID %>').value ="true";   
        //document.getElementById('<%=FixedConversion.ClientID %>').disabled=false;
        //document.getElementById('<%=YearOnYearConversion.ClientID %>').disabled=false;
    }
    else 
    {
        var dropdown=document.getElementById('<%=currency.ClientID %>');
        if(dropdown)
        {
            document.getElementById('<%=hdnConversionID.ClientID %>').value =dropdown.value;  
        }
        else
        {
            document.getElementById('<%=hdnConversionID.ClientID %>').value ="0";  
        }
        //document.getElementById('<%=FixedConversion.ClientID %>').disabled=false;
        //document.getElementById('<%=YearOnYearConversion.ClientID %>').disabled=false;
    }
}

function getTaxonomyFilterPage()
{
    return "<%= getTaxonomyFilterPage()%>";
}

function getAnchorVisibility(val)
 {
     if(val.length>0)
     {     
        return "block";
     }
     else
     {
        return "none";
     }
 }
 function HasAdditionalDetails(val)
 {
     if(val)
     {     
        return "block";
     }
     else
     {
        return "none";
     }
 }
 </script>
 




  
    <div id="display_options">
        <div style="margin-top:2px;">
            <div class="display_options">
                <h3>                        
                    Display options 
                </h3>
            </div>
            <div>
              <table width="700">
                    <tr>
                        <td valign="top">  <b>Columns</b>
                            <div id="ColumnFilterDiv1" runat="server"></div>
                        </td>                        
                        <td valign="top">  <b>Years</b>
                            <table>
                                <tr>
                                    <td>Start Year&nbsp; </td>
                                    <td><asp:DropDownList ID="StartYear" runat="server"  Font-Size="11px" width="60px"/></td>
                                </tr>
                                <tr>
                                    <td>End Year&nbsp; </td>
                                    <td><asp:DropDownList ID="EndYear" runat="server"  Font-Size="11px" width="60px"/></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left:50px;" runat="server" id="DataFormats">
                            <b>Show data in</b> 
                            <table><tr>
                            <td><asp:RadioButton ID="ShowAbsoluteValues" runat="server" Text="Absolute" onClick="AbsoluteOrIndex('absolute');" GroupName="Currency"/></td>
                            <td><asp:DropDownList ID="currency" Width="105px" runat="server" Font-Size="11px" /> </td></tr>
                            <tr style="display:none"><td></td><td><asp:RadioButton ID="FixedConversion" runat="server" Text="Fixed" onClick="AbsoluteOrIndex('fixed');" GroupName="ConversionType"/>
                            <asp:RadioButton ID="YearOnYearConversion" runat="server" Text="Year on Year" onClick="AbsoluteOrIndex('yearonyear');" GroupName="ConversionType"/></td></tr>
                            <tr><td colspan="2">
                            <asp:RadioButton ID="ShowIndexTo100" runat="server" Text="Index To 100" onClick="AbsoluteOrIndex('index');" GroupName="Currency"/>                            
                            </td></tr>
                            <tr><td colspan="2"><asp:RadioButton ID="ShowPerCapita" runat="server" Text="Per capita" onClick="AbsoluteOrIndex('percapita');" GroupName="Currency"/>
                            </td></tr>
                            <tr><td colspan="2">
                            <asp:RadioButton ID="ShowGrowths" runat="server" Text="Growth Rate (%)" onClick="AbsoluteOrIndex('growths');" GroupName="Currency"/>
                            </td></tr>
                            </table>
                       </td>
                       <td valign="top" style="padding-left:20px;">
                            <asp:Repeater ID="rptUnitSelections" runat="server"
                                OnItemDataBound="rptUnitSelections_ItemDataBound">
                                <HeaderTemplate>                               
                                <table cellpadding="2">
                                <tr>
                                    <td colspan="2"><b>Units Conversion</b></td>                                    
                                </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="BaseUnitsID" runat="server"
                                                Text='<%#Eval("baseunitsid") %>'
                                                Visible="false"></asp:Label>
                                           <%#Eval("basename") %> &nbsp; 
                                        </td>
                                        <td><asp:DropDownList ID="ddlOptions" runat="server"  
                                                Font-Size="11px"
                                                width="140px"/>
                                        </td>
                                    </tr>                                   
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table><br />
                                </FooterTemplate>        
                            </asp:Repeater>
                       </td>
                    </tr>
                </table>
            </div>
        </div>
           <div id="display_options_buttons">
                <a href="javascript:toggleLayer('display_options');TryRenderGrid();" title="click to cancel">Cancel</a>
                <asp:LinkButton ID="Update" runat="server" CssClass="save_button" OnClientClick="JavaScript:toggleLayer('display_options');return CheckDisplayOptionsChanged();"
                    OnClick="Update_Click" ToolTip="click to update the results">Update</asp:LinkButton>  
             </div>
                    </div>
    <div class="subtitle">
        <div id="display_options_tab" style="margin-top:1px;">
                <a href="javascript:toggleLayer('display_options');TryRenderGrid();" title="click to change the display options">
                    <div>
                        Display options
                    </div>
                </a>
         </div>
         <div id="extractOpts" style="float:left;display: block;">
            <ul class="jumplinks">
                <li class="last"><span id="ctl00_ContentPlaceHolder1_lblExtract" class="lblExport">Extract data to </span></li>
                <li class="excel_icon"><asp:LinkButton ID="lnkExcel" Text="Excel" runat="server" OnClick="Excel_Click" ToolTip="extract results to excel" ></asp:LinkButton> </li>
            </ul>
        </div>
    </div>
        
        <div style="float:left;margin-top:5px;width:100%;position:relative">
 
            <asp:Repeater ID="RemovedTaxonomies" runat="server">
            <ItemTemplate>
               <span class="RemovedTaxonomyHeading"> <%#Eval("key") %> </span>
                    <a style="color:White;text-decoration: none;text-align: center;visibility:<%#Eval("isvisible") %>"    
                        href="<%=  getTaxonomyFilterPage()%>?id=<%#Eval("key") %>&uid=<%=DateTime.Now.ToString("HHmmss") %>" 
                        onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: false} )">
                       <img alt="Filter <%#Eval("key") %>" 
                            border="0" 
                            src="../assets/images/filter.gif"/>  
                    </a>          
                : <span class="RemovedTaxonomyData"> <%#Eval("value").ToString().Length > 0 ? Eval("value").ToString() : "[Blanks]" %></span><br />
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>        
            </asp:Repeater>
                
        
        <asp:Label ID="GridStatusMessage" runat="server" ForeColor="red"></asp:Label>
        
     <asp:Panel ID="ResultsPanel" runat="server" Height="435px" Width="99%" CssClass="ResultsPanel" >     
       <ComponentArt:Grid id="ResultsGrid"                                    
        CssClass="grid"
        RunningMode="client"                                                                             
        ShowHeader="false"    
        ShowFooter="false"
        AllowColumnResizing="true"
        AllowEditing="false"                                            
        HeaderCssClass="GridHeader"               
        FooterCssClass="GridFooter"                      
        SliderHeight="20"
        SliderWidth="150"
        SliderGripWidth="9"   
        SliderPopupOffsetX="20"                                                             
        IndentCellWidth="22"
        IndentCellCssClass="HeadingCell_Freeze"     
        LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
        LoadingPanelPosition="MiddleCenter"
        LoadingPanelFadeDuration="10"
        LoadingPanelFadeMaximumOpacity="60"                                                                
        ScrollBar="auto"
        ImagesBaseUrl="../assets/images/"
        PagerStyle="Numbered"
        PagerInfoPosition="BottomLeft"
        PagerPosition="BottomRight"
        PreExpandOnGroup="true"                                			        
        ScrollTopBottomImagesEnabled="true"
        ScrollTopBottomImageHeight="2"
        ScrollTopBottomImageWidth="16"
        ScrollImagesFolderUrl="../assets/images/scroller/"
        ScrollButtonWidth="16"
        ScrollButtonHeight="17"
        ScrollBarCssClass="ScrollBar"
        ManualPaging="false"
        ScrollGripCssClass="ScrollGrip"
        ScrollBarWidth="16"     

        Height="410"    
        Width="910" runat="server" AllowHorizontalScrolling="True" EnableViewState="false" >
        <Levels>
          <ComponentArt:GridLevel                                      
                AllowReordering="false"
                AllowGrouping="false"      
                AllowSorting="false"                                                  
                HeadingCellCssClass="HeadingCell"                                                                                                
                HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText"                                                    
                GroupHeadingCssClass="HeadingCell_Freeze"
                DataCellCssClass="DataCell"
                AlternatingRowCssClass="RowAlt"
                RowCssClass="Row"                                          
                SelectedRowCssClass="SelRow"   
                SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" >                                                    
            <Columns>
                     
            </Columns>
          </ComponentArt:GridLevel>
          
        </Levels>
      <ClientEvents>
            <SortChange EventHandler="ResultsGrid_onSortChange" />          
            <Load EventHandler="ResultsGrid_onLoad" />
            <ColumnResize EventHandler="ResultsGrid_onColumnResize" />                 
      </ClientEvents>

        <ClientTemplates>                     
          <ComponentArt:ClientTemplate Id="CommonDataTemplate">                                                                           
            <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## DataItem.GetCurrentMember().get_value() ##</div>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="FormattedDataTemplate">                                                                           
            <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## formatN(DataItem.GetCurrentMember().get_value()) ##</div>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="TooltipTemplate">                                                                           
            <div title="## DataItem.GetCurrentMember().get_value() ##"> ## DataItem.GetCurrentMember().get_value().replace(/#%cLt#%/g, '<');  ## </div>           
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TooltipTemplateWithDefinition">                                                                           
            <div style="float:left" title="## DataItem.GetCurrentMember().get_value() ##"> 
                <table cellpadding="0" cellspacing="0"><tr><td nowrap="nowrap">
                ## DataItem.GetCurrentMember().get_value().replace(/#%cLt#%/g, '<'); ##
                </td><td>
                
                 <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('Definition').get_value())##" 
                    href="../TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetMember('taxonomyid').Text ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                    &nbsp;<img alt="View Definition" border="0" src="Assets/Images/help2.gif"  />
                </a> 
                </td></tr></table>
            </div> 

          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="MeasureTooltipTemplateWithDefinition">                                                                           
            <div title="## DataItem.GetCurrentMember().get_value() ##"> 
                <table cellpadding="0" cellspacing="0"><tr><td nowrap="nowrap">
                ## DataItem.GetCurrentMember().get_value().replace(/#%cLt#%/g, '<'); ##
                </td><td>
                
                 <a style="color: Red; text-decoration: none;display:none"
                    href="../TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetMember('measureid').Text ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'ajax'} )">
                    &nbsp;<img alt="View Definition" border="0" src="Assets/Images/help2.gif"  />
                </a> 
                </td></tr></table>
            </div>
          </ComponentArt:ClientTemplate>
           <ComponentArt:ClientTemplate Id="CAGRTemplate">                                                                           
            <div style="color:##GetCagrLabelCss(DataItem.GetCurrentMember().get_value())##"> ## AddPercentage(DataItem.GetCurrentMember().get_value()) ##</div>
          </ComponentArt:ClientTemplate>                 
          <ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
              <table height="410" width="940" bgcolor="#f6f6f6" style="margin-left:0px;margin-top:0px;padding-top:0px"><tr><td valign="center">
                  <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td style="font-size:11pt;font-family:Arial;font-weight:bold;color:#0000a0;">Loading...&nbsp;</td>
                    <td><img src="../assets/images/spinner.gif" border="0"></td>
                  </tr>
                  </table>
          </td></tr></table>

          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate ID="TrendChartTemplate">
          <div style="padding: 0px; font-weight: bold; text-align: center;">
                <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('rowID').Text) ##" 
                    href="TrendImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&conversionid=## DataItem.GetMember('ConversionID').Text ##&startyear=## getStartYear() ##&endyear=## getEndYear() ##&targetCurrencyid=## getTargetCurrentID() ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'PopupChart', objectType: 'iframe'} )">
                    <img alt="View Trend Chart" border="0" src="../assets/images/chart_icon.gif" />
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
           <ComponentArt:ClientTemplate ID="DataSourceClientTemplate">            
            <div style="text-align:center; padding-left:2px;">
                <a style="color:White;text-decoration: none;text-align: center;" 
                    href="DataSource.aspx?rowid=## DataItem.GetMember('rowID').Text ##"
                    onclick="return hs.htmlExpand(this, { contentId: 'DataSource', objectType: 'ajax'} )">
                    <img border="0" src="../assets/images/icon-info.gif" alt="View Data Source" />
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate ID="ColumnFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;overflow:hidden;">
            <table><tr><td>
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a> </td><td>
                <a style="color:White;text-decoration: none;text-align: center;" 
                    href="## getTaxonomyFilterPage()##?id=## DataItem.DataField ##&uid=## getUniqueID() ##" 
                    onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: false} )">
                   <img alt="Filter ## DataItem.DataField ##" 
                        border="0" 
                        src="../assets/images/filter.gif"/>  
                </a>          </td></tr></table>       
                <%--<asp:ImageButton ID="filterData" runat="server"
                    ImageUrl="../assets/images/filter.gif"
                    OnClientClick="return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'ajax'} )"
                    PostBackUrl="DataSource.aspx" />--%>
            </div>
          </ComponentArt:ClientTemplate>
           <ComponentArt:ClientTemplate ID="ColumnNonFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;">
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
       <%-- <ComponentArt:ClientTemplate ID="ColumnFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;">
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a> <img alt="pop" border="0" src="../assets/images/filter.gif" onclick="mypopup()" />                
            </div>
          </ComponentArt:ClientTemplate>--%>
        </ClientTemplates>
      </ComponentArt:Grid>
      </asp:Panel>
      
 <div style="width:100%;display:block;padding-top:5px;" align="right">Note: Forecast data in <i>italics</i><br />CAGR is calculated for the selected range of years
 </div>
 <br />
</div>
       <div class="highslide-html-content" id="PopupChart" style="width:350px;height:325px">
                <div class="highslide-header">
                    <ul>
                        <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                        <li class="highslide-close">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                                Text="Close"></asp:LinkButton>
                        </li>
                    </ul>
                    <h1>Trend Chart</h1>
                </div>
                <div class="highslide-body">               
                </div>
            </div>
      
        <div class="highslide-html-content" id="DataSource" style="width:280px;height:200px">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                    <li class="highslide-close">
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hs.close(this);return false;"
                            Text="Close"></asp:LinkButton>
                    </li>
                </ul>
                <h1>Data Source</h1>
            </div>
            <div class="highslide-body">
            </div>
        </div>
        <div class="highslide-html-content" id="ColumnFilterSelections" style="width:460px;height:470px;padding-bottom:35px">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                    <li class="highslide-close">
                        <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hs.close(this);return false;"
                            Text="Close"></asp:LinkButton>
                    </li>
                </ul>
                <h1>Filter Selections</h1>
            </div>
            <div class="highslide-body" style="overflow:hidden;margin-left:10px" >                 
            </div>
            <div class="button_right" style="width:100px;margin-top:0px;margin-right:50px">
                <asp:LinkButton ID="UpdateResults" runat="server" OnClientClick="hs.close(this);RefreshPage();return false;"
                    Text="Update" Width="100px"></asp:LinkButton> 
            </div>     
        </div>
       
<div class="highslide-html-content" id="TaxonomyDefinition" style="width:280px;height:200px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Taxonomy Definition</h1>
    </div>
    <div class="highslide-body" style="padding:0px;margin:0px">
    </div>
</div>
 

<div id="Loadingimg" style="visibility: hidden;">
        <div style="left: 0px; top: 0px" id="progressBackgroundFilter_New" class="progressBackgroundFilter_New">
        </div>
        <div id="processMessage_New" class="processMessage_New">
            <b>Loading ...</b>
            <img alt="" id="spin" src="../Assets/Images/spinner.gif" />
        </div>
    </div>    
       
<asp:HiddenField ID="ColumnToHide" runat="server" Value="" />
<asp:HiddenField ID="hdnsavedsearch" runat="server" /> 
<asp:HiddenField ID="isDisplayOptionsChanged" runat="server" />
<asp:HiddenField ID="HiddenColumns" runat="server" />
<asp:HiddenField ID="hdnUnitsConversions" runat="server" value=""/> 
<asp:HiddenField ID="hdnStartYear" runat="server" />
<asp:HiddenField ID="hdnEndYear" runat="server" />
<asp:HiddenField ID="hdnConversionID" runat="server" />
<asp:HiddenField ID="hdnCurrencyConversionType" runat="server" />
</asp:Content>


