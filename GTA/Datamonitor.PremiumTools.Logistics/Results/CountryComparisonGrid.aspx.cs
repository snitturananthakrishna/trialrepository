using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using CArt = ComponentArt.Web.UI;
using Aspose.Cells;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class CountryComparisonGrid : AuthenticationBasePage
    {
        // EVENT OVERRIDES
        protected void Page_Load(object sender, EventArgs e)
        {
            //Set the source for the view options control
            ((ViewOptions)Master.FindControl("ViewOptionsControl")).Source = "countrycomparison";

            ((MasterPages.ResultsMaster)Master).AllowStatusBar = false;
            currency.Attributes.Add("onchange", "javascript:currency_onChange(this);");
            DisplayOptionsDiv.Visible = GlobalSettings.ShowDisplayOptionsInCountryComparison;
            
            if (!IsPostBack && !ResultsGrid.CausedCallback)
            {
                //First time page loading settings
                hdnConversionID.Value = GlobalSettings.DefaultCurrencyID;
                Session["TargetCurrencyID"] = GlobalSettings.DefaultCurrencyID;

                //Bind currencies to the dropdown list forcurrency conversion
                BindCurrencies();

                //Store the currency dropdown selected value in hidden field for further events
                if (currency.SelectedValue.Length > 0)
                {
                    hdnConversionID.Value = currency.SelectedValue;
                }
            }
            if (IsPostBack)
            {
                //Store the conversion id in session
                Session["TargetCurrencyID"] = hdnConversionID.Value;
            }
            if (GlobalSettings.AllowAbsoluteValues)
            {
                currency.SelectedValue = hdnConversionID.Value;
            }

            //Bind country & years dropdown controls
            BindControls();

            //Get the grid results
            DataSet ResultsData = GetResults(false);

            //Bind the data to grid
            BindResultsGrid(ResultsData, CountryCombo.Text);            
        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            //Get the dataset 
            DataSet ResultsData = GetResults(false);

            //Add Usage Logging           
            LogUsage("Results - Country Comparison", "Grid1", "");

            //Add total column to resultset, by adding the values of country data columns
            //ResultsData = AddValuesToTotalColumn(ResultsData, CountryCombo.Text);
            //Bind to grid
            BindResultsGrid(ResultsData, CountryCombo.Text);
        }

        private void BindCurrencies()
        {   
            currency.Visible = false;
            DataFormats.Visible = false;

            //Check the config settings to decide whether we can show th currency dropdown or not
            if (GlobalSettings.AllowAbsoluteValues)
            {                
                currency.Visible = true;

                #region Bind Currency list to the dropdown list

                //get the default currency id from config
                int DefaultCurrencyID = Convert.ToInt32(GlobalSettings.DefaultCurrencyID);
                DataSet Currencies = null;

                //Get all possible Currency conversions for the defaultcurrency
                Currencies = SqlDataService.GetCurrencyTypes(DefaultCurrencyID);

                //Show the currency dropdown only when found atleast one target currency to convert 
                if (Currencies != null && Currencies.Tables.Count > 0 && Currencies.Tables[0].Rows.Count > 0)
                {
                    //Bind Currency list to the dropdown list
                    currency.DataTextField = "Name";
                    currency.DataValueField = "ToID";
                    currency.DataSource = Currencies;
                    currency.DataBind();

                    //Default selection
                    if (currency.Items.Count > 0)
                    {
                        currency.SelectedValue = currency.Items[0].Value;
                        try
                        {
                            currency.SelectedValue = DefaultCurrencyID.ToString();
                        }
                        catch { }
                        Session["TargetCurrencyID"] = currency.SelectedValue;
                    }
                    DataFormats.Visible = true;
                }
                else
                {
                    //Made the currency dropdown invisible as no target conversion found
                    currency.Visible = false;
                }

                #endregion

            }
              
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //Get the grid results to reload grid
            DataSet ResultsData = GetResults(false);

            //Bind the grid
            BindResultsGrid(ResultsData, CountryCombo.Text);

            //Clear the options change flag
            isDisplayOptionsChanged.Value = "";
        }

        // PRIVATE METHODS (initial population)   
        private void BindResultsGrid(DataSet ResultsData,string selectedCountries)
        {
            //Clear all existing columns
            ResultsGrid.Levels[0].Columns.Clear();

            //Add required columns to the grid
            CArt.GridColumn col1 = new CArt.GridColumn();

            #region Add indicator column
            
            col1.DataField="Indicator" ;
            col1.Width = 240;
            col1.HeadingText="Indicator";
            col1.HeadingCellCssClass="HeadingCell_Freeze";
            col1.DataCellCssClass="HeadingCell_Freeze";
            col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
            if (GlobalSettings.ShowTaxonomyDefinitionInResultsGrid)
            {
                col1.DataCellClientTemplateId = "CommonDataTemplateWithDefinition";
            }
            else
            {
                col1.DataCellClientTemplateId = "CommonDataTemplate";
            }
            ResultsGrid.Levels[0].Columns.Add(col1);

            #endregion

            //Check whether measure column be added to grid or not
            if (ConfigurationManager.AppSettings.Get("IncludeMeasureinCountryComparisonGrid").ToLower().Equals("true"))
            {
                #region Add Measure column
                
                col1 = new CArt.GridColumn();
                //add measure column
                col1.DataField = "Measure";
                col1.Width = 80;
                col1.HeadingText = "Measure";
                col1.HeadingCellCssClass = "HeadingCell_Freeze";
                col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
                col1.DataCellCssClass = "HeadingCell_Freeze";
                col1.DataCellClientTemplateId = "CommonDataTemplate";
                ResultsGrid.Levels[0].Columns.Add(col1);

                #endregion
            }

            #region Add units column
            
            col1 = new CArt.GridColumn();
            
            col1.DataField = "Units";
            col1.Width = 60;
            col1.HeadingText = "Units";
            col1.HeadingCellCssClass = "HeadingCell_Freeze";
            col1.DataCellCssClass = "HeadingCell_Freeze";
            col1.DataCellClientTemplateId = "CommonDataTemplate";
            ResultsGrid.Levels[0].Columns.Add(col1);

            #endregion

            #region Add chart column
            
            col1 = new CArt.GridColumn();
            col1.HeadingCellCssClass = "HeadingCell_Freeze";
            col1.Width = 45;
            col1.HeadingText = "Charts";
            col1.DataCellCssClass = "HeadingCell_Freeze";
            col1.DataCellClientTemplateId = "ChartTemplate";
            ResultsGrid.Levels[0].Columns.Add(col1);
            #endregion

            #region Add rowid column - hidden
            
            col1 = new CArt.GridColumn();
            col1.DataField = "rowID";
            col1.Visible = false;
            ResultsGrid.Levels[0].Columns.Add(col1);

            #endregion

            #region Add taxonomyid column - hidden
            
            col1 = new CArt.GridColumn();
            col1.DataField = "taxonomyid";
            col1.Visible = false;
            ResultsGrid.Levels[0].Columns.Add(col1);

            #endregion

            #region Add definition column - hidden
            
            col1 = new CArt.GridColumn();
            col1.DataField = "Definition";
            col1.Visible = false;
            ResultsGrid.Levels[0].Columns.Add(col1);

            #endregion

            #region Add data columns
            
            if (ResultsData != null && ResultsData.Tables.Count > 0)
            {
                foreach (DataColumn column in ResultsData.Tables[0].Columns)
                {
                    if (selectedCountries.Contains(column.ColumnName) || column.ColumnName.ToLower()=="total")
                    {                  
                        //All country columns along with total column will be data columns
                        col1 = new CArt.GridColumn();
                        col1.DataField = column.ColumnName;
                        col1.HeadingText = column.ColumnName;
                        col1.Align = ComponentArt.Web.UI.TextAlign.Right;
                        col1.DataCellClientTemplateId = GlobalSettings.FormatValuesInGrid ?
                                "FormattedDataTemplate" :
                                "CommonDataTemplate"; 
                        ResultsGrid.Levels[0].Columns.Add(col1);
                    }
                }
            }

            #endregion

            //Bind the data to the grid now
            ResultsGrid.DataSource = ResultsData;
        }

        private DataSet GetResults(bool forExcel)
        {
            DataSet ResultsData = null;
            if (CountryCombo.Text.Trim().Length > 0)
            {
                //Change the format of selected countries. ex: from A,B to [A],[B] - to query DB
                string SelectedCountries = "[" + CountryCombo.Text.Trim().Replace(";", "],[") + "]";
                
                //Get Selections xml
                string SelectionsXML = GetFilterCriteriaFromSession();

                //Get data 
                ResultsData = SqlDataService.GetCountryComparisonGridData(SelectionsXML,
                    SelectedCountries,
                    hdnYear.Value,
                    forExcel,
                    Convert.ToInt32(string.IsNullOrEmpty(Session["TargetCurrencyID"].ToString()) ?
                    "0" : Session["TargetCurrencyID"].ToString()));
            }
            return ResultsData;
        }

        private string GetSelectionsXML()
        {
            string SelectionsXML = string.Empty;

            //Get default taxonomy types list
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();

            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                    {
                        Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                        if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                        {
                            //Get all keys (ids) from dictionary
                            List<int> keys = new List<int>(selectedIDsList.Keys);

                            //convert int array to string array
                            string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                            TaxonomySelectedValue = string.Join(",", SelectedIDs);
                        }

                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            //Format the taxonomy node string
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                    }
                }

                //Prepare the final selections xml string
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        private void BindControls()
        {
            //Get the filter criteria from session
            string SelectionsXML = GetFilterCriteriaFromSession();

            //Get all the indicators and countries data from database for the provided selections xml
            DataSet IndicatorsAndCountries = SqlDataService.GetIndicatorsAndCountries(SelectionsXML);

            #region Binding Country dropdown
            
            //Loop through all the rows and add them as country tree nodes
            foreach (DataRow dbRow in IndicatorsAndCountries.Tables[1].Rows)
            {
                //Create a country node
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Country"].ToString(),
                      dbRow["CountryID"].ToString(),
                      true,
                      true,
                      false);

                //Add node to the dropdown
                CountryTree.Nodes.Add(newNode);
            }

            //Checkon the defaults countries
            SelectDefaultsForCheckBoxCombo(CountryCombo, CountryTree, hdnCountry);

            #endregion

            #region Binding Years dropdown
            
            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            hdnYears.Value = "";

            //Loop through all the valid years and add them as tree nodes to years dropdown
            for (int counter = StartYear; counter <= EndYear; counter++)
            {                
                //Create a year node
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(counter.ToString(),
                      counter.ToString(),
                      false,
                      false,
                      false);

                //Add the node to dropdown
                YearTree.Nodes.Add(newNode);

                //Keep all the years in hidden field
                hdnYears.Value += counter.ToString()+",";
            }

            //Set the year dropdown selected value
            if (string.IsNullOrEmpty(hdnYear.Value))
            {                
                //If hdnyear field is empty then get the default comparison year from config
                CArt.TreeViewNode CurrentYearNode = YearTree.FindNodeById(GlobalSettings.CountryComparisonDefaultYear);
                                
                if (CurrentYearNode != null)
                {
                    //If default year is found in the tree then make it selected
                    YearTree.SelectedNode = CurrentYearNode;
                }
                else
                {
                    //Otherwise make the start year node as selected
                    YearTree.SelectedNode = YearTree.FindNodeById(StartYear.ToString());
                }

                //Store the selected year in hidden field
                hdnYear.Value = YearTree.SelectedNode.Value;

                //Set the selected year value as combo box text
                YearCombo.Text = YearTree.SelectedNode.Value;
            }
            else
            {
                //Otherwise If hidden field contains a value then make it as selected year.
                YearTree.SelectedNode = YearTree.FindNodeById(hdnYear.Value);
            }

            #endregion
        }

        private CArt.TreeViewNode CreateNode(string text,
            string value,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            //Create a treeview node object and set the attributes
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        private void SelectDefaultsForCheckBoxCombo(CArt.ComboBox combo,
            CArt.TreeView tree,
            HiddenField hdnField)
        {   
            //By default select first 5 nodes....Afterwards maintain all the node selections
            if (hdnField.Value == "")
            {
                //Default selection
                combo.Text = "";
                int DefaultSelectionsCount = 5;
                foreach (CArt.TreeViewNode tnode in tree.Nodes)
                {
                    if (DefaultSelectionsCount <= 0) break;
                    if (tnode.ShowCheckBox)
                    {
                        combo.Text += tnode.Text + ";";
                        hdnField.Value += tnode.Value + ",";
                        tnode.Checked = true;
                        DefaultSelectionsCount--;
                    }
                }
                combo.Text = combo.Text.TrimEnd(new char[] { ';' });
            }
            else
            {
                //Maintaining selections
                string Selections= ","+hdnField.Value;

                foreach (CArt.TreeViewNode tnode in tree.Nodes)
                {
                    if (Selections.Contains(","+tnode.ID + ","))
                    {                        
                        tnode.Checked = true;
                       
                    }
                }
            }
        }

        public int GetCountryComparisonMaxLimit()
        {
            return GlobalSettings.CountryComparisonMaxLimit;
        }

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Workbook excel = new Workbook();
            string selectedYear = YearCombo.Text;
            string header = "Country Comparison";

            //Log
            LogUsage("Results - Country Comparison - Excel download", "Grid1", "");

            //Get results to export to excel
            DataSet ResultsData = GetResults(true);

            if (ResultsData != null &&
                ResultsData.Tables.Count > 0)
            {
                try
                {
                    //Remove definition column
                    ResultsData.Tables[0].Columns.Remove("Definition");
                }
                catch { }
                try
                {
                    //Remove taxonomyid column
                    ResultsData.Tables[0].Columns.Remove("taxonomyid");
                }
                catch { }
            }

            //Check the result set
            if (ResultsData != null && ResultsData.Tables.Count > 0)
            {
                //If data found then export it to excel
                excel = ExtractionsHelper.ExportToExcelCountryComparisonData(ExcelTemplatePath,
                             header,
                             LogoPath,
                             selectedYear,
                             ResultsData.Tables[0]);
            }

            //Save the excel to physical location
            excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);            
        }

        public string getTaxonomyFilterPage()
        {
            return GlobalSettings.SearchPage.ToLower().Contains("search1") ? "TaxonomyFilter1.aspx" : "TaxonomyFilter.aspx";
        }        
    }
}
