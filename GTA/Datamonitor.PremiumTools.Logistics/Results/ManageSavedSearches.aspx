<%@ Page Language="C#" 
         AutoEventWireup="true" 
         MasterPageFile="~/MasterPages/Logistics.Master" 
         CodeBehind="ManageSavedSearches.aspx.cs" 
         Inherits="Datamonitor.PremiumTools.Generic.Results.ManageSavedSearches" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server"> 
 <asp:HyperLink ID="HomeLink" runat="server" Text="Home"></asp:HyperLink>
  <div id="maincontent_full">
  
    <GMPT:PageTitleBar id="PageTitleBar1" runat="server"></GMPT:PageTitleBar>
    
    <div id="column_other">
      <div class="refinesearch" >
        <h2>Options</h2>
        <ul>
          <li class="selected"><a href="#"> Saved Searches Library</a></li><%--<li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'highslide-reset' } )">Reset My Home</a></li>--%></ul>     
      </div>
    </div>
    
    <div class="other_styles" id="maincontent2">
      <h1>With Saved Searches you'll never be more than a click away from the content most relevant to you.</h1>
      <p>The Knowledge Center allows you to save your search or browse at any point by clicking on the �Save this search� button at the top of any browse screen. All your Saved Searches are now handily stored for quick access in the drop-down menu in the top right of the screen.</p>
      <p>&nbsp;</p>
      <h2>Your Saved Searches Library</h2>
            
    <asp:GridView id="SavedSearchesGrid" 
                  BorderStyle="None" 
                  BorderWidth="0px" 
                  CssClass="browse_table" 
                  runat="server" 
                  AllowSorting="True" 
                  Width="100%"
                  AutoGenerateColumns="False" 
                  OnRowCommand="SavedSearchesGrid_RowCommand"
                  OnRowDataBound="SavedSearchesGrid_RowDataBound" 
                  OnSorting="SavedSearchesGrid_Sorting"                  
                  >        
      <AlternatingRowStyle BackColor="#ECF0F5" />
      <Columns>
        <asp:BoundField HeaderText="SearchName" Visible ="false" />
        <asp:TemplateField HeaderText="SearchId" Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblSearchId" Text='<%#Bind("searchId") %>' runat="Server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="searchName" Visible="False">
            <ItemTemplate>
                <asp:Label ID="lblSearchName" Text='<%# Bind("searchName") %>' runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Search Name" SortExpression="searchName">
            <ItemTemplate>
               <a id="aSearch" runat="server" style="font-weight:bold;"></a>
            </ItemTemplate>
            <HeaderStyle ForeColor="White" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Created" SortExpression="Date" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="txtcreatedDate" runat="server" Text='<%# Bind("createdDate") %>'></asp:TextBox>
            </EditItemTemplate>
            <HeaderStyle ForeColor="White" />
            <ItemTemplate>
                <asp:Label ID="lblcreatedDate" runat="server" Text='<%# Bind("createdDate") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField> 
        <asp:TemplateField HeaderText="Delete">
            <ItemStyle HorizontalAlign="Justify"/>
            <ItemTemplate>
                <asp:ImageButton ID="imageButtonDelete" runat="server" OnClientClick="return confirm('Are you sure you want to delete this Saved search?');" CommandName="DeleteSavedSearch" ImageUrl="~/Assets/Images/savedsearch_delete.gif" />
            </ItemTemplate>
        </asp:TemplateField>
      </Columns>            
    </asp:GridView>
    
    </div>
    
  </div>   
</asp:Content>

