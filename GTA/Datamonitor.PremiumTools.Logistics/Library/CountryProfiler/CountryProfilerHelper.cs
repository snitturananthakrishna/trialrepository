using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Configuration;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    public static class CountryProfilerHelper
    {
        /// <summary>
        /// Builds the country profiler chart.
        /// </summary>
        /// <param name="chartID">The chart ID.</param>
        /// <param name="countryID">The country ID.</param>
        /// <param name="countryName">Name of the country.</param>
        /// <param name="countryIDsTOCompare">The country Ids TO compare.</param>
        /// <returns></returns>
        public static dotnetCHARTING.Chart BuildCountryProfilerChart(string chartID,
            string countryID,
            string countryName,
            string countryIDsTOCompare)
        {
            dotnetCHARTING.Chart targetChart = new dotnetCHARTING.Chart();          

           
            if (!string.IsNullOrEmpty(chartID))
            {
                ///PREPARE SELECTION CRITERIA XML
                //GET SELECTED CHART'S FILTER CRITERIA FROM DB

                XmlDocument document = new XmlDocument();
                XmlDocument doc = new XmlDocument();
                string filterOptionsXML;
                string seriesChartType = string.Empty;
                string chartType;
                string chartTitle;
                string chartWidth="400";
                string chartHeight="300";
                bool ShowLegend = false;
                string xAxis;
                string yAxis;
                string series;
                string measure;
                string pageURL;
                Bitmap ChartBitmap = null;
                
                string SelectionsXML = string.Empty;

                DataSet ds = SqlDataService.GetCountryProfilerChartMetaData(Convert.ToInt32(chartID));

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    #region Prepare XML
                    string filterXml = ds.Tables[0].Rows[0]["SelectionCriteria"].ToString();
                    pageURL = ds.Tables[0].Rows[0]["PageUrl"].ToString();
                    document.LoadXml(filterXml);
                    chartType = document.SelectSingleNode("//ChartType").Attributes["SelectedValue"].Value;
                    chartTitle = document.SelectSingleNode("//Title").InnerText;
                    if (document.SelectSingleNode("//ChartType").Attributes["SeriesChartType"] != null)
                    {
                        seriesChartType= document.SelectSingleNode("//ChartType").Attributes["SeriesChartType"].Value;
                    }                   
                    filterOptionsXML = document.SelectSingleNode("//FilterOptions").OuterXml;
                    if (!string.IsNullOrEmpty(filterOptionsXML))
                    {
                        doc.LoadXml(filterOptionsXML);
                    }

                    DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
                    if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
                    {

                        string countryids = countryID;
                        switch (pageURL)
                        {
                            case "XYChart":
                                countryids = countryID + "," + countryIDsTOCompare;
                                break;
                        }

                        SelectionsXML = "<taxonomy id='1' selectIDs='" + countryids + "' columnID='CountryID' />";

                        //Build selection criteria xml
                        foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                        {
                            string TaxonomySelectedValue = string.Empty;
                            string TaxonomySelectedText = string.Empty;
                            if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("1"))
                            {
                                string TaxonomyTypeID = taxonomyRow["ID"].ToString();
                                string TaxonomyName = taxonomyRow["Name"].ToString();                                

                                if (!string.IsNullOrEmpty(filterOptionsXML))
                                {
                                    string xPathExpression = "//FilterOptions/TaxonomyType[@ID='" + TaxonomyTypeID + "']";
                                    XmlElement element = (XmlElement)doc.SelectSingleNode(xPathExpression);

                                    if (element != null)
                                    {
                                        TaxonomySelectedValue = element.Attributes["SelectedValue"].Value;

                                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                            taxonomyRow["ID"].ToString(),
                                            TaxonomySelectedValue,
                                            taxonomyRow["ColumnRowID"].ToString());
                                    }
                                }
                            }

                        }

                        SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                    }

                    #endregion

                    //GET DATA FROM DATABASE & PLOT CHART
                    switch (pageURL)
                    {
                        case "XYChart":
                        #region XY CHART
                            xAxis = document.SelectSingleNode("//XAxis").Attributes["SelectedValue"].Value.ToString();
                            yAxis = document.SelectSingleNode("//YAxis").Attributes["SelectedValue"].Value.ToString();
                            series = document.SelectSingleNode("//Series").Attributes["SelectedValue"].Value.ToString();
                            chartWidth = document.SelectSingleNode("//ChartType").Attributes["Width"].Value.ToString();
                            chartHeight = document.SelectSingleNode("//ChartType").Attributes["Height"].Value.ToString();
                            ShowLegend = Convert.ToBoolean(document.SelectSingleNode("//ChartType").Attributes["ShowLegend"].Value);

                            //targetChart.DefaultSeries.DefaultElement.URL = "JavaScript:alert(\'Clicked on element %Name in series %SeriesName\')";

                            DataSet ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(xAxis),
                                Int32.Parse(yAxis),
                                Int32.Parse(series),
                                SelectionsXML);

                            
                            if (ChartDataset != null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
                            {
                                string ChartTypeSeries = "3";
                                string YAxisLabel = ChartDataset.Tables[0].Rows[0]["Units"].ToString();
                                if (!string.IsNullOrEmpty(chartType))
                                {
                                    ChartTypeSeries = chartType.Split('|')[1];
                                }
                                string[] seriesNames = new string[]{};
                                string[] types = new string[] { };

                                targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                                    targetChart,
                                    ChartTypeSeries,
                                    YAxisLabel,
                                    "",
                                    new System.Web.UI.WebControls.Unit(chartWidth),
                                    new System.Web.UI.WebControls.Unit(chartHeight));

                                //TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                                //Set legend properties and styles
                                targetChart.LegendBox.Visible = ShowLegend;
                                targetChart.DefaultElement.ToolTip = "Click to view Country Snapshot";
                                                
                                if (!string.IsNullOrEmpty(seriesChartType))
                                {
                                    string[] seriesTexts = seriesChartType.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                                    types = seriesTexts.Length > 0 ? seriesTexts[0].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                                    seriesNames = seriesTexts.Length > 1 ? seriesTexts[1].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                                }

                                if (seriesNames.Length == 0 && ChartTypeSeries.Equals("stacked"))
                                {
                                    targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                }
                                if (ChartTypeSeries.Equals("Horizontal"))
                                {
                                    targetChart.Type = dotnetCHARTING.ChartType.ComboHorizontal;

                                }

                                if (targetChart.SeriesCollection.Count > 0 && targetChart.SeriesCollection[0].Elements.Count > 0)
                                {
                                    foreach (dotnetCHARTING.Element el in targetChart.SeriesCollection[0].Elements)
                                    {
                                        el.URL = "../Results/CountryOverview.aspx?country="+el.Name;
                                        el.URLTarget = "_blank";

                                        if(countryName.Equals(el.Name,StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            el.Color = Color.FromArgb(255, 172, 112);
                                        }
                                    }
                                }

                                for (int i = 0; i < targetChart.SeriesCollection.Count; i++)
                                {                                    
                                    if (seriesChartType.Length > 0)
                                    {
                                        if (seriesNames != null)
                                        {
                                            for (int j = 0; j < seriesNames.Length; j++)
                                            {
                                                if (seriesNames[j].Equals(targetChart.SeriesCollection[i].Name))
                                                {
                                                    targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(types[j]);
                                                    if (ChartTypeSeries.Equals("stacked") && (types[j] == "4" || types[j] == "7"))
                                                    {
                                                        targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                                    }
                                                    if (types[j] == "4")
                                                    {
                                                        targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                                        targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string Seriestype;
                                            Seriestype = i < types.Length ? types[i] : ChartTypeSeries;

                                            if (!Seriestype.Equals("Horizontal"))
                                            {
                                                targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(Seriestype);

                                                if (Seriestype == "4")
                                                {
                                                    targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                                    targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                                    targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                                }
                                            }
                                        }
                                    }
                                }

                                
                            }

                            break;
                            #endregion
                        case "pieChart":
                            #region Pie CHART
                            series = document.SelectSingleNode("//Series").Attributes["SelectedValue"].Value.ToString();
                            measure = document.SelectSingleNode("//Measure").Attributes["SelectedValue"].Value.ToString();
                            chartWidth = document.SelectSingleNode("//ChartType").Attributes["Width"].Value.ToString();
                            chartHeight = document.SelectSingleNode("//ChartType").Attributes["Height"].Value.ToString();
                            ShowLegend = Convert.ToBoolean(document.SelectSingleNode("//ChartType").Attributes["ShowLegend"].Value);

                            ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(series),
                                Int32.Parse(measure),
                                SelectionsXML);


                            if (ChartDataset != null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
                            {
                                string ChartTypeSeries = "";
                                string ChartType = "Pie";
                                if (!string.IsNullOrEmpty(chartType))
                                {
                                    ChartTypeSeries = chartType.Split('|')[1];
                                    ChartType = ChartTypeSeries == "Pie" ? "Pie" : "Donut";
                                }
                                //string AxisCriteria = "Series: " + series + "; Measure: " + measure;

                                targetChart = TwoDimensionalCharts.PlotPieChartProfiler(ChartDataset,
                                    targetChart,
                                    new System.Web.UI.WebControls.Unit(chartWidth),
                                    new System.Web.UI.WebControls.Unit(chartHeight),
                                    ChartType);
                                targetChart.LegendBox.Visible = ShowLegend;
                            }


                            #endregion
                            break;
                    }
                   
                }

               
                
            }



            return targetChart;
            
        }

    }    
}
