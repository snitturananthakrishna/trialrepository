using System;
using System.IO;
using System.Data;
using System.Web;
using System.Collections;

namespace Datamonitor.PremiumTools.Logistics.Library.Handlers
{
    /// <summary>
    /// Session handler to store session data
    /// </summary>  
    public class SessionHandler : IHttpHandler
    {
        /// <summary>The query string parameter to use for the taxonomy type id.</summary>
        public const string TAXONOMY_TYPE_ID_PARAM = "taxonomyTypeID";

        /// <summary>The query string parameter to use for the taxonomy id.</summary>
        public const string TAXONOMY_ID_PARAM = "taxonomyID";

        /// <summary>The query string parameter to use for the name.</summary>
        public const string NAME_PARAM = "name";

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {

            // Get querystring params               
            string taxonomyTypeID = context.Request.QueryString[TAXONOMY_TYPE_ID_PARAM];
            string taxonomyID = context.Request.QueryString[TAXONOMY_ID_PARAM];
            string name = context.Request.QueryString[NAME_PARAM];

            // Validate params
            if (string.IsNullOrEmpty(taxonomyTypeID))
            {
                throw new ArgumentNullException(TAXONOMY_TYPE_ID_PARAM, "The taxonomy type id must be supplied on the querystring");
            }
            if (string.IsNullOrEmpty(taxonomyID))
            {
                throw new ArgumentNullException(TAXONOMY_ID_PARAM, "The taxonomy id must be supplied on the querystring");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(NAME_PARAM, "The name must be supplied on the querystring");
            }

            //Save data in session

            //Return status
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
