using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Library.SqlDataAccess
{
    /// <summary>
    /// 
    /// </summary>
    public class SqlDataService : DataServiceBase
    {
        private const string GETGRIDDATA_PROC_NAME = "GetGridData";
        private const string GETTAXONOMY_PROC_NAME = "getTaxonomy";
        private const string GETSUBTAXONOMY_PROC_NAME = "getTaxonomyForParentID";
        private const string GETYAXISDATA_PROC_NAME = "GetYaxisData";
        private const string GETCHARTDATA_PROC_NAME = "GetChartData";
        //private const string GETDRPSDATA_PROC_NAME = "GetTaxonomiesLeafNodes";
        private const string GETANALYSISDATA_PROC_NAME = "GetAnalysisData";
        private const string GETPOPUPCHARTDATA_PROC_NAME = "GetPopupChartData";
        private const string GETDUALYAXISDATA_PROC_NAME = "GetDualYAxisData";
        private const string GETPIEDATA_PROC_NAME = "GetPieChartData";
        private const string GETBUBBLEDATA_PROC_NAME = "GetBubbleChartData";
        private const string GETEXTERNALINDICATORSDATA_PROC_NAME = "GetMacroEconomicData";
        private const string GETMEPOPUPCHARTDATA_PROC_NAME = "GetMEPopupChartData";
        private const string GETCOUNTRYCOMPARISONCHARTDATA_PROC_NAME = "GetCountryComparisonChartData";
        private const string GETINDICATORSANDCOUNTRIES_PROC_NAME = "GetIndicatorsAndCountries";
        private const string GETINDICATORSFORBCBUBBLECHART_PROC_NAME = "GetIndicatorsForBCBubbleChart";
        private const string GETHEATMAPDATA_PROC_NAME = "GetHeatMapData";
        private const string GETINDICATORCOMPARISONCHARTDATA_PROC_NAME = "GetIndicatorComparisonChartData";
        private const string GETCUSTOMCHARTDATA_PROC_NAME = "GetCustomChartData";
        private const string ADDUSERLOGGINGDETAILS_PROC_NAME = "AddUserLoggingDetails";
        private const string GETEXTERNALINDICATORSDATAV1_PROC_NAME = "GetMacroEconomicDataV1";
        private const string GETMINMAXYEARS_PROC_NAME = "GetMinMaxYears";
        private const string GETCURRENCYTYPES_PROC_NAME = "GetCurrencyTypes";
        private const string GETGRIDDATAFORMANUALPAGING_PROC_NAME = "GetGridDataForManualPaging";
        private const string GETSUBTAXONOMYTYPEID_PROC_NAME = "USP_GetSubTaxonomyTypeID";
        private const string GETGRIDDATAVIEW1_PROC_NAME = "GetGridDataView1";
        private const string GETGRIDDATAVIEW2_PROC_NAME = "GetGridDataView2";
        private const string GETGRIDDATAVIEW3_PROC_NAME = "GetGridDataView3";
        private const string GETGRIDDATAVIEW4_PROC_NAME = "GetGridDataView4";
        private const string GETCOUNTRYCOMPARISONGRIDDATA_PROC_NAME = "GetCountryComparisonGridData";
        private const string GETPREDEFINEDREPORTSECTIONS_PROC_NAME = "GetPredefinedReportSections";
        private const string GETPREDEFINEDREPORTSECTIONSDATA_PROC_NAME = "GetPredefinedReportSectionData";
        private const string GETCOUNTRYDATAFORREPORTS_PROC_NAME = "GetCountryDataForReports";
        private const string GETCOUNTRYCOMPARISONIMAGEDATA_PROC_NAME = "GetCountryComparisonImageData";
        private const string GETCOUNTRYCOMPARISONIMAGEMEDATA_PROC_NAME = "GetCountryComparisonImageMEData";
        private const string GETCOUNTRYOVERVIEWALLDATA_PROC_NAME = "GetCountryOverviewAllData";
        private const string GETCOUNTRYOVERVIEWQUARTILEDATA_PROC_NAME = "GetCountryOverviewQuartileData";
        private const string GETCOUNTRYOVERVIEWKEYSTATISTICS_PROC_NAME = "GetCountryOverviewKeyStatistics";
        private const string GETALLCOUNTRIES_PROC_NAME = "GetAllCountries";
        private const string GETCOUNTRYOVERVIEWTRENDCHARTDATA_PROC_NAME = "GetCountryOverviewTrendChartData";
        private const string GETCOUNTRYOVERVIEWINDICATORQUARTILEDATA_PROC_NAME = "GetCountryOverviewIndicatorQuartileData";
        private const string GETADDITIONALDETAILS_PROC_NAME = "GetAdditionalDetails";
        private const string GETCOUNTRYOVERVIEWSECTIONDETAILS_PROC_NAME = "GetCountryOverviewSectionDetails";
        private const string GETMEASURESFORSELECTEDINDICATORS_PROC_NAME = "GetMeasuresForSelectedIndicators";
        private const string GETBASICBUBBLECHARTDATA_PROC_NAME = "GetBasicBubbleChartData";
        private const string GETQUICKSEARCHTAXONOMYLIST_PROC_NAME = "GetQuickSearchTaxonomy";
        private const string GETCOUNTRIESINORDER_PROC_NAME = "GetCountriesInOrder";
        private const string GETGROUPTAXONOMYIDS_PROC_NAME = "GetGroupTaxonomyIds";
        private const string GETPREDICTIVESEARCHTAXONOMY_PROC_NAME = "GetPredictiveSearchTaxonomy";
        private const string GETDATASOURCEINFORMATION_PROC_NAME = "GetDataSourceInformation";
        private const string GETPREDEFINEDTABLESLIST_PROC_NAME = "GetPredefinedTables";
        private const string GETPREDEFINEDTABLEDATA_PROC_NAME = "GetPredefinedTableData";
        private const string GETINDICATORMETHODOLOGYLINKS_PROC_NAME = "GetIndicatorMethodologyLinks";
        
        
        
        
        public SqlDataService() : base() { }



        /// <summary>
        /// Gets the taxonomy list.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyList(int taxonomyTypeID, string source)
        {
            DataSet retValue = null;
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(GlobalSettings.TaxonomyDefaultsConfigPath));
            string xPathExpression = "//TaxonomyType[@ID='" + taxonomyTypeID + "']";
            XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string DependentTaxonomytype = xaxisItem.Attributes["DependentTaxonomytype"].Value;

            if (!string.IsNullOrEmpty(DependentTaxonomytype) && !DependentTaxonomytype.Equals("None", StringComparison.CurrentCultureIgnoreCase))
            {
                string taxonomyIDs = string.Empty;
                Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(DependentTaxonomytype);
                if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                {
                    //Get all keys (ids) from dictionary
                    List<int> keys = new List<int>(selectedIDsList.Keys);
                    //convert int array to string array
                    string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                    taxonomyIDs = string.Format(",{0},", string.Join(",", SelectedIDs));
                    retValue = GetDependentTaxonomyByTypeID(taxonomyTypeID, taxonomyIDs, source);
                }
                else
                {
                    retValue = GetOriginalTaxonomyByTypeID(taxonomyTypeID, source);
                }

            }
            else
            {
                retValue = GetOriginalTaxonomyByTypeID(taxonomyTypeID, source);
            }

            return retValue;

        }

        /// <summary>
        /// Gets the dependent taxonomy by type ID.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyIDs">The taxonomy Ids.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        private static DataSet GetDependentTaxonomyByTypeID(int taxonomyTypeID,
            string taxonomyIDs,
            string source)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@selectedIDs", taxonomyIDs));
            sqlParams.Add(new SqlParameter("@source", source));
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetTaxonomyByDependency", sqlParams.ToArray());
            return ds;
        }

        public static int SetWelcomePageVisibility(string KCuserID,
           bool ShowPage)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            sqlParams.Add(new SqlParameter("@KCuserID", KCuserID));
            sqlParams.Add(new SqlParameter("@ShowPage", ShowPage));

            int rc = SqlHelper.ExecuteNonQuery(ConnectionString, "SetWelcomePageVisibility", sqlParams.ToArray());
            return rc;
        }

        public static DataSet GetWelcomePageVisibility(string KCuserID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@KCuserID", KCuserID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetWelcomePageVisibility", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the predefined chart meta data.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static DataSet GetPredefinedChartMetaData(int id)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@id", id));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetPredefinedChartMetaData", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the country profiler chart meta data.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static DataSet GetCountryProfilerChartMetaData(int id)
        {
            DataSet ds;
            if (HttpContext.Current.Cache.Get("CountryProfilerChartMetaData" + id) != null)
            {
                ds = (DataSet)HttpContext.Current.Cache.Get("CountryProfilerChartMetaData" + id);
            }
            else
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter("@id", id));

                ds = SqlHelper.ExecuteDataset(ConnectionString, "GetCountryProfilerChartMetaData", sqlParams.ToArray());

                HttpContext.Current.Cache.Add("CountryProfilerChartMetaData" + id,
                    ds,
                    null,
                    DateTime.Now.AddHours(1),
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }
            return ds;
        }

        

        /// <summary>
        /// Gets the original taxonomy by type ID.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        public static DataSet GetOriginalTaxonomyByTypeID(int taxonomyTypeID, string source)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@source", source));
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETTAXONOMY_PROC_NAME, sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the macroeconomic indicators list.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetMacroeconomicIndicatorsList()
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
           
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetMacroeconomicIndicatorsList", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the taxonomy for selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="parentIDs">The parent Ids.</param>
        /// <param name="selectedTaxonomyIDs">The selected taxonomy Ids.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyForSelection(int taxonomyTypeID, 
            string parentIDs,
            string selectedTaxonomyIDs)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@taxonomyIds", string.IsNullOrEmpty(parentIDs) ? null : parentIDs));
            sqlParams.Add(new SqlParameter("@DependentTaxonomySelectedIds", selectedTaxonomyIDs));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetTaxonomyForSelection", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the taxonomy for selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="parentIDs">The parent Ids.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyForSelection(int taxonomyTypeID, string parentIDs)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@taxonomyIds", parentIDs));
            sqlParams.Add(new SqlParameter("@selectedIds", string.Empty));

            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetTaxonomyForSelection", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the taxonomy data.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyData(int taxonomyTypeID)
        {
            DataTable retValue = null;
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETTAXONOMY_PROC_NAME, sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Get the list of columns for the Numerical dataset.
        /// The Columns list is stored in the tblColumns table in SQL Server
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTaxonomyForParent(int parentTaxonomyID)
        {
            DataTable retValue = null;
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@parentTaxonomyID", parentTaxonomyID));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETSUBTAXONOMY_PROC_NAME, sqlParams.ToArray());
            if (ds != null)
                retValue = ds.Tables[0];
            return retValue;
        }

        /// <summary>
        /// Gets the analysis data.
        /// </summary>
        /// <param name="xaxisTypeID">The x axis type ID.</param>
        /// <param name="yAxisTaxonomyID">The y axis taxonomy ID.</param>
        /// <param name="seriesTypeID">The series type ID.</param>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="years">The years.</param>
        /// <returns></returns>
        public static DataSet GetAnalysisData(int xaxisTypeID,
            int yAxisTaxonomyID,
            int seriesTypeID,
            string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@xAxisTypeID", xaxisTypeID));
            sqlParams.Add(new SqlParameter("@YAxisTaxonomyID", yAxisTaxonomyID));
            sqlParams.Add(new SqlParameter("@seriesTypeID", seriesTypeID));
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETANALYSISDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the analysis data.
        /// </summary>
        /// <param name="SeriesTypeID">The series type ID.</param>
        /// <param name="xaxisTypeID">The x axis type ID.</param>
        /// <param name="yAxis1TaxonomyID">The y axis1 taxonomy ID.</param>
        /// <param name="yAxis2TaxonomyID">The y axis2 taxonomy ID.</param>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <returns></returns>
        public static DataSet GetBubbleChartData(int SeriesTypeID,
            int xaxisTypeID,
            int yAxis1TaxonomyID,
            int yAxis2TaxonomyID,
            string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@SeriesTypeID", SeriesTypeID));
            sqlParams.Add(new SqlParameter("@xAxisTypeID", xaxisTypeID));
            sqlParams.Add(new SqlParameter("@YAxisTaxonomyID", yAxis1TaxonomyID));
            sqlParams.Add(new SqlParameter("@yAxis2TaxonomyID", yAxis2TaxonomyID));
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETBUBBLEDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the DualYAxis data.
        /// </summary>
        /// <param name="xaxisTypeID">The x axis type ID.</param>
        /// <param name="yAxis1TaxonomyID">The y axis1 taxonomy ID.</param>
        /// <param name="yAxis2TaxonomyID">The y axis2 taxonomy ID.</param>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <returns></returns>
        public static DataSet GetDualYAxisData(int xaxisTypeID,
            int yAxis1TaxonomyID,
            int yAxis2TaxonomyID,
            string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@xAxisTypeID", xaxisTypeID));
            sqlParams.Add(new SqlParameter("@YAxisTaxonomyID", yAxis1TaxonomyID));
            sqlParams.Add(new SqlParameter("@seriesTypeID", yAxis2TaxonomyID));
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETDUALYAXISDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets PIE chart data.
        /// </summary>
        /// <param name="xaxisTypeID">The x axis type ID.</param>
        /// <param name="yAxisTaxonomyID">The Top dropdown value.</param>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="years">The years.</param>
        /// <returns></returns>
        public static DataSet GetPieChartData(int xaxisTypeID,
            int MeasureDropdownValue,
            string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@xAxisTypeID", xaxisTypeID));
            sqlParams.Add(new SqlParameter("@TopValue", MeasureDropdownValue));
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETPIEDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the unique taxonomy by criteria.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <returns></returns>
        public static DataSet GetUniqueTaxonomyByCriteria(string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", null));
            sqlParams.Add(new SqlParameter("@YAxis", null));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetUniqueTaxonomyByCriteria", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the unique taxonomy by criteria.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="YAxis">The Y axis.</param>
        /// <returns></returns>
        public static DataSet GetUniqueTaxonomyByCriteria(string selectionsXML, string YAxis)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", null));
            sqlParams.Add(new SqlParameter("@source", null));
            if (!string.IsNullOrEmpty(YAxis))
            {
                sqlParams.Add(new SqlParameter("@YAxis", YAxis));
            }
            else
            {
                sqlParams.Add(new SqlParameter("@YAxis", null));
            }

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetUniqueTaxonomyByCriteria", sqlParams.ToArray());
            return ds;
        }

        public static DataSet GetUniqueTaxonomyByCriteria(string selectionsXML, 
            string YAxis, 
            string source)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", null));
            sqlParams.Add(new SqlParameter("@YAxis", !string.IsNullOrEmpty(YAxis) ? YAxis : null)); 
            sqlParams.Add(new SqlParameter("@source", !string.IsNullOrEmpty(source) ? source : null));
                       

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetUniqueTaxonomyByCriteria", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the unique taxonomy by criteria.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        public static DataSet GetUniqueTaxonomyByCriteria(string selectionsXML, int taxonomyTypeID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@YAxis", null));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetUniqueTaxonomyByCriteria", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the Y axis taxonomy by criteria.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="includeGrowthRates">if set to <c>true</c> [include growth rates].</param>
        /// <returns></returns>
        public static DataSet GetYAxisTaxonomyByCriteria(string selectionsXML, bool includeGrowthRates)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@inlcudeGrowths", includeGrowthRates));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetYAxisTaxonomyByCriteria", sqlParams.ToArray());
            return ds;
        }


        /// <summary>
        /// Gets the grid data.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="includeGrowths">if set to <c>true</c> [include growths].</param>
        /// <param name="predefinedViewID">The predefined view ID.</param>
        /// <param name="conversionIDs">The conversion I ds.</param>
        /// <returns></returns>
        public static DataSet GetGridData(string selectionsXML,           
            bool includeGrowths,
            string predefinedViewID,           
            string conversionIDs)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));            
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETGRIDDATA_PROC_NAME, sqlParams.ToArray());            
            
            return ds;
        }

        /// <summary>
        /// Gets the view results grid data with manual paging option: view1 page.
        /// </summary>
        /// <param name="selectionsXML"></param>
        /// <param name="includeGrowths"></param>
        /// <param name="currencyConversionID"></param>
        /// <param name="includeMacroeconomicData"></param>
        /// <param name="predefinedViewID"></param>
        /// <param name="conversionIDs"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static DataSet GetGridDataView1(string selectionsXML,
            bool includeGrowths,
            int currencyConversionID,
            bool includeMacroeconomicData,
            string predefinedViewID,
            string conversionIDs,
            int pageIndex,
            int pageSize,
            string sortColumn,
            string sortDirection)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@currencyConversionID", currencyConversionID));
            sqlParams.Add(new SqlParameter("@includeMacroeconomicData", includeMacroeconomicData));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));
            sqlParams.Add(new SqlParameter("@pageIndex", pageIndex));
            sqlParams.Add(new SqlParameter("@pageSize", pageSize));
            sqlParams.Add(new SqlParameter("@sortColumn", sortColumn));
            sqlParams.Add(new SqlParameter("@sortDirection", sortDirection));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETGRIDDATAVIEW1_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the grid data view2.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="includeGrowths">if set to <c>true</c> [include growths].</param>
        /// <param name="targetCurrencyid">The target currencyid.</param>
        /// <param name="includeMacroeconomicData">if set to <c>true</c> [include macroeconomic data].</param>
        /// <param name="predefinedViewID">The predefined view ID.</param>
        /// <param name="conversionIDs">The conversion I ds.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <returns></returns>
        public static DataSet GetGridDataView2(string selectionsXML,
            bool includeGrowths,
            int targetCurrencyid,
            bool includeMacroeconomicData,
            string predefinedViewID,
            string conversionIDs,
            int pageIndex,
            int pageSize,
            string startYear,
            string endYear,
            bool currencyconversionType)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@targetCurrencyid", targetCurrencyid));
            sqlParams.Add(new SqlParameter("@includeMacroeconomicData", includeMacroeconomicData));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));
            sqlParams.Add(new SqlParameter("@pageIndex", pageIndex));
            sqlParams.Add(new SqlParameter("@pageSize", pageSize));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));
            sqlParams.Add(new SqlParameter("@CurrencyconversionType", currencyconversionType));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETGRIDDATAVIEW2_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the grid data view3.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="includeGrowths">if set to <c>true</c> [include growths].</param>
        /// <param name="targetCurrencyid">The target currencyid.</param>
        /// <param name="includeMacroeconomicData">if set to <c>true</c> [include macroeconomic data].</param>
        /// <param name="predefinedViewID">The predefined view ID.</param>
        /// <param name="conversionIDs">The conversion I ds.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <returns></returns>
        public static DataSet GetGridDataView3(string selectionsXML,
            bool includeGrowths,
            int targetCurrencyid,
            bool includeMacroeconomicData,
            string predefinedViewID,
            string conversionIDs,
            int pageIndex,
            int pageSize,
            string startYear,
            string endYear)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@targetCurrencyid", targetCurrencyid));
            sqlParams.Add(new SqlParameter("@includeMacroeconomicData", includeMacroeconomicData));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));
            sqlParams.Add(new SqlParameter("@pageIndex", pageIndex));
            sqlParams.Add(new SqlParameter("@pageSize", pageSize));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETGRIDDATAVIEW3_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the view results grid data with manual paging option: view1 page.
        /// </summary>
        /// <param name="selectionsXML"></param>
        /// <param name="includeGrowths"></param>
        /// <param name="currencyConversionID"></param>
        /// <param name="includeMacroeconomicData"></param>
        /// <param name="predefinedViewID"></param>
        /// <param name="conversionIDs"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static DataSet GetGridDataView4(string selectionsXML,
            bool includeGrowths,
            int currencyConversionID,
            bool includeMacroeconomicData,
            string predefinedViewID,
            string conversionIDs,
            int pageIndex,
            int pageSize,
            string sortColumn,
            string sortDirection)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@currencyConversionID", currencyConversionID));
            sqlParams.Add(new SqlParameter("@includeMacroeconomicData", includeMacroeconomicData));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));
            sqlParams.Add(new SqlParameter("@pageIndex", pageIndex));
            sqlParams.Add(new SqlParameter("@pageSize", pageSize));
            sqlParams.Add(new SqlParameter("@sortColumn", sortColumn));
            sqlParams.Add(new SqlParameter("@sortDirection", sortDirection));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETGRIDDATAVIEW4_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the grid data.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="selectedYears">The selected years.</param>
        /// <param name="includeGrowths">The status to include growths or not.</param>
        /// <param name="predefinedViewID">The predefined view ID.</param>
        /// <returns></returns>
        public static DataSet GetGridDataV2(string selectionsXML,
            string selectedYears,
            bool includeGrowths,
            string predefinedViewID,
            string startYear,
            string endYear,
            int conversionID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@years", selectedYears));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));
            sqlParams.Add(new SqlParameter("@conversionID", conversionID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                "GetGridDataV2",
                sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// Gets the chart data.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <returns></returns>
        public static DataSet GetChartData(string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETCHARTDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetYaxisData()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETYAXISDATA_PROC_NAME, null);
            return ds;
        }

        /// <summary>
        /// Gets the unit conversions list.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetUnitconversionsList()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetUnitconversionsList", null);
            return ds;
        }

        /// <summary>
        /// Gets the predefined charts list.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPredefinedChartList()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetPredefinedChartList", null);
            return ds;
        }



        /// <summary>
        /// Gets the chart data for a given ColumnRowID.
        /// </summary>
        /// <param name="taxonomyTypeID">The ColumnRowID.</param>
        /// <returns></returns>
        public static DataSet GetPopupChartData(long ColumnRowID,
            int startYear,
            int endYear,
            long conversionIDs,
            int targetCurrencyID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@ColumnRowID", ColumnRowID));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));
            sqlParams.Add(new SqlParameter("@conversionIDs", conversionIDs));
            sqlParams.Add(new SqlParameter("@targetCurrencyID", targetCurrencyID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETPOPUPCHARTDATA_PROC_NAME, sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the forced dependencies.
        /// </summary>
        /// <param name="taxonomyIds">The taxonomy ids.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetForcedDependencies(string taxonomyIds)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyIds", taxonomyIds));
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetForcedDependencies", sqlParams.ToArray());
            Dictionary<string, string> ForcedTaxonomies = new Dictionary<string, string>();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ForcedTaxonomies.Add(row["TaxonomyTypeID"].ToString(), row["TaxonomyIDs"].ToString());
                }
            }
            return ForcedTaxonomies;
        }

        /// <summary>
        /// Gets the external indicator data.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="years">The years.</param>
        /// <returns></returns>
        public static DataSet GetExternalIndicatorData(string selectionsXML, string years)
        {
            return GetExternalIndicatorData(selectionsXML, years, false);
        }

        /// <summary>
        /// Gets the external indicator data.
        /// </summary>
        /// <param name="selectionsXML">The selections XML.</param>
        /// <param name="years">The years.</param>
        /// <param name="years">option data is for grid/excel</param>
        /// <returns></returns>
        public static DataSet GetExternalIndicatorData(string selectionsXML, string years, bool isForGrid)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@years", years));
            sqlParams.Add(new SqlParameter("@isForGrid", isForGrid ? 1 : 0));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETEXTERNALINDICATORSDATA_PROC_NAME, sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the macro economic indicators chart data for a given ColumnRowID.
        /// </summary>
        /// <param name="rowID">The rowID.</param>
        /// <param name="startYear">The start year.</param>
        /// <param name="endYear">The end year.</param>
        /// <returns></returns>
        public static DataSet GetMEPopupChartData(long rowID,
            int startYear,
            int endYear)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETMEPOPUPCHARTDATA_PROC_NAME, sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the view results grid data with manual paging option.
        /// </summary>
        /// <param name="selectionsXML"></param>
        /// <param name="selectedYears"></param>
        /// <param name="macroEconomicYears"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="predefinedViewID"></param>
        /// <param name="includeGrowths"></param>
        /// <param name="startYear"></param>
        /// <param name="endYear"></param>
        /// <param name="conversionID"></param>
        /// <returns></returns>
        public static DataSet GetGridDataForManualPaging(string selectionsXML,
            string selectedYears,
            string macroEconomicYears,
            int pageIndex,
            int pageSize,
            string sortColumn,
            string sortDirection,
            string predefinedViewID,
            bool includeGrowths,
            string startYear,
            string endYear,
            int conversionID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));
            sqlParams.Add(new SqlParameter("@years", selectedYears));
            sqlParams.Add(new SqlParameter("@macroYears", macroEconomicYears));
            sqlParams.Add(new SqlParameter("@pageIndex", pageIndex));
            sqlParams.Add(new SqlParameter("@pageSize", pageSize));
            sqlParams.Add(new SqlParameter("@sortColumn", sortColumn));
            sqlParams.Add(new SqlParameter("@sortDirection", sortDirection));
            sqlParams.Add(new SqlParameter("@predefinedViewID", predefinedViewID));
            sqlParams.Add(new SqlParameter("@includeGrowths", includeGrowths));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));
            sqlParams.Add(new SqlParameter("@conversionID", conversionID));


            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETGRIDDATAFORMANUALPAGING_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        public static int savedSearchDetails(string ExtractTYPE,
                  string userName,
                  string searchName,
                  string searchXml)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@extractTYPE", ExtractTYPE));
            sqlParams.Add(new SqlParameter("@userName", userName));
            sqlParams.Add(new SqlParameter("@searchName", searchName));
            sqlParams.Add(new SqlParameter("@searchXml", searchXml));

            SqlParameter p1 = new SqlParameter("@output_ID", SqlDbType.Int);
            p1.Direction = ParameterDirection.Output;
            sqlParams.Add(p1);

            int result = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure,
                "usp_SavedSearchDetails",
                 sqlParams.ToArray());
            return result;
        }

        public static DataSet GetSavedSearches(string userName)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@userName", userName));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetSavedSearches", sqlParams.ToArray());
            return ds;
        }

        public static DataSet GetSavedSearchSearchCriteria(int searchId)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@searchId", searchId));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetSavedSearchSearchCriteria", sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetSavedSearchesOfUser(string userName,
                               string sortExpression)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@userName", userName));
            sqlParams.Add(new SqlParameter("@sortExpression", sortExpression));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Usp_GetSavedSearchesofUser", sqlParams.ToArray());
            return ds;
        }

        public static void DeleteSavedSearch(int searchId)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@searchId", searchId));

            SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Usp_DeleteSavedSearch", sqlParams.ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indicatorID"></param>
        /// <param name="selectedCountries"></param>
        /// <param name="startYear"></param>
        /// <param name="endYear"></param>
        /// <returns></returns>
        public static DataSet GetCountryComparisonChartData(long indicatorID,
            string selectedCountries,
            int startYear,
            int endYear)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@indicatorID", indicatorID));
            sqlParams.Add(new SqlParameter("@selectedCountries", selectedCountries));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, 
                CommandType.StoredProcedure, 
                GETCOUNTRYCOMPARISONCHARTDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectionsXML"></param>
        /// <returns></returns>
        public static DataSet GetIndicatorsAndCountries(string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETINDICATORSANDCOUNTRIES_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetIndicatorsForBCBubbleChart(string selectionsXML)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selections", selectionsXML));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETINDICATORSFORBCBUBBLECHART_PROC_NAME,
                 sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetHeatMapData(int measureTaxonomyID, 
                              string mapType, 
                              string selectionsXml)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@MeasureTaxonomyID", measureTaxonomyID));
            sqlParams.Add(new SqlParameter("@mapType", mapType));
            sqlParams.Add(new SqlParameter("@selections", selectionsXml));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, 
                CommandType.StoredProcedure, 
                GETHEATMAPDATA_PROC_NAME, 
                sqlParams.ToArray());
            
            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedIndicators"></param>
        /// <param name="countryID"></param>
        /// <param name="startYear"></param>
        /// <param name="endYear"></param>
        /// <returns></returns>
        public static DataSet GetIndicatorComparisonChartData(string selectedIndicators,
            int countryID,
            int startYear,
            int endYear)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selectedIndicators", selectedIndicators));
            sqlParams.Add(new SqlParameter("@countryID", countryID));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETINDICATORCOMPARISONCHARTDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetBasicBubbleChartData(string countryIDs,
                int xAxisIndicatorID,
                int yAxisIndicatorID,
                int bubbleIndicatorID,
                int year)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@CountryIds", countryIDs));
            sqlParams.Add(new SqlParameter("@XAxisIndicatorId", xAxisIndicatorID));
            sqlParams.Add(new SqlParameter("@YAxisIndicatorId", yAxisIndicatorID));
            sqlParams.Add(new SqlParameter("@BubbleIndicatorId", bubbleIndicatorID));
            sqlParams.Add(new SqlParameter("@Year", year));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETBASICBUBBLECHARTDATA_PROC_NAME,
                 sqlParams.ToArray());

            return ds;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedIndicators"></param>
        /// <param name="selectedCountries"></param>
        /// <param name="startYear"></param>
        /// <param name="endYear"></param>
        /// <returns></returns>
        public static DataSet GetCustomChartData(string selectedIndicators,
            string selectedCountries,
            int startYear,
            int endYear)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selectedIndicators", selectedIndicators));
            sqlParams.Add(new SqlParameter("@selectedCountries", selectedCountries));
            sqlParams.Add(new SqlParameter("@startYear", startYear));
            sqlParams.Add(new SqlParameter("@endYear", endYear));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETCUSTOMCHARTDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Site"></param>
        /// <param name="Event"></param>
        /// <param name="Product_ID"></param>
        /// <param name="AdditionalDetails"></param>
        /// <param name="IPAddress"></param>
        /// <param name="usrType"></param>
        public static void addUserLoggingDetails(string UserID, 
                 string SiteName, 
                 string Event, 
                 string DealID, 
                 string AdditionalDetails, 
                 string IPAddress, 
                 string userType)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();           
            sqlParams.Add(new SqlParameter("@UserID", UserID));
            sqlParams.Add(new SqlParameter("@SiteName", SiteName));
            sqlParams.Add(new SqlParameter("@Event", Event));
            sqlParams.Add(new SqlParameter("@DealID", DealID));
            sqlParams.Add(new SqlParameter("@AdditionalDetails", AdditionalDetails));
            sqlParams.Add(new SqlParameter("@IPAddress", IPAddress));
            sqlParams.Add(new SqlParameter("@userType", userType));

            SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, ADDUSERLOGGINGDETAILS_PROC_NAME,sqlParams.ToArray());
         }

         /// <summary>
         /// Gets the external indicator data.
         /// </summary>
         /// <param name="selectionsXML">The selections XML.</param>
         /// <param name="years">The years.</param>
         /// <param name="years">option data is for grid/excel</param>
         /// <returns></returns>
         public static DataSet GetExternalIndicatorDataV1(string selectionsXML, string years)
         {
             List<SqlParameter> sqlParams = new List<SqlParameter>();
             sqlParams.Add(new SqlParameter("@selections", selectionsXML));
             sqlParams.Add(new SqlParameter("@years", years));

             DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETEXTERNALINDICATORSDATAV1_PROC_NAME, sqlParams.ToArray());

             return ds;
         }

         /// <summary>
         /// 
         /// </summary>
         /// <param name="selectionsXML"></param>
         /// <returns></returns>
        public static DataSet GetMinMaxYears(string selectionsXML)
         {
             List<SqlParameter> sqlParams = new List<SqlParameter>();
             sqlParams.Add(new SqlParameter("@selections", selectionsXML));

             DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETMINMAXYEARS_PROC_NAME,
                 sqlParams.ToArray());

             return ds;
         }


         /// <summary>
         /// Gets the taxonomy definition.
         /// </summary>
         /// <param name="taxonomyID">The taxonomy ID.</param>
         /// <param name="selectedCountryIDs">The selected country Ids.</param>
         /// <returns></returns>
        public static DataSet GetTaxonomyDefinition(int taxonomyID, string selectedCountryIDs)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyID", taxonomyID));
            sqlParams.Add(new SqlParameter("@selectedCountryIDs", selectedCountryIDs));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                "GetTaxonomyDefinition",
                sqlParams.ToArray());

            return ds;
        }
         /// <summary>
         /// 
         /// </summary>
         /// <param name="fromID"></param>
         /// <returns></returns>
        public static DataSet GetCurrencyTypes(int fromID)
         {
             List<SqlParameter> sqlParams = new List<SqlParameter>();
             sqlParams.Add(new SqlParameter("@fromID", fromID));

             DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETCURRENCYTYPES_PROC_NAME,
                 sqlParams.ToArray());

             return ds;
         }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxonomyID"></param>
        /// <returns></returns>
        public static DataSet GetSubTaxonomyTypeID(long taxonomyID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyID", taxonomyID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETSUBTAXONOMYTYPEID_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }



        /// <summary>
        /// Gets the country comparison grid data.
        /// </summary>
        /// <param name="selectionXml">The selection XML.</param>
        /// <param name="countries">The countries.</param>
        /// <param name="year">The year.</param>
        /// <param name="forExcel">if set to <c>true</c> [for excel].</param>
        /// <param name="targetCurrencyID">The target currency ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryComparisonGridData(string selectionXml,
            string countries,
            string year,
            bool forExcel,
            int targetCurrencyID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selectionXml", selectionXml));
            sqlParams.Add(new SqlParameter("@countries", countries));
            sqlParams.Add(new SqlParameter("@year", year));
            sqlParams.Add(new SqlParameter("@forExcel", forExcel));
            sqlParams.Add(new SqlParameter("@targetCurrencyID", targetCurrencyID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETCOUNTRYCOMPARISONGRIDDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetPredefinedReportSections()
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure, 
                 GETPREDEFINEDREPORTSECTIONS_PROC_NAME, 
                 sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetCountryDataForReports()
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETCOUNTRYDATAFORREPORTS_PROC_NAME,
                 sqlParams.ToArray());

            return ds;
        }

        public static DataSet getPredefinedReportSectionsData(int SectionID, int CountryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@sectionID", SectionID));
            sqlParams.Add(new SqlParameter("@countryID", CountryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure, 
                 GETPREDEFINEDREPORTSECTIONSDATA_PROC_NAME, 
                 sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country map zoom level.
        /// </summary>
        /// <param name="CountryID">The country ID.</param>
        /// <returns></returns>
        public static string GetCountryMapZoomLevel(int CountryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            sqlParams.Add(new SqlParameter("@countryID", CountryID));

            object value = SqlHelper.ExecuteScalar(ConnectionString,
                 CommandType.StoredProcedure,
                 "GetZoomLevel",
                 sqlParams.ToArray());

            short val = value != null ? (short)value : (short)2;
            return val.ToString();
        }

        /// <summary>
        /// Gets the country overview data.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <param name="countryID">The country ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewKSData(int rowID, int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 "GetCountryOverviewKSData",
                 sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the chart data for a given indicator.
        /// </summary>
        /// <param name="rowID"></param>
        /// <param name="selectedCountries"></param>
        /// <param name="year"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DataSet GetCountryComparisonImageData(long rowID,
            string selectedCountries,
            string year,
            string type)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@countries", selectedCountries));
            sqlParams.Add(new SqlParameter("@year", year));
            sqlParams.Add(new SqlParameter("@type", type));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, 
                GETCOUNTRYCOMPARISONIMAGEDATA_PROC_NAME, 
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country comparison image ME data.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <param name="selectedCountries">The selected countries.</param>
        /// <param name="year">The year.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static DataSet GetCountryComparisonImageMEData(long rowID,
           string selectedCountries,
           string year,
           string type)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@countries", selectedCountries));
            sqlParams.Add(new SqlParameter("@year", year));
            sqlParams.Add(new SqlParameter("@type", type));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYCOMPARISONIMAGEMEDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country overview data for a given countryid.
        /// </summary>
        /// <param name="rowID"></param>
        /// <param name="selectedCountries"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewData(int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWALLDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewQuartileData(int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWQUARTILEDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewKeyStatistics(int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWKEYSTATISTICS_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country overview flag data.
        /// </summary>
        /// <param name="countryID">The country ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewFlagData(int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                "GetCountryOverviewFlagData",
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets all country list.
        /// </summary>        
        /// <returns></returns>
        public static DataSet GetAllCountries()
        {
            DataSet Countries = (DataSet)HttpContext.Current.Cache["AllCountries"];
            if (Countries == null)
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                Countries = SqlHelper.ExecuteDataset(ConnectionString,
                    GETALLCOUNTRIES_PROC_NAME,
                    sqlParams.ToArray());

                HttpContext.Current.Cache.Insert("AllCountries",
                    Countries,
                    null,
                    DateTime.Now.AddMinutes(180),
                    System.Web.Caching.Cache.NoSlidingExpiration);

            }
            return Countries;
        }


        /// <summary>
        /// Gets the country overview trend chart data.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewTrendChartData(long rowID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWTRENDCHARTDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country overview indicator quartile data.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewIndicatorQuartileData(int rowID,int year)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@year", year));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWINDICATORQUARTILEDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        public static DataSet GetCountryProfileIndicatorQuartileData(int rowID, int year)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@year", year));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                "GetCountryProfilerIndicatorQuartileData",
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the additional details.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <param name="targetCurrencyID">The target currency ID.</param>
        /// <returns></returns>
        public static DataSet GetAdditionalDetails(int rowID, int targetCurrencyID,bool isExcel)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));
            sqlParams.Add(new SqlParameter("@targetCurrencyID", targetCurrencyID));
            sqlParams.Add(new SqlParameter("@isExcel", isExcel));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETADDITIONALDETAILS_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the country overview section details.
        /// </summary>
        /// <param name="sectionID">The section ID.</param>
        /// <returns></returns>
        public static DataSet GetCountryOverviewSectionDetails(int sectionID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@sectionID", sectionID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETCOUNTRYOVERVIEWSECTIONDETAILS_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the measures for selected indicators.
        /// </summary>
        /// <param name="SelectedIndicators">The selected indicators.</param>
        /// <returns></returns>
        public static DataSet GetMeasuresForSelectedIndicators(string SelectedIndicators)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@Indicators", SelectedIndicators));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETMEASURESFORSELECTEDINDICATORS_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the quick search taxonomy list.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        public static DataSet GetQuickSearchTaxonomyList(int taxonomyTypeID)
        { 
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETQUICKSEARCHTAXONOMYLIST_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the countries in order.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetCountriesInOrder()
        { 
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETCOUNTRIESINORDER_PROC_NAME);
            return ds;
        }

        /// <summary>
        /// Gets the group taxonomy ids.
        /// </summary>
        /// <param name="groupID">The group ID.</param>
        /// <returns></returns>
        public static DataSet GetGroupTaxonomyIds(string selectedGroupIDs)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@selectedGroupIDs", selectedGroupIDs));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, 
                GETGROUPTAXONOMYIDS_PROC_NAME,
                sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the predictive search taxonomy.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="searchString">The search string.</param>
        /// <returns></returns>
        public static DataSet GetPredictiveSearchTaxonomy(int taxonomyTypeID,string searchString)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@searchString", searchString));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                GETPREDICTIVESEARCHTAXONOMY_PROC_NAME,
                sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// GETs the country profiler sections N charts.
        /// </summary>
        /// <param name="countryID">The country ID.</param>
        /// <returns></returns>
        public static DataSet GETCountryProfilerSectionsNCharts(int countryID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@countryID", countryID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                "GETCountryProfilerSectionsNCharts",
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the data source information.
        /// </summary>
        /// <param name="rowID">The row ID.</param>
        /// <returns></returns>
        public static DataSet GetDataSourceInformation(int rowID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@rowID", rowID));

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETDATASOURCEINFORMATION_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the predefined tables list.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPredefinedTablesList()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETPREDEFINEDTABLESLIST_PROC_NAME);

            return ds;
        }

        /// <summary>
        /// Gets the predefined table data.
        /// </summary>
        /// <param name="tableID">The table ID.</param>
        /// <returns></returns>
        public static DataSet GetPredefinedTableData(int tableID)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@id", tableID));


            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETPREDEFINEDTABLEDATA_PROC_NAME,
                sqlParams.ToArray());

            return ds;
        }

        /// <summary>
        /// Gets the indicator methodology links.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetIndicatorMethodologyLinks()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                CommandType.StoredProcedure,
                GETINDICATORMETHODOLOGYLINKS_PROC_NAME);

            return ds;
        }
    }
}

