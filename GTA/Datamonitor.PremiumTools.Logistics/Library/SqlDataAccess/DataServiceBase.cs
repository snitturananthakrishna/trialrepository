using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Datamonitor.PremiumTools.Generic.Library.SqlDataAccess
{
    public class DataServiceBase
    {
        ////////////////////////
        /// Constructor
        ////////////////////////
        public DataServiceBase() { }

        public static string ConnectionString { get { return ConfigurationManager.ConnectionStrings["Datamonitor.GPT.Database"].ConnectionString; } }       
    }
}
