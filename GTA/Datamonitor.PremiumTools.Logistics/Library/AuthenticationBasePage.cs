using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// The base page for all pages that require the user to be autheticated successfully to access.
    /// </summary>
    public class AuthenticationBasePage : BasePage
    {       
        //PUBLIC PROPERTIES

        public string KCUserID
        {
            get { return Session["KCUserId"] == null ? null : Session["KCUserId"].ToString(); }
        }

        public string SourceKC
        {
            get { return Session["SourceKC"] == null ? null : Session["SourceKC"].ToString(); }
        }

        public bool IsSharedUser
        {
            get { return Session["IsSharedUser"] == null ? true : Convert.ToBoolean(Session["IsSharedUser"].ToString().ToLower()); }
        }

        public string KCUrl
        {
            get
            {
                string KCUrl = string.Empty;
                switch (SourceKC)
                {
                    case "retail":
                    case "auto":
                    case "bgcio":
                    case "consumer":
                    case "fs":
                    case "forecourt":
                    case "logistics":
                    case "news_and_deals":
                    case "pharma":
                    case "ps":
                    case "technology":
                    case "utilities":
                    case "sourcing":
                        KCUrl = GlobalSettings.KCRedirectUrl;
                        break;
                    /* Ovum branded sites.. */
                    case "ovumit":
                    case "vendor":
                    case "telecoms":
                    case "enterprise":
                        KCUrl = GlobalSettings.OvumKCRedirectUrl;
                        break;
                    default:
                        break;
                }
                return KCUrl;
            }
        }

        public string WordTemplatePath
        {
            get
            {
                return Server.MapPath(GlobalSettings.WordTemplatePath);
            }
        }

        public string PowerpointTempaltePath
        {
            get
            {
                return Server.MapPath(GlobalSettings.PowerpointTempaltePath);
            }
        }

        public string ExcelTemplatePath
        {
            get
            {
                return Server.MapPath(GlobalSettings.ExcelTemplatePath);
            }
        }

        public bool ShowWelcomeMsg
        {
            get
            {
                return ConfigurationManager.AppSettings["ShowWelcomeMsg"] != null && 
                    ConfigurationManager.AppSettings.Get("ShowWelcomeMsg").Equals("true");
            }
        }

        public bool ShowIndepthCountryPDFdownload
        {
            get
            {
                return ConfigurationManager.AppSettings["ShowIndepthCountryPDFdownload"] != null &&
                    ConfigurationManager.AppSettings.Get("ShowIndepthCountryPDFdownload").Equals("true",StringComparison.CurrentCultureIgnoreCase);
            }
        }

        public string LogoPath
        {
            get
            {
                string LPath = Server.MapPath(GlobalSettings.ExtractLogoPathForOvum);   
                if (!string.IsNullOrEmpty(SourceKC))
                {
                    switch (SourceKC.ToLower())
                    {
                        case "ovumit":
                        case "telecoms":
                        case "vendor":
                        case "enterprise":
                            LPath = Server.MapPath(GlobalSettings.ExtractLogoPathForOvum);   
                            break;
                    }
                }
                return LPath;
            }
        }

        // EVENT OVERRIDES

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);            

            //CHECK IF URL REFERRER IS ENABLED
            if (ConfigurationManager.AppSettings.Get("URLReferer_Enabled") != null && 
                    Convert.ToBoolean(ConfigurationManager.AppSettings.Get("URLReferer_Enabled")))
            {
                if (Session["HTTPRef_Authenticated"] != null && Session["HTTPRef_Authenticated"].ToString().Equals("true",StringComparison.CurrentCultureIgnoreCase))
                {
                    Session["KCUserId"] = "SCRIP100";
                    Session["SourceKC"] = ConfigurationManager.AppSettings.Get("defaultKC");
                    Session["IsSharedUser"] = "true";
                }
                else
                {                    
                   Response.Redirect(ConfigurationManager.AppSettings["URLReferer_RedirectURL"].ToString());
                }
            }
            else  if (GlobalSettings.IsSSOEnabled)
            {
                if (string.IsNullOrEmpty(SourceKC))
                {
                    string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                    Response.Redirect(strOvumUrl, true);
                    //Response.Redirect("http://www.datamonitor.com/kc/");
                }
            }
            else if (Session["KCUserId"] == null)
            {
                string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                Response.Redirect(strOvumUrl, true);
                //Session["KCUserId"] = "user1";
                //Session["SourceKC"] = System.Configuration.ConfigurationManager.AppSettings.Get("defaultKC");
                //Session["IsSharedUser"] = true;
                ////Server.Transfer("~/Error/Default.aspx?Code=SessionExpired");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

       
        //PUBLIC METHODS

        public void LogUsage(string Event, 
            string productID, 
            string additionalDetails)
        {
            if (!string.IsNullOrEmpty(KCUserID))
            {
                try
                {
                    SqlDataService.addUserLoggingDetails(KCUserID,
                        SourceKC,
                        Event,
                        productID,
                        additionalDetails,
                        HttpContext.Current.Request.UserHostAddress.ToString(),
                        "");
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
