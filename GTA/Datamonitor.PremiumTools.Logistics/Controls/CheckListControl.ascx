<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckListControl.ascx.cs" 
Inherits="Datamonitor.PremiumTools.Generic.Controls.CheckListControl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<style type="text/css">
.GridHeaderText
{
    text-align: left;
}
</style>

 <ComponentArt:Grid id="Grid1"         
    EnableViewState="false"
    CssClass="Compo_Grid"        
    ShowFooter="false"        
    PagerStyle="Numbered"
    ShowHeader="true"
    ShowSearchBox="true"         
    SearchTextCssClass="GridHeaderText"
    SearchOnKeyPress="true"        
    RunningMode="Client"
    GroupingNotificationText = ""
    PageSize="12" 
    ImagesBaseUrl="images/"        
    runat="server" >
    <Levels>
      <ComponentArt:GridLevel ShowHeadingCells="false">
        <Columns>
          <ComponentArt:GridColumn DataField="Name"  DataCellServerTemplateId="TaxonomyNameTemplate" Width="400" /> 
          <ComponentArt:GridColumn DataField="ID" Visible="false" ColumnType="Default" /> 
          <ComponentArt:GridColumn DataField="CheckStatus" Visible="false" ColumnType="Default" /> 
        </Columns>            
      </ComponentArt:GridLevel>
    </Levels>
    <ServerTemplates>
      <ComponentArt:GridServerTemplate Id="TaxonomyNameTemplate" runat="server">
        <Template>
          <input type="checkbox" <%# Container.DataItem["CheckStatus"] %>
           class="chk<%# Container.DataItem["ID"] %>" onclick="javascript:ListitemCheckChange(this,escape('<%# TaxonomyType %>'),
          '<%# FullTaxonomyTypeID %>','<%# Container.DataItem["ID"] %>',escape('<%# Container.DataItem["name"] %>'));" /><%# Container.DataItem["Name"] %>
          
        </Template>
      </ComponentArt:GridServerTemplate>
    </ServerTemplates>
  </ComponentArt:Grid>
