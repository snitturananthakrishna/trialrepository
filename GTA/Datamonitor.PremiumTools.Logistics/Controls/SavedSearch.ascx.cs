using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class SavedSearch : System.Web.UI.UserControl
    {
        private static string _redirectUrl;

        protected void Page_Load(object sender, EventArgs e)
        {            
            if(!IsPostBack)
            {
                //ddlSavedSearches.Attributes.Add("onchange", "javascript:search_onChange();");               
            }
           
        }
        
        public void LoadSavedSearches(string redirectUrl)
        {
            _redirectUrl = redirectUrl;
            //Bind saved searches               
            BindSavedSearches();
        }

        private void BindSavedSearches()
        {
            //Get the KCuser details from the parent page
            bool IsSharedUser=((AuthenticationBasePage)Page).IsSharedUser;
            string KCUserID = ((AuthenticationBasePage)Page).KCUserID;

            if (!IsSharedUser)
            {
                //Load saved searches belongs to current user
                DataSet savedSearchesDataset = new DataSet();
                ddlSavedSearches.Items.Clear();
                string strUserName = KCUserID;
                savedSearchesDataset = SqlDataService.GetSavedSearches(strUserName);
                ddlSavedSearches.Items.Add(new ListItem("My Saved Searches", "-4"));
                ddlSavedSearches.Items.Add(new ListItem("----------------------------", "-3"));
                if (savedSearchesDataset.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in savedSearchesDataset.Tables[0].Rows)
                    {
                        ddlSavedSearches.Items.Add(new ListItem(dr["searchName"].ToString(), dr["SearchId"].ToString()));
                    }
                    ddlSavedSearches.Items.Add(new ListItem("----------------------------", "-2"));
                    ddlSavedSearches.Items.Add(new ListItem("Manage Saved Searches", "-1"));
                }
            }
            else
            {
                //Make saved searches dropdown invisible
                ddlSavedSearches.Visible = false;
            }
        }

        protected void ddlSavedSearches_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet savedSearchesDataset = new DataSet();
            string savedSearchXml = string.Empty;
            int searchId = Convert.ToInt32(ddlSavedSearches.SelectedValue.Split('~')[0]);
            int j = 0;
            string actionType = "update";

            if (searchId > 0)
            {
                savedSearchesDataset = SqlDataService.GetSavedSearchSearchCriteria(searchId);
                if (savedSearchesDataset.Tables.Count > 0 && savedSearchesDataset.Tables[0].Rows.Count > 0)
                {
                    savedSearchXml = savedSearchesDataset.Tables[0].Rows[0]["searchXml"].ToString();
                }
                XmlDocument savedSearchDocument = new XmlDocument();
                savedSearchDocument.LoadXml(savedSearchXml);

                XmlElement root = savedSearchDocument.DocumentElement;
                XmlNodeList xmlNodes = root.SelectNodes("/selections/taxonomy");

                //clear the session
                CurrentSession.ClearSelectionsFromSession();
                
                //Store the current saved search details into session
                foreach (XmlNode node in xmlNodes)
                {
                    string taxonomyTypeID = node.Attributes["id"].Value;
                    string taxonomyID = node.Attributes["selectIDs"].Value;
                    string parentTaxonomyID = node.Attributes["parentSelectionIDs"].Value;
                    string columnID = node.Attributes["columnID"].Value;
                    string taxonomyIDText = node.Attributes["selectIDsText"].Value;
                    string parentTaxonomyIDText = node.Attributes["parentSelectIDsText"].Value;                   

                    //add taxonomy details to session
                    if (taxonomyID.Trim().Length > 0)
                    {
                        string[] taxonomyIDs = taxonomyID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] taxonomyIDsTexts = taxonomyIDText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        if (taxonomyIDs.Length > 1)
                        {
                            for (int i = 0; i < taxonomyIDs.Length; i++)
                            {
                                string name = taxonomyTypeID + "~" + taxonomyIDs[i] + "~" + taxonomyIDsTexts[i];
                                SessionHandler.savedSearchCriteria(taxonomyID, actionType, columnID, name, i + j);
                            }
                        }
                        else
                        {
                            string name = taxonomyTypeID + "~" + taxonomyID + "~" + taxonomyIDText;
                            SessionHandler.savedSearchCriteria(taxonomyID, actionType, columnID, name, j);
                            j = j + 1;
                        }
                    }

                    //add parent taxonomy details to session
                    if (parentTaxonomyID.Trim().Length > 0)
                    {
                        string[] parentTaxonomyIDs = parentTaxonomyID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] parentTaxonomyIDsTexts = parentTaxonomyIDText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        if (parentTaxonomyIDs.Length > 1)
                        {
                            for (int i = 0; i < parentTaxonomyIDs.Length; i++)
                            {
                                string name = taxonomyTypeID + "~" + parentTaxonomyIDs[i] + "~" + parentTaxonomyIDsTexts[i];
                                SessionHandler.savedSearchCriteria(taxonomyID, actionType, columnID, name, i + j);
                            }
                        }
                        else
                        {
                            string name = taxonomyTypeID + "~" + parentTaxonomyID + "~" + parentTaxonomyIDText;
                            SessionHandler.savedSearchCriteria(taxonomyID, actionType, columnID, name, j);
                            j = j + 1;
                        }
                    }
                }
            }

            //string strRedirectPage = GlobalSettings.DefaultResultsPage;
            if (Convert.ToInt32(ddlSavedSearches.SelectedValue.Split('~')[0]) > 0)
            {
                Response.Redirect(_redirectUrl + "?SearchId=" + Convert.ToInt32(ddlSavedSearches.SelectedValue.Split('~')[0]));
            }
            else if (ddlSavedSearches.SelectedValue == "-1")
            {
                string ManagePageUrl =Request.Url.ToString().ToLower().Contains("results") ? "ManageSavedSearches.aspx" : "Results/ManageSavedSearches.aspx";
                Response.Redirect(ManagePageUrl, true);
            }
            else if (Convert.ToInt32(ddlSavedSearches.SelectedValue) < -1)
            {
                //Response.Redirect(_redirectUrl);
                string ManagePageUrl = Request.Url.ToString();
                Response.Redirect(ManagePageUrl, true);
            }
        }    

    }
}