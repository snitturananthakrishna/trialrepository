using System;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public class BaseSelectionControl : System.Web.UI.UserControl
    {
        // PRIVATE FIELDS
        private string _taxonomyType;
        private int _taxonomyTypeID;
        private string _source;
        private string _fullTaxonomyTypeID;
        private string _parentIDs;
        private bool _showTaxonomyDefinition;
        private int _controlWidth;
        private int _controlHeight;
        private string _expandNodeID;
        private bool _flagChecked;
        private string _flagCheckboxText;

        // PUBLIC PROPERTIES

        /// <summary>
        /// Gets or sets the type of the taxonomy.
        /// </summary>
        /// <value>The type of the taxonomy.</value>
        public string TaxonomyType
        {
            get { return _taxonomyType; }
            set { _taxonomyType = value; }
        }


        /// <summary>
        /// Gets or sets the full taxonomy ID.
        /// </summary>
        /// <value>The full taxonomy ID.</value>
        public string FullTaxonomyTypeID
        {
            get { return _fullTaxonomyTypeID; }
            set { _fullTaxonomyTypeID = value; }
        }

        /// <summary>
        /// Gets or sets the parent Ids.
        /// </summary>
        /// <value>The parent Ids.</value>
        public string ParentIDs
        {
            get { return _parentIDs; }
            set { _parentIDs = value; }
        }  

        /// <summary>
        /// Gets or sets the taxonomy type ID.
        /// </summary>
        /// <value>The taxonomy type ID.</value>
        public int TaxonomyTypeID
        {
            get { return _taxonomyTypeID; }
            set { _taxonomyTypeID = value; }
        }

        /// <summary>
        /// Gets or sets the width of the control.
        /// </summary>
        /// <value>The width of the control.</value>
        public int ControlWidth
        {
            get { return _controlWidth; }
            set { _controlWidth = value; }
        }

        /// <summary>
        /// Gets or sets the height of the control.
        /// </summary>
        /// <value>The height of the control.</value>
        public int ControlHeight
        {
            get { return _controlHeight; }
            set { _controlHeight = value; }
        }

        /// <summary>
        /// Gets or sets the expand node ID.
        /// </summary>
        /// <value>The expand node ID.</value>
        public string ExpandNodeID
        {
            get { return _expandNodeID; }
            set { _expandNodeID = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [flag checked].
        /// </summary>
        /// <value><c>true</c> if [flag checked]; otherwise, <c>false</c>.</value>
        public bool FlagChecked
        {
            get { return _flagChecked; }
            set { _flagChecked = value; }
        }

        /// <summary>
        /// Gets or sets the flag checkbox text.
        /// </summary>
        /// <value>The flag checkbox text.</value>
        public string FlagCheckboxText
        {
            get { return _flagCheckboxText; }
            set { _flagCheckboxText = value; }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public virtual void LoadData()
        {

        }

        /// <summary>
        /// Gets or sets the source of event.
        /// </summary>
        /// <value>The source of event.</value>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        /// <summary>
        /// Gets or sets the value to show taxonomy definition or not.
        /// </summary>
        /// <value>The value to show taxonomy definition or not.</value>
        public bool ShowTaxonomyDefinition
        {
            get { return _showTaxonomyDefinition; }
            set { _showTaxonomyDefinition = value; }
        }

        /// <summary>
        /// Gets the filter criteria from session.
        /// </summary>
        /// <returns></returns>
        protected static string GetFilterCriteriaFromSession()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        List<int> keys = new List<int>(selectedIDsList.Keys);
                        //convert int array to string array
                        string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                            taxonomyRow["ID"].ToString(),
                            string.Join(",", SelectedIDs),
                            taxonomyRow["ColumnRowID"].ToString());

                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }
            return SelectionsXML;
        }
    }
}
