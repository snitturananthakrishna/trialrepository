<%@ Control Language="C#" 
        AutoEventWireup="true" 
        CodeBehind="ChartFilters.ascx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Controls.ChartFilters" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<style type="text/css">
.rightdiv
{
float:right;
margin-bottom:4px;
}
</style>
<script type="text/javascript">
function SingleFilterTaxonomyView1_onNodeSelect(sender, eventArgs)
{       
    SingleFilterBox1.set_text(eventArgs.get_node().get_text());
    SingleFilterBox1.collapse();
    document.getElementById('<%=Taxonomy1Selection.ClientID %>').value = eventArgs.get_node().get_value();   
}
function SingleFilterTaxonomyView2_onNodeSelect(sender, eventArgs)
{       
    SingleFilterBox2.set_text(eventArgs.get_node().get_text());
    SingleFilterBox2.collapse();
    document.getElementById('<%=Taxonomy2Selection.ClientID %>').value = eventArgs.get_node().get_value();   
}
function SingleFilterTaxonomyView3_onNodeSelect(sender, eventArgs)
{       
    SingleFilterBox3.set_text(eventArgs.get_node().get_text());
    SingleFilterBox3.collapse();
    document.getElementById('<%=Taxonomy3Selection.ClientID %>').value = eventArgs.get_node().get_value();   
}
function SingleFilterTaxonomyView10_onNodeSelect(sender, eventArgs)
{       
    SingleFilterBox10.set_text(eventArgs.get_node().get_text());
    SingleFilterBox10.collapse();
    document.getElementById('<%=Taxonomy10Selection.ClientID %>').value = eventArgs.get_node().get_value();   
}
//function ChartTypes_onNodeSelect(sender, eventArgs)
//{ 
//    ChartTypesDropDown.set_text(eventArgs.get_node().get_text());
//    ChartTypesDropDown.collapse();
//    document.getElementById('<%=ChartType.ClientID %>').value = eventArgs.get_node().get_value();   
//}

function Years_onChange()
{
    var yearsDropdown = document.getElementById('<%=FilterBoxYears.ClientID %>');   
    document.getElementById('<%=Taxonomy999Selection.ClientID %>').value = yearsDropdown.value;
}

function MultipleFilterTaxonomyView1_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%=Taxonomy1Selection.ClientID %>');
    if (eventArgs.get_node().get_checked()) 
    {
        hdnControl.value = hdnControl.value  + eventArgs.get_node().get_value()+ ",";
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(eventArgs.get_node().get_value()+",","");
    }
}
function MultipleFilterTaxonomyView2_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%=Taxonomy2Selection.ClientID %>');
    if (eventArgs.get_node().get_checked()) 
    {
        hdnControl.value = hdnControl.value + eventArgs.get_node().get_value()+",";
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(eventArgs.get_node().get_value()+",","");
    }
}
function MultipleFilterTaxonomyView3_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%=Taxonomy3Selection.ClientID %>');
    if (eventArgs.get_node().get_checked()) 
    {
        hdnControl.value = hdnControl.value + eventArgs.get_node().get_value()+",";
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(eventArgs.get_node().get_value()+",","");
    }
}
function MultipleFilterTaxonomyView10_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%=Taxonomy10Selection.ClientID %>');
    if (eventArgs.get_node().get_checked()) 
    {
        hdnControl.value = hdnControl.value + eventArgs.get_node().get_value()+",";
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(eventArgs.get_node().get_value()+",","");
    }
}

</script>
<h1>FILTER OPTIONS</h1>
    <div id="FilterDiv1">
        <div id="SingleFilterDiv1" style="float:right;margin-bottom:4px;">   
            <ComponentArt:ComboBox id="SingleFilterBox1" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="120"
            DropDownHeight="297"
            DropDownWidth="220" >
          <DropdownContent>
              <ComponentArt:TreeView id="SingleFilterTaxonomyView1" Height="297" Width="220"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
                <NodeSelect EventHandler="SingleFilterTaxonomyView1_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
        </div>
        <div id="MultipleFilterDiv1" style="float:right;margin-bottom:4px;">   
            <ComponentArt:ComboBox id="MultipleFilterBox1" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="120"
            DropDownHeight="297"
            DropDownWidth="220" 
                      Text="--Multiple--">
          <DropdownContent>
              <ComponentArt:TreeView id="MultipleFilterTaxonomyView1" 
              Height="297" Width="220"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="powersearch_tree"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"                
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"                                
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="false"
                EnableViewState="false"  
                    FillContainer="true" 
                runat="server" >
              <ClientEvents>
                <NodeCheckChange EventHandler="MultipleFilterTaxonomyView1_onNodeCheckChange" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
        </div>
        <div style="width:60px;">Country:</div>
    </div>
    <div id="FilterDiv2">
        <div id="SingleFilterDiv2" style="float:right;margin-bottom:4px;">   
            <ComponentArt:ComboBox id="SingleFilterBox2" runat="server"
              KeyboardEnabled="false"
              AutoFilter="false"
              AutoHighlight="false"
              AutoComplete="false"
              CssClass="comboBox"                                            
              TextBoxCssClass="comboTextBox"
              DropDownCssClass="comboDropDown"
              ItemCssClass="comboItem"                      
              SelectedItemCssClass="comboItemHover"
              DropHoverImageUrl="../assets/images/drop_hover.gif"
              DropImageUrl="../assets/images/drop.gif"
              Width="120"
              DropDownHeight="160"
              DropDownWidth="120"
              Visible="true"
              >
              <DropdownContent>
                  <ComponentArt:TreeView id="SingleFilterTaxonomyView2" Height="160" Width="120"
                    DragAndDropEnabled="false"
                    NodeEditingEnabled="false"
                    KeyboardEnabled="true"
                    CssClass="TreeView"
                    NodeCssClass="TreeNode"                                                        
                    NodeEditCssClass="NodeEdit"
                    SelectedNodeCssClass="NodeSelected"
                    LineImageWidth="19"
                    LineImageHeight="20"
                    DefaultImageWidth="16"
                    DefaultImageHeight="16"
                    ItemSpacing="0"
                    NodeLabelPadding="3"
                    ImagesBaseUrl="../assets/tvlines/"
                    LineImagesFolderUrl="../assets/images/tvlines/"
                    ShowLines="false"
                    EnableViewState="false"                           
                    runat="server" >
                  <ClientEvents>
                    <NodeSelect EventHandler="SingleFilterTaxonomyView2_onNodeSelect" />
                  </ClientEvents>
                  </ComponentArt:TreeView>
              </DropdownContent>
            </ComponentArt:ComboBox>
        </div>
         <div id="MultipleFilterDiv2" style="float:right;margin-bottom:4px;">   
            <ComponentArt:ComboBox id="MultipleFilterBox2" runat="server"
              KeyboardEnabled="false"
              AutoFilter="false"
              AutoHighlight="false"
              AutoComplete="false"
              CssClass="comboBox"                                            
              TextBoxCssClass="comboTextBox"
              DropDownCssClass="comboDropDown"
              ItemCssClass="comboItem"                      
              SelectedItemCssClass="comboItemHover"
              DropHoverImageUrl="../assets/images/drop_hover.gif"
              DropImageUrl="../assets/images/drop.gif"
              Width="120"
              DropDownHeight="160"
              DropDownWidth="120"
              Visible="true"
              Text="--Multiple--"
              >
              <DropdownContent>
                  <ComponentArt:TreeView id="MultipleFilterTaxonomyView2" Height="160" Width="120"
                    DragAndDropEnabled="false"
                    NodeEditingEnabled="false"
                    KeyboardEnabled="true"
                    CssClass="TreeView"
                    NodeCssClass="TreeNode"                                                        
                    NodeEditCssClass="NodeEdit"
                    SelectedNodeCssClass="NodeSelected"
                    LineImageWidth="0"
                    LineImageHeight="20"
                    DefaultImageWidth="0"
                    DefaultImageHeight="16"
                    ItemSpacing="0"
                    NodeLabelPadding="3"
                    ImagesBaseUrl="../assets/tvlines/"
                    LineImagesFolderUrl="../assets/images/tvlines/"
                    ShowLines="false"
                    EnableViewState="false"                           
                    runat="server"
                     >
                  <ClientEvents>
                    <NodeCheckChange EventHandler="MultipleFilterTaxonomyView2_onNodeCheckChange" />
                  </ClientEvents>
                  </ComponentArt:TreeView>
              </DropdownContent>
            </ComponentArt:ComboBox>
        </div>
        <div style="width:60px;">Mode:</div>
    </div> 
    <div id="FilterDiv3"> 
        <div id="SingleFilterDiv3" style="float:right;margin-bottom:4px;">   
           <ComponentArt:ComboBox id="SingleFilterBox3" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="297"
                      DropDownWidth="220"
                      Visible="true"
                      >
                      <DropdownContent>
                          <ComponentArt:TreeView id="SingleFilterTaxonomyView3" Height="297" Width="220"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="false"                           
                            runat="server" >
                          <ClientEvents>
                            <NodeSelect EventHandler="SingleFilterTaxonomyView3_onNodeSelect" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                      </DropdownContent>
            </ComponentArt:ComboBox>         
           </div>
        <div id="MultipleFilterDiv3" style="float:right;margin-bottom:4px;">   
           <ComponentArt:ComboBox id="MultipleFilterBox3" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="297"
                      DropDownWidth="220"
                      Visible="true"
                      Text="--Multiple--"
                      >
                      <DropdownContent>
                          <ComponentArt:TreeView id="MultipleFilterTaxonomyView3" Height="297" Width="220"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="false"                           
                            runat="server" >
                          <ClientEvents>
                            <NodeCheckChange EventHandler="MultipleFilterTaxonomyView3_onNodeCheckChange" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                      </DropdownContent>
            </ComponentArt:ComboBox>         
           </div>
           <div style="width:60px;">Market:</div>
    </div>
    <div id="FilterDiv10">
        <div id="SingleFilterDiv10" style="float:right;margin-bottom:4px;">   
        <ComponentArt:ComboBox id="SingleFilterBox10" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="97"
                      DropDownWidth="120"
                      Visible="true"
                      >
                      <DropdownContent>
                          <ComponentArt:TreeView id="SingleFilterTaxonomyView10" Height="97" Width="120"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="false"
                            EnableViewState="false"                           
                            runat="server" >
                          <ClientEvents>
                            <NodeSelect EventHandler="SingleFilterTaxonomyView10_onNodeSelect" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                      </DropdownContent>
            </ComponentArt:ComboBox>
        </div>
         <div id="MultipleFilterDiv10" style="float:right;margin-bottom:4px;">   
        <ComponentArt:ComboBox id="MultipleFilterBox10" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="97"
                      DropDownWidth="120"
                      Visible="true"
                      Text="--Multiple--"
                      >
                      <DropdownContent>
                          <ComponentArt:TreeView id="MultipleFilterTaxonomyView10" Height="97" Width="120"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="false"
                            EnableViewState="false"                           
                            runat="server" >
                          <ClientEvents>
                            <NodeCheckChange EventHandler="MultipleFilterTaxonomyView10_onNodeCheckChange" />                            
                          </ClientEvents>
                          </ComponentArt:TreeView>
                      </DropdownContent>
            </ComponentArt:ComboBox>
        </div>
        <div style="width:60px;">Destination: </div>
    </div>
    <div id="FilterDiv999">
        <div id="SingleFilterDiv999" style="float:right;margin-bottom:4px;">   
            <asp:DropDownList ID="FilterBoxYears" runat="server" 
                Width="120"
                Font-Size="11px">
            </asp:DropDownList>
        </div>
        <div id="MultipleFilterDiv999" style="float:right;margin-bottom:4px;">   
            <asp:CheckBoxList ID="MultipleFilterBoxYears" runat="server" 
                Width="120"
                Font-Size="11px">
            </asp:CheckBoxList>
        </div>
        <div style="width:60px;">Year: </div>
    </div>
   <%-- <div id="FilterChartType">
                <div style="float:right;margin-bottom:4px;">    
                <ComponentArt:ComboBox ID="ChartTypesDropDown" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="120"
                      DropDownHeight="220"
                      DropDownWidth="140"
                      Visible="true">
                    <DropDownContent>
                         <ComponentArt:TreeView id="TreeView1" Height="220" Width="140"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="16"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="false"
                            SiteMapXmlFile="~/Config/ChartType.xml"
                            runat="server" >
                          <ClientEvents>
                            <NodeSelect EventHandler="ChartTypes_onNodeSelect" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                    </DropDownContent>
                </ComponentArt:ComboBox>          
                </div>
                <div style="width:60px;">Chart&nbsp;Type: </div>
                
            </div>--%>
     
<asp:HiddenField ID="Taxonomy1Selection" runat="server" />
<asp:HiddenField ID="Taxonomy2Selection" runat="server" />
<asp:HiddenField ID="Taxonomy3Selection" runat="server" />
<asp:HiddenField ID="Taxonomy10Selection" runat="server" />
<asp:HiddenField ID="Taxonomy999Selection" runat="server" />
<asp:HiddenField ID="ChartType" runat="server" />