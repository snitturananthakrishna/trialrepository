<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="SavedSearch.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.SavedSearch" %>

<script type="text/javascript">
    function search_onChange()
    {       
        var dropdown=$("#<%= ddlSavedSearches.ClientID%>");
        var searchid= $("#<%= ddlSavedSearches.ClientID%> option:selected").val();
        if(searchid > -2)
        {
            __doPostBack(dropdown, '');
        }
    }
   
</script>


<asp:DropDownList 
    ID="ddlSavedSearches" 
    runat="server" 
    CssClass="formstyle5" 
    OnSelectedIndexChanged="ddlSavedSearches_SelectedIndexChanged" AutoPostBack="true">
    </asp:DropDownList>