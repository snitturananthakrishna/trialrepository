<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="AskAnAnalyst.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Logistics.Controls.AskAnAnalyst" %>

<a href="#" class="ask" 
    style="font-size:10px; font-family:verdana; font-weight:lighter;" 
    onclick="return hs.htmlExpand(this, { contentId: 'highslide-ask' } )">Ask an analyst a question</a>