<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="CountrySnapshotOptions.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.CountrySnapshotOptions" %>
    
<div id="SearchOtionsDiv" runat="server">
    <div id="database_tabs_bar">
        <ul>
            <li class="title">View Options</li>  
            <li id="CountryBrowserLI" runat="server">
                <asp:LinkButton ID="CountryBrowserLink" runat="server" 
                    OnClick="CountryBrowser_Click" 
                    Text="Country Browser"
                    ToolTip="Country Browserh"></asp:LinkButton>
                
            </li>           
            <li id="CountryProfilerLI" runat="server">
                <asp:LinkButton ID="CountryProfilerLink" runat="server" 
                    OnClick="CountryProfiler_Click" 
                    Text="Country Profiler"
                    ToolTip="Country Profiler"></asp:LinkButton>
                
            </li>            
           
        </ul>
    </div>
    <br /><br />
</div>