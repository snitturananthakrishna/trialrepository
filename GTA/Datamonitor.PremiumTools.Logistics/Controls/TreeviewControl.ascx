<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TreeviewControl.ascx.cs" Inherits="Datamonitor.PremiumTools.Generic.Controls.TreeviewControl" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<h2>
    <asp:Label ID="lblSearchTitle" runat="server"></asp:Label>
</h2>
<script type="text/javascript">	
function GetTreeView() {
         return <%= tvForAll.ClientObjectId %>;
    }
    
    </script>
     <div style="padding-bottom:7px">
        <asp:CheckBox Width="400" ID="FlagCheckBox" runat="server" Text="&nbsp;Show in alphabetic order" />
    </div>
<ComponentArt:TreeView  ID="tvForAll" runat="server"   EnableViewState="true"      
                    HoverPopupEnabled="true" 
                    HoverPopupNodeCssClass="NodePopup" 
                    FillContainer="true" 
                    Width="410px"
                    Height="340px" 
                    DragAndDropEnabled="false"
                    NodeEditingEnabled="false" 
                    KeyboardEnabled="true" 
                    AutoScroll="true" 
                    HoverNodeCssClass="HoverTreeNode"
                    ShowLines="true" 
                    LineImageWidth="19" 
                    LineImageHeight="20"                     
                    DefaultImageWidth="16"
                    DefaultImageHeight="16"  
                    CausesValidation="false"
                    ItemSpacing="0"  
                    ExpandNodeOnSelect="false"
                    CollapseNodeOnSelect="false"
                    NodeLabelPadding="3" 
                    LineImagesFolderUrl="~/assets/images/tvlines" 
                    ImagesBaseUrl="~/assets/images/tvlines"
                    ClientSideOnNodeCheckChanged="tvForAll_onNodeCheckChange"    
                    CssClass="powersearch_tree">

                    <ClientTemplates>
                        <ComponentArt:ClientTemplate id="NT">
                              <div>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="DT">
                            <div>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;" 
                                href="TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetProperty('ID') ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                <img alt="View Definition" border="0" src="Assets/Images/help2.gif" height="10px" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="NTCheckAll">
                              <div><a href="javascript:checkAllSubNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img border="0" id="Img3" src= "Assets/Images/checkall8.gif" /></a>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="DTCheckAll">
                            <div><a href="javascript:checkAllSubNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img border="0" id="Img1" src= "Assets/Images/checkall8.gif" /></a>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;" 
                                href="TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetProperty('ID') ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                <img alt="View Definition" border="0" src="Assets/Images/help2.gif" height="10px" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="NTUnCheckAll">
                              <div><a href="javascript:unCheckAllSubNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img border="0" id="Img7" src= "Assets/Images/uncheckall8.gif" /></a>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="DTUnCheckAll">
                            <div><a href="javascript:unCheckAllSubNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img border="0" id="Img2" src= "Assets/Images/uncheckall8.gif" /></a>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;" 
                                href="TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetProperty('ID') ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                <img alt="View Definition" border="0" src="Assets/Images/help2.gif" height="10px" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="NTPnSel">
                              <div><a href="javascript:CheckThisNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img alt="Click to get the Total value" border="0" id="Img4" src= "Assets/Images/pnsel_old.gif" /></a>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="DTPnSel">
                            <div><a href="javascript:CheckThisNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img alt="Click to get the Total value" border="0" id="Img5" src= "Assets/Images/pnsel_old.gif" /></a>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;" 
                                href="TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetProperty('ID') ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                <img alt="View Definition" border="0" src="Assets/Images/help2.gif" height="10px" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="NTPn">
                              <div><a href="javascript:unCheckThisNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img alt="" border="0" id="Img6" src= "Assets/Images/pn_old.gif" /></a>## DataItem.get_text(); ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate id="DTPn">
                            <div><a href="javascript:unCheckThisNode(## DataItem.get_id(); ##)" style="margin-right:2px;" ><img alt="" border="0" id="Img8" src= "Assets/Images/pn_old.gif" /></a>## DataItem.get_text(); ## 
                            <a style="color: Red; text-decoration: none;" 
                                href="TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetProperty('ID') ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                <img alt="View Definition" border="0" src="Assets/Images/help2.gif" height="10px" />
                            </a> 
                            </div>
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:TreeView>


<div class="highslide-html-content" id="TaxonomyDefinition" style="width:280px;height:200px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Taxonomy Definition</h1>
    </div>
    <div class="highslide-body" style="padding:0px;margin:0px">
    </div>
    &nbsp;
</div>
