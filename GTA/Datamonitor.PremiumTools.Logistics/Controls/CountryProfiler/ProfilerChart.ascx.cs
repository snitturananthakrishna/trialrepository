using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class ProfilerChart : System.Web.UI.UserControl
    {
        // PRIVATE FIELDS
        private string _chartID;
        private string _countryID;
        private string _countryName;
        private string _countryIDsTOCompare;

        // PUBLIC PROPERTIES
        /// <summary>
        /// Gets or sets the chart ID.
        /// </summary>
        /// <value>The chart ID.</value>
        public string ChartID
        {
            get { return _chartID; }
            set { _chartID = value; }
        }
        /// <summary>
        /// Gets or sets the country ID.
        /// </summary>
        /// <value>The country ID.</value>
        public string CountryID
        {
            get { return _countryID; }
            set { _countryID = value; }
        }
        /// <summary>
        /// Gets or sets the name of the country.
        /// </summary>
        /// <value>The name of the country.</value>
        public string CountryName
        {
            get { return _countryName; }
            set { _countryName = value; }
        }
        /// <summary>
        /// Gets or sets the country Ids TO compare.
        /// </summary>
        /// <value>The country ids TO compare.</value>
        public string CountryIDsTOCompare
        {
            get { return _countryIDsTOCompare; }
            set { _countryIDsTOCompare = value; }
        }
        

        //EVENT OVERRIDES
        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                BuildDefaultChart();
            }
        }

        //EVENT HANLDERS
        public void ChartCallBack_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string chartID = e.Parameters[0].ToString();
            string countryID = e.Parameters[1].ToString();
            string countryName = e.Parameters[2].ToString();
            string countryIDsTOCompare = e.Parameters[3].ToString();

            dotnetCHARTING.Chart targetChart = CountryProfilerHelper.BuildCountryProfilerChart(chartID,
                  countryID,
                  countryName,
                  countryIDsTOCompare);

            string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

            //string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());

            ChartImage.ImageUrl = ChartImage.ResolveUrl(strChartPath).Replace("/Controls/", "/");
            ChartImage.Attributes.Add("usemap", targetChart.ImageMapName);
            ImageMapLabel.Text = targetChart.ImageMapText;
            ChartImage.RenderControl(e.Output);
            ImageMapLabel.RenderControl(e.Output);

        }

        //PRIVATE METHODS
        private void BuildDefaultChart()
        {
            StringBuilder Selections = new StringBuilder();

            Selections.Append("<script language='javascript' type='text/javascript'>");
            Selections.Append(string.Format("LoadDefaultChart('{0}','{1}','{2}','{3}');",
                                ChartID,
                                CountryID,
                                CountryName,
                                CountryIDsTOCompare));
            Selections.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadDefaultChart", Selections.ToString(), false);

        }

       //PUBLIC METHODS
        public void BuildChart()
        {           
            dotnetCHARTING.Chart targetChart = CountryProfilerHelper.BuildCountryProfilerChart(ChartID,
                  CountryID,
                  CountryName,
                  CountryIDsTOCompare);

            string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");            

            ChartImage.ImageUrl = ChartImage.ResolveUrl(strChartPath).Replace("/Controls/", "/");
            ChartImage.Attributes.Add("usemap", targetChart.ImageMapName);
            ImageMapLabel.Text = targetChart.ImageMapText;
            //ChartImage.RenderControl(e.Output);
            //ImageMapLabel.RenderControl(e.Output);
        }
    }
}