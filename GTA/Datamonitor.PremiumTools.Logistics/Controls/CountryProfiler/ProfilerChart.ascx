<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfilerChart.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.ProfilerChart" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Assembly="dotnetCHARTING" Namespace="dotnetCHARTING" TagPrefix="dotnetCHARTING" %>
<script type="text/javascript">
function LoadDefaultChart( ChartID, CountryID, CountryName, CountryIDsTOCompare)
{
    ChartCallbackProfiler.callback(ChartID, CountryID, CountryName, CountryIDsTOCompare);
}
</script>
<ComponentArt:CallBack id="ChartCallbackProfiler" runat="server"  OnCallback="ChartCallBack_Callback" >	
<Content>
    <div>
        <asp:Image id="ChartImage" ImageUrl="~/assets/images/No Data.GIF.jpg"  runat="server" />
        <asp:Label id="ImageMapLabel" runat="server"/>
    </div>
</Content>
  <LoadingPanelClientTemplate>
      <table width="400" style="height:300" cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td align="center">
        <table cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td style="font-size:10px;">Loading...&nbsp;</td>
          <td><img src="../assets/images/spinner.gif" width="16" height="16" alt="" /></td>
        </tr>
        </table>
        </td>
      </tr>
      </table>
</LoadingPanelClientTemplate>  
</ComponentArt:CallBack>
<asp:HiddenField ID="hdnChartID" runat="server" />
<asp:HiddenField ID="hdnCountryID" runat="server" />
<asp:HiddenField ID="hdnCountryName" runat="server" />
<asp:HiddenField ID="hdnCountryIDsTOCompare" runat="server" />


