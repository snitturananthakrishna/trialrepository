using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Logistics.Controls
{
    public partial class CountryFilter : System.Web.UI.UserControl
    {
        private string _taxonomyType;

        // EVENT OVERRIDES
        public string TaxonomyFilterPage
        {
            get
            {
                return GlobalSettings.SearchPage.ToLower().Contains("search1") ? "TaxonomyFilter1.aspx" : "TaxonomyFilter.aspx";
            }
        }

        public string ShowCountryFilterLink
        {
            get
            {
                return GlobalSettings.ShowCountryFilterLink ? "block" : "none";
            }
        }

        public string TaxonomyType
        {
            get
            {
                return string.IsNullOrEmpty(_taxonomyType) ? "Country" : _taxonomyType;
            }
            set
            {
                _taxonomyType = value;
            }
        }

    }
}