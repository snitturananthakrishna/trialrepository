<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartLinks.ascx.cs" Inherits="Datamonitor.PremiumTools.Generic.Controls.ChartLinks" %>

<script type="text/javascript" language="javascript" src="../Assets/Scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" >
$(document).ready(function()
{
	//slides the element with class "menu_body" when paragraph with class "menu_head" is clicked 
	$("#firstpane p.menu_head").click(Groupclick);			
});
function Groupclick()
    {
		$(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
       	$(this).siblings().css({backgroundImage:"url(left.png)"});
	}
	function toggleOnPageLoad()
	{
	    var LItag=$("LI.selected");	    
	    LItag.parent().parent().show();
	}
</script>

<style type="text/css">
.hiddenLI
{
    visibility:hidden;
}

/*CSS Code for accordion menu using jQuery */
.menu_list {
	width: 150px;
}
.menu_head {
	padding: 5px 2px;
	cursor: pointer;
	position: relative;
	margin-top:1px;
    font-weight:bold;
    background: #D2D9E5;
}
.menu_body {
	display:none;
}
.menu_body a {
  display:block;  
  padding-left:10px;
  font-weight:bold;
  text-decoration:none;
}
.menu_body a:hover {
  color: #000000;
  text-decoration:underline;
}
</style>

<h2>Basic Charting</h2>
<ul>    
    <li class="<%=GetLinkStyle("CountryComparison.aspx")%>" ><a href="countrycomparison.aspx">Country/Region Comparison</a></li>
    <li class="<%=GetLinkStyle("IndicatorComparison.aspx")%>" ><a href="../Analysis/IndicatorComparison.aspx">Indicator Comparison</a></li>
    <li class="<%=GetLinkStyle("CustomChart.aspx")%>" ><a href="../Analysis/CustomChart.aspx">Custom Chart</a></li>
    <li class="<%=GetLinkStyle("BubbleChart1.aspx")%>"><a href="../Analysis/BubbleChart1.aspx">Bubble Chart</a></li>
</ul> 

<asp:Repeater ID="GroupNamesRepeater" 
              runat="server"               
              OnItemDataBound="GroupNamesRepeater_ItemDataBound">
    <HeaderTemplate>
        <h2>Predefined Charts </h2>   
        <ul> 
    </HeaderTemplate>
     <FooterTemplate>
        </ul>
    </FooterTemplate>
    <ItemTemplate>         
        <%--<a href="<%# DataBinder.Eval(Container.DataItem, "PageUrl")%>?id=<%# DataBinder.Eval(Container.DataItem, "ChartID")%>">
            <%# DataBinder.Eval(Container.DataItem, "ChartName") %></a>--%>
       <li>
         <div id="firstpane" class="menu_list" > 
            <p class="menu_head"><%# DataBinder.Eval(Container.DataItem, "GroupName") %></p>
              <div class="menu_body">              
                  <asp:Repeater ID="ChartNamesRepeater" runat="server" >
                      <HeaderTemplate><ul></HeaderTemplate>
                      <FooterTemplate></ul></FooterTemplate>
                      <ItemTemplate>
                        <li class="<%#GetLinkStyle(Convert.ToString(DataBinder.Eval(Container.DataItem, "PageUrl"))+"?id="+Convert.ToString(DataBinder.Eval(Container.DataItem, "ChartID")))%>">                             
                          <a href="<%# DataBinder.Eval(Container.DataItem, "PageUrl")%>?id=<%# DataBinder.Eval(Container.DataItem, "ChartID")%>">
                              <%# DataBinder.Eval(Container.DataItem, "ChartName") %></a>                     
                         </li>
                       </ItemTemplate>                     
                  </asp:Repeater> 
               </div>                   
           </div>  
         </li>           
    </ItemTemplate>   
</asp:Repeater>

    <asp:Repeater ID="ChartsRepeater" runat="server" >
      <HeaderTemplate><h2>Predefined Charts  </h2>   <ul></HeaderTemplate>
      <FooterTemplate></ul></FooterTemplate>
      <ItemTemplate>
         <li class="<%#GetLinkStyle(Convert.ToString(DataBinder.Eval(Container.DataItem, "PageUrl"))+"?id="+Convert.ToString(DataBinder.Eval(Container.DataItem, "ChartID")))%>" >
            <a href="<%# DataBinder.Eval(Container.DataItem, "PageUrl")%>?id=<%# DataBinder.Eval(Container.DataItem, "ChartID")%>">
                <%# DataBinder.Eval(Container.DataItem, "ChartName") %></a>
        </li>
       </ItemTemplate>                     
    </asp:Repeater> 
<h2>ADVANCED Charting</h2>
<ul>
  <li class="<%=GetLinkStyle("xychart.aspx")%>" ><a href="../Analysis/XYChart.aspx">XY Chart</a></li>
  <%--<li class="<%=GetLinkStyle("dualyaxischart.aspx")%>" ><a href="../Analysis/dualyaxischart.aspx">Dual Y-Axis Chart</a></li>--%>
  <li class="<%=GetLinkStyle("piechart.aspx")%>" ><a href="../Analysis/PieChart.aspx">Pie Chart</a></li>
  <%--<%--<li class="<%=GetLinkStyle("bartimeseries.aspx")%>" ><a href="BarTimeSeries.aspx">Bars(time Series)+Pie</a></li>              --%>
  <%--<li class="<%=GetLinkStyle("bubblechart.aspx")%>" ><a href="bubblechart.aspx">Bubble Chart</a></li>--%>
 <%--<li class="<%=GetLinkStyle("growthcomparison")%>" ><a href="GrowthComparison.aspx">Growth Comparison</a></li>
  <li class="<%=GetLinkStyle("heatmap.aspx")%>" ><a href="../Analysis/heatmap.aspx">Heat Map</a></li>--%>
  <%--<li class="<%=GetLinkStyle("customchart.aspx")%>" ><a href="CustomChart.aspx">Build Custom Chart</a></li>
  <li class="<%=GetLinkStyle("xyscrollchart.aspx")%>" ><a href="XYScrollChart.aspx">XY Scroll Chart</a></li>--%>
  <li class="<%=GetLinkStyle("chartwizard.aspx")%>"><a href="../Analysis/chartwizard.aspx">Chart Wizard</a></li>
</ul>  